/**
  ******************************************************************************
  * @file           : LIT_TurbiditySensor.c
  * @brief          : Source file for RK500-07 Turbidity (SS) Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include "LIT_TurbiditySensor.h"
#include "LIT_ModbusMaster.h"
#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "stm32wlxx.h"
#include "math.h"

/* Private constants ----------------------------------------------------------*/
#define baud							9600
#define timeout							1500
#define polling							1000
#define retry_count						10
#define LED								9
#define TOTAL_NO_OF_REGISTERS			100

/* Private types -------------------------------------------------------------*/
enum
{
  READ_REGISTER,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

union {
  float float32;
  uint32_t uint32;
} u;

/* Private variables ---------------------------------------------------------*/
// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

/* Exported functions ---------------------------------------------------------*/
void TurbiditySensor_Init( void )
{
    /*
   * Construct Packet untuk setiap req yang ada
   * - packet
   * - slave address
   * - function
   * - register address
   * - unsigned int count
   * - local reg address
   */
  modbus_construct(&packets[READ_REGISTER], 8, READ_HOLDING_REGISTERS, 0, 2, 0);

   /*
	* Konfigurasi pengiriman
	* - Baud Rate
	* - Timeout (ms)
	* - polling time (ms)
	* - retry count
	* - packets struct
	* - total packet number
	* - local register variable
	*/
  modbus_configure(baud, timeout, polling, retry_count, packets, TOTAL_NO_OF_PACKETS, regs);
}

void TurbiditySensor_Read( Turbidity_t *object )
{
	memset(regs, 0x00, sizeof(regs));

	modbus_update();

	Modbus_PRINTF(2, "%d",packets[0].id);
	Modbus_PRINTF(2, "%d",packets[0].function);
	Modbus_PRINTF(2, "%d",packets[0].address);
	Modbus_PRINTF(2, "%d",packets[0].data);
	Modbus_PRINTF(2, "%d\n",packets[0].local_start_address);

	Modbus_PRINTF(2, "%d",packets[0].requests);
	Modbus_PRINTF(2, "%d",packets[0].successful_requests);
	Modbus_PRINTF(2, "%d",packets[0].failed_requests);
	Modbus_PRINTF(2, "%d",packets[0].exception_errors);
	Modbus_PRINTF(2, "%d",packets[0].retries);
	Modbus_PRINTF(2, "%d\n",packets[0].connection);

	u.uint32 = regs[0] << 16 | regs[1];
	object->turbidity = isnan(u.float32) ? 0 : u.float32;

	Modbus_PRINTF(1, "Turbidity: %d.%03d NTU\r\n", (uint32_t)(object->turbidity), (uint32_t)(object->turbidity * 1000) % 1000);

}
