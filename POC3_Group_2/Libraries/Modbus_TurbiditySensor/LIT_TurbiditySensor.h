/**
  ******************************************************************************
  * @file           : LIT_TurbiditySensor.h
  * @brief          : Header file for RK500-07 Turbidity (SS) Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_TURBIDITY_SENSOR_H__
#define __LIT_TURBIDITY_SENSOR_H__

/* Includes ------------------------------------------------------------------*/
#include <inttypes.h>

/* Exported constants --------------------------------------------------------*/
#define TURBIDITY_READY_DURATION_MS               4000
#define SENSOR_READY_DURATION_MS                  TURBIDITY_READY_DURATION_MS

/* Exported types ------------------------------------------------------------*/
typedef struct
{
  float turbidity;

} Turbidity_t;

/* Exported functions prototypes ---------------------------------------------*/
void TurbiditySensor_Init( void );
void TurbiditySensor_Read( Turbidity_t *object );

#endif /* __LIT_TURBIDITY_SENSOR_H__ */