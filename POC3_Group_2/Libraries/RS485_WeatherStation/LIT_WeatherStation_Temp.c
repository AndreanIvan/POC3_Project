#include "LIT_WeatherStation_Temp.h"
#include "stm32wlxx_hal.h"
//#include "stm32wlxx_hal_uart.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include "sys_app.h"

//const char string_21[] = "%c %d %s %d %d %s %d %s %d %s %s %s %c %d %s %s %s %d %s %s %s %d %s %s %d %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s"; //GMX531 GMX541 GMX551 frame format
const char string_32[] = "%s %d %d %d %d %d %s %s %s %s %s %d %s %s %s %s %s %s %s %s %s %s"; //GMX531 GMX541 GMX551 frame format
//const char string_31[] = "%c %d %d %d %d %d %s %s %s %s %s %d %s %s %s %s %s %s %s %s %s %s"; //GMX531 + User defined frame format

extern UART_HandleTypeDef _STM_UART1;
extern UART_HandleTypeDef _STM_UART3;
char StrBuffer1[99]="";

weatherStationVector gmx;
uint8_t stationModel = 0;

/* Prints the supplied string to uart */
//static void debugPrint(char string[])
//{
//	HAL_UART_Transmit(&_STM_UART1, (uint8_t*)string, strlen(string), HAL_MAX_DELAY);
//}
#define debugPrint(X)							APP_PRINTF("%s", X)

uint8_t buffer_temp[512];
uint16_t counter = 0;

//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//  HAL_UART_Receive_IT(&_STM_UART3, &buffer_temp[counter++], 1);
//}

void weatherStation_ON()
{
  //uint8_t stationModel = 0;
  //ATSerial.begin(9600);
  //ATSerial.flush();
  //delay(5);

  const uint16_t BUFFER_SIZE = 300;
  char buffer[BUFFER_SIZE];
  memset(buffer, 0x00, sizeof(buffer));
  bool exitWhile = 0;
  //uint16_t i = 0;
  //unsigned long previous = HAL_GetTick(); //millis();

  //ATSerial.print('9'); // sementara dicomment. Mohon diuncomment lagi
  //delay(1000);
  HAL_UART_Transmit(&_STM_UART3, (uint8_t *) "9", strlen("9"), 100);
  //HAL_UART_Receive(&_STM_UART3, (uint8_t *) buffer, 70, 10000); //lama

  memset(buffer,0x00,BUFFER_SIZE); //tambahan
  HAL_UART_Receive(&_STM_UART3, (uint8_t *) buffer, BUFFER_SIZE, 2000); //baru
  //debugPrint("\n");
  if (strstr(buffer, "<END OF STARTUP MESSAGE>") != NULL)
  {
	  exitWhile = 1;
  }
  sprintf(StrBuffer1, "exitWhile: %d\n",exitWhile);
  debugPrint(StrBuffer1);
  //Capture buffer
  //coba tes comment
  //ternyata bekerja dengan baik yaitu saat startup yang sering nilai semua sensor null
  //sementara tidak ada pengecekan nyala sensor saat startup
  /*
  while (((HAL_GetTick() - previous) < 10000) && exitWhile == 0)
  {
    if (Serial.available())
    {
      //Avoid storing 0x00 in our buffer string
      char buffer_temp = Serial.read();
      if (buffer_temp != 0x00)
      {
        buffer[i] = buffer_temp;
        i++;
      }
      if (strstr(buffer, "<END OF STARTUP MESSAGE>") != NULL)
      {
        exitWhile = 1;
      }
      if (i == BUFFER_SIZE)
      {
        exitWhile = 1;
      }
    }

    //avoid millis overflow problem after approximately 50 days
    if ( millis() < previous ) previous = millis();
  }
*/
#if DEBUG_LIT > 1
  debugPrint("WS RX:");
  debugPrint(buffer);
#endif
  //ATSerial.flush();
  //delay(5);

  if (strstr(buffer, "GMX100") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX100 found");
#endif

    stationModel = WS_GMX100;
  }
  else if (strstr(buffer, "GMX101") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX101 found");
#endif

    stationModel = WS_GMX101;
  }
  else if (strstr(buffer, "GMX200") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX200 found");
#endif

    stationModel = WS_GMX200;
  }
  else if (strstr(buffer, "GMX240") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX240 found");
#endif

    stationModel = WS_GMX240;
  }
  else if (strstr(buffer, "GMX300") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX300 found");
#endif

    stationModel = WS_GMX300;
  }
  else if (strstr(buffer, "GMX301") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX301 found");
#endif

    stationModel = WS_GMX301;
  }
  else if (strstr(buffer, "GMX400") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX400 found");
#endif

    stationModel = WS_GMX400;
  }
  else if (strstr(buffer, "GMX500") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX500 found");
#endif

    stationModel = WS_GMX500;
  }
  else if (strstr(buffer, "GMX501") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX501 found");
#endif

    stationModel = WS_GMX501;
  }
  else if (strstr(buffer, "GMX531") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("\nGMX531 found\n");
#endif

    stationModel = WS_GMX531;
  }
  else if (strstr(buffer, "GMX541") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX541 found");
#endif

    stationModel = WS_GMX541;
  }
  else if (strstr(buffer, "GMX550") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX550 found");
#endif

    stationModel = WS_GMX550;
  }
  else if (strstr(buffer, "GMX551") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX551 found");
#endif

    stationModel = WS_GMX551;
  }
  else if (strstr(buffer, "GMX600") != NULL)
  {
#if DEBUG_LIT > 1
	  debugPrint("GMX600 found");
#endif

    stationModel = WS_GMX600;
  }
}

uint8_t weatherStation_read()
{

	const uint16_t BUFFER_SIZE = 512; //jika diset 250 program jadi lambat dan sering error upload
	char buffer_ws_raw[BUFFER_SIZE];
//	char buffer_temp[BUFFER_SIZE];
	memset(buffer_ws_raw, 0x00, sizeof(buffer_ws_raw));
	memset(buffer_temp, 0x00, sizeof(buffer_ws_raw));
	bool exitWhile = 0;
	bool dataFrameFound = 0;
	bool dataFrameParsed = 0;
	//uint16_t i = 0;
	//unsigned long previous = HAL_GetTick();
	char buffer_table[100];
	memset(buffer_table, 0x00, sizeof(buffer_table));
	int compass, windDirection, correctedWindDirection, avgWindDirection, avgCorrectedWindDirection, relativeHumidity;// solarRadiation;
	//gmx.windDirection = 25;

	// Initialize vector variables
	gmx.windDirection = 0;
	gmx.correctedWindDirection = 0;
	gmx.avgWindDirection = 0;
	gmx.windSpeed = 0;
	gmx.avgWindSpeed = 0;
	gmx.avgWindGustDirection = 0;
	gmx.avgWindGustSpeed = 0;
	gmx.avgCorrectedWindDirection = 0;
	strcpy(gmx.windSensorStatus, "0");
	gmx.precipTotal = 0;
	gmx.precipIntensity = 0;
	gmx.precipStatus = 0;
	gmx.compass = 0;
	gmx.xTilt = 0;
	gmx.yTilt = 0;
	gmx.zOrient = 0;
	strcpy(gmx.timestamp, "0");
	gmx.supplyVoltage = 0;
	strcpy(gmx.status, "0");
	gmx.solarRadiation = 0;
	gmx.sunshineHours = 0;
	strcpy(gmx.sunriseTime, "0");
	strcpy(gmx.solarNoonTime, "0");
	strcpy(gmx.sunsetTime, "0");
	strcpy(gmx.sunPosition, "0");
	strcpy(gmx.twilightCivil, "0");
	strcpy(gmx.twilightNautical, "0");
	strcpy(gmx.twilightAstronom, "0");
	gmx.pressure = 0;
	gmx.pressureSeaLevel = 0;
	gmx.pressureStation = 0;
	gmx.relativeHumidity = 0;
	gmx.temperature = 0;
	gmx.dewpoint = 0;
	gmx.absoluteHumidity = 0;
	gmx.airDensity = 0;
	gmx.wetBulbTemperature = 0;
	gmx.windChill = 0;
	gmx.heatIndex = 0;
#ifdef GMX_GPS_OPTION
	gmx.gpsCorrectedSpeed = 0;
	gmx.gpsAvgCorrectedSpeed = 0;
	gmx.gpsCorrectedGustSpeed = 0;
	gmx.gpsCorrectedGustDirection = 0;
	strcpy(gmx.gpsLocation, "0");
	gmx.gpsHeading = 0;
	gmx.gpsSpeed = 0;
	strcpy(gmx.gpsStatus, "0");
#endif

	// Create and initialize local variables
	char node_letter_local = 0;
	char windSpeed_local[7];
	char avgWindSpeed_local[7];
	char avgWindGustSpeed_local[7];
	char precipTotal_local[10];
	char precipIntensity_local[8];
	char precipStatus_local = 'N';
	char xTilt_local[4];
	char yTilt_local[4];
	char zOrient_local[3];
	char supplyVoltage_local[6];
	char sunshineHours_local[5];
	char pressure_local[7];
	char pressureSeaLevel_local[7];
	char pressureStation_local[7];
	char temperature_local[6];
	char dewpoint_local[7];
	char absoluteHumidity_local[6];
	char airDensity_local[3];
	char wetBulbTemperature_local[7];
	char windChill_local[5];
	char solarRad_local[6];
#ifdef GMX_GPS_OPTION
	char gpsCorrectedSpeed_local[7];
	char gpsAvgCorrectedSpeed_local[7];
	char gpsCorrectedGustSpeed_local[7];
	char gpsSpeed_local[8];
#endif

	strcpy(windSpeed_local, "0");
	strcpy(avgWindSpeed_local, "0");
	strcpy(avgWindGustSpeed_local, "0");
	strcpy(precipTotal_local, "0");
	strcpy(precipIntensity_local, "0");
	strcpy(xTilt_local, "0");
	strcpy(yTilt_local, "0");
	strcpy(zOrient_local, "0");
	strcpy(supplyVoltage_local, "0");
	strcpy(sunshineHours_local, "0");
	strcpy(pressure_local, "0");
	strcpy(pressureSeaLevel_local, "0");
	strcpy(pressureStation_local, "0");
	strcpy(temperature_local, "0");
	strcpy(dewpoint_local, "0");
	strcpy(absoluteHumidity_local, "0");
	strcpy(airDensity_local, "0");
	strcpy(wetBulbTemperature_local, "0");
	strcpy(windChill_local, "0");
	strcpy(solarRad_local, "0");
#ifdef GMX_GPS_OPTION
	strcpy(gpsCorrectedSpeed_local, "0");
	strcpy(gpsAvgCorrectedSpeed_local, "0");
	strcpy(gpsCorrectedGustSpeed_local, "0");
	strcpy(gpsSpeed_local, "0");
#endif

	counter = 0;
	memset(buffer_temp,0,BUFFER_SIZE); //tambahan

	HAL_UART_Transmit(&_STM_UART3, (uint8_t *) "Q", strlen("Q"), 50);

//	HAL_UART_Receive(&_STM_UART3, (uint8_t *) buffer_temp, BUFFER_SIZE, 4000);
	HAL_UART_Receive_IT (&_STM_UART3, &buffer_temp[counter++], 1);
	HAL_Delay(3000);
	debugPrint("buffer_temp:\n");

	debugPrint("Buffer temp: ");
	debugPrint(buffer_temp);
	debugPrint("\r\n");
//	HAL_UART_Transmit(&_STM_UART1, (uint8_t *)buffer_temp, BUFFER_SIZE, 2000);

	const char* startOfText = strstr(buffer_temp, "Q");
	const char* endOfText = strstr(startOfText, "\x03");
	if(startOfText)
	{
		int diff_addr = (int)(endOfText) - (int)(startOfText);
		strncpy(buffer_temp, startOfText, diff_addr );
		buffer_temp[diff_addr - 1] = 0x03;
		memset(&buffer_temp[diff_addr] ,0,BUFFER_SIZE - diff_addr);
	}

	debugPrint(buffer_temp);
	debugPrint("\n");

	char buffer_temp_previous;

	int j = 0;
	for (int i=0; i<BUFFER_SIZE; i++)
	{
		if ((buffer_temp[i] != 0x00) && (buffer_temp[i] != 0x02))
		{
			//sprintf(StrBuffer1, "buffer_temp[%d]: %c\n", i, buffer_temp[i]);
			//debugPrint(StrBuffer1);
			//Change ',' for ' ' (whitespace character) for better parsing
			if (buffer_temp[i] != ',')
			{
			  //debugPrint("tidak koma\n");
			  buffer_ws_raw[j] = buffer_temp[i];

			}
			else //jika buffer_temp == ','
			{
				if (buffer_temp[i] == buffer_temp_previous)
				{
					// Serial.print(F("double comma at: "));
					// Serial.print(F("index: "));
					// Serial.println(i);
					buffer_ws_raw[j] = '0';
					j++;
					buffer_ws_raw[j] = ' ';

				}
				else
				{
					buffer_ws_raw[j] = ' ';
				}
			}
			buffer_temp_previous = buffer_temp[i];
			j++; //naikkan counter j

		}
		//ETX (End of Text) found
		if (buffer_ws_raw[j-1] == '\x03')
		{
			buffer_ws_raw[j - 1] = 0x00;
			dataFrameFound = 1;
			debugPrint("ETX (End of Text) found\n");
		}
		if (j - 1 == BUFFER_SIZE)
		{
			sprintf(StrBuffer1, "exitWhile: %d\n", exitWhile);
			debugPrint(StrBuffer1);
			exitWhile = 1;
		}
	}
	debugPrint("buffer_ws_raw:\n");
	debugPrint(buffer_ws_raw);
	debugPrint("\n");
	sprintf(StrBuffer1, "dataFrameFound: %d\n",dataFrameFound);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "stationModel: %d\n",stationModel);
	debugPrint(StrBuffer1);
	debugPrint("\n");

	if (dataFrameFound) // && (stationModel == WS_GMX531 || stationModel == WS_GMX541 || stationModel == WS_GMX551))
	{
		debugPrint("GMX_GPS_OPTION 531\n");
		//const char string_32[] = "%c %d %d %d %d %d %s"; //GMX531 GMX541 GMX551 frame format
		sscanf (buffer_ws_raw, string_32, &node_letter_local, &compass, &windDirection, &correctedWindDirection, &avgWindDirection, &avgCorrectedWindDirection, windSpeed_local, avgWindSpeed_local, gpsAvgCorrectedSpeed_local, gmx.windSensorStatus, temperature_local, &relativeHumidity, dewpoint_local, pressure_local, solarRad_local, sunshineHours_local, precipTotal_local, precipIntensity_local, gmx.timestamp, supplyVoltage_local, gmx.gpsStatus, gmx.status);
		//sscanf (buffer_ws_raw, string_32, &node_letter_local, &compass, &windDirection, &correctedWindDirection, &avgWindDirection, &avgCorrectedWindDirection, windSpeed_local, avgWindSpeed_local, gpsAvgCorrectedSpeed_local, gmx.windSensorStatus, temperature_local, &relativeHumidity, dewpoint_local, pressure_local, solarRad_local, sunshineHours_local, precipTotal_local, precipIntensity_local, gmx.timestamp, supplyVoltage_local, gmx.gpsStatus, gmx.status);
		//gmx.solarRadiation
		//gmx.solarRadiation, sunshineHours_local, precipTotal_local, precipIntensity_local, gmx.timestamp, supplyVoltage_local, gmx.gpsStatus, gmx.status

		dataFrameParsed = 1;
	}
/*
	sprintf(StrBuffer1, "node_letter_local: %c\n", node_letter_local);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "compass: %d\n", gmx.compass);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "windDirection: %d\n", gmx.windDirection);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "correctedWindDirection: %d\n", gmx.correctedWindDirection);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "avgWindDirection: %d\n", gmx.avgWindDirection);
	debugPrint(StrBuffer1);
	sprintf(StrBuffer1, "avgCorrectedWindDirection: %d\n", gmx.avgCorrectedWindDirection);
	debugPrint(StrBuffer1);
	*/
	//sprintf(StrBuffer1, "windSpeed_local: %s\n", windSpeed_local);
	//debugPrint(StrBuffer1);

	sprintf(StrBuffer1, "dataFrameParsed: %d\n", dataFrameParsed);
	debugPrint(StrBuffer1);

	if (dataFrameParsed == 1)
	{
		if (precipStatus_local == 'Y')
	    {
	      gmx.precipStatus = 1;
	    }
	    else
	    {
	      gmx.precipStatus = 0;
	    }
		/*
		float val;
		char str[20];

		strcpy(str, "0.15");
		val = atof(str);
		sprintf(StrBuffer1, "String value = %s, Float value = %.2f\n", str, val);
		debugPrint(StrBuffer1);
		*/
		//atof function must include these lib #include <stdlib.h>
	    gmx.windSpeed = atof (windSpeed_local);
	    //sprintf(StrBuffer1, "windSpeed_local: %.2f\n", gmx.windSpeed);
	    //debugPrint(StrBuffer1);
	    gmx.avgWindSpeed = atof (avgWindSpeed_local);
	    gmx.avgWindGustSpeed = atof (avgWindGustSpeed_local);
	    gmx.precipTotal = atof (precipTotal_local);
	    gmx.precipIntensity = atof (precipIntensity_local);
	    gmx.xTilt = atof (xTilt_local);
	    gmx.yTilt = atof (yTilt_local);
	    gmx.zOrient = atof (zOrient_local);
	    gmx.supplyVoltage = atof (supplyVoltage_local);
	    gmx.sunshineHours = atof (sunshineHours_local);
	    gmx.pressure = atof (pressure_local);
	    gmx.pressureSeaLevel = atof (pressureSeaLevel_local);
	    gmx.pressureStation = atof (pressureStation_local);
	    gmx.temperature = atof (temperature_local);
	    gmx.dewpoint = atof (dewpoint_local);
	    gmx.absoluteHumidity = atof (absoluteHumidity_local);
	    gmx.airDensity = atof (airDensity_local);
	    gmx.wetBulbTemperature = atof (wetBulbTemperature_local);
	    gmx.windChill = atof (windChill_local);
		gmx.solarRadiation = atof(solarRad_local);
		gmx.compass = compass;
		gmx.windDirection = windDirection;
		gmx.correctedWindDirection = correctedWindDirection;
	    gmx.avgWindDirection = avgWindDirection;
	    gmx.avgCorrectedWindDirection = avgCorrectedWindDirection;
		gmx.relativeHumidity = relativeHumidity;
		// SerialUSB.print(F("compass: "));
		// SerialUSB.println(gmx.compass);
		// SerialUSB.print(F("windDirection: "));
		// SerialUSB.println(gmx.windDirection);
	#ifdef GMX_GPS_OPTION
	    gmx.gpsCorrectedSpeed = atof (gpsCorrectedSpeed_local);
	    gmx.gpsAvgCorrectedSpeed = atof (gpsAvgCorrectedSpeed_local);
	    gmx.gpsCorrectedGustSpeed = atof (gpsCorrectedGustSpeed_local);
	    gmx.gpsSpeed = atof (gpsSpeed_local);
	//    Serial.print("gpsCorrectedSpeed_local: ");
	//    Serial.println(gmx.gpsCorrectedSpeed);
	//    Serial.print("gpsAvgCorrectedSpeed: ");
	//    Serial.println(gmx.gpsAvgCorrectedSpeed);
	//    Serial.print("gpsCorrectedGustSpeed: ");
	//    Serial.println(gmx.gpsCorrectedGustSpeed);

	#endif
	    //debugPrint("GMX_GPS_OPTION 531:\n");


	    return 1;
	}
	else
	{
		return 0;
	}
}

void getData(char* response, int timeout)
{
	memset(response,0,999);

	HAL_UART_Transmit(&_STM_UART3, (uint8_t *) "Q", strlen("Q"), 50);

	HAL_UART_Receive(&_STM_UART3, (uint8_t *) response, 200, timeout);
}

/* Prints the supplied string to uart */
//void debugPrint(char string[])
//{
//	HAL_UART_Transmit(&_STM_UART1, (uint8_t*)string, strlen(string), HAL_MAX_DELAY);
//}
