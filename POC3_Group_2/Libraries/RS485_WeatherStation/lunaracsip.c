#include "lunaracsip.h"
#include "stm32wlxx_hal.h"
#include "stm32wlxx_hal_uart.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

extern UART_HandleTypeDef _ACSIP_UART;
extern UART_HandleTypeDef _STM_UART;
char StrBuffer[99]="";


void sip_reset(int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip reset", strlen("sip reset"), timeout);
}

void sip_reset_with_response(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip reset", strlen("sip reset"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 999, timeout);
}

void sip_get_version(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip get_ver", strlen("sip get_ver"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 20, timeout);
}

void sip_set_log_debug(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip set_log debug", strlen("sip set_log debug"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 20, timeout);
}

void mac_get_band(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_band", strlen("mac get_band"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

void mac_get_ch_para(char* response, int ch, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac get_ch_para %d", ch);
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_set_ch_status(char* response, char* status, int ch, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac set_ch_status %d %s", ch, status);
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_get_ch_status(char* response, int ch, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac get_ch_status %d", ch);
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_set_ch_freq(char* response, int ch_id, long int freq, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac set_ch_freq %d %ld", ch_id, freq);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) command, strlen(command), 50);

	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_set_ch_dr_range(char* response, int ch_id, int min_dr, int max_dr, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac set_ch_dr_range %d %d %d", ch_id, min_dr, max_dr);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) command, strlen(command), 50);

	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_set_rx2(char* response, int dr, long int freq, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac set_rx2 %d %ld", dr, freq);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) command, strlen(command), 50);

	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_get_rx2(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_rx2", strlen("mac get_rx2"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 50, timeout);
}

void mac_set_join_ch(char* response, char* status, int ch, int timeout)
{
	memset(response,0,999);
	char command[99];
	sprintf(command,"mac set_join_ch %d %s", ch, status);

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}

void mac_get_join_ch(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_join_ch", strlen("mac get_join_ch"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 100, timeout);
}


bool mac_set_power(int power_dBm)
{
	char command[99];
	char resp[99];

	memset(resp,0,99); //terima dari ACSip wajib menggunakan memset. Jika tidak maka karakter aneh akan muncul
	if (!is_valid_power_dBm(power_dBm))
	{
	   debugPrint("not valid power_dBm\n");
	   return false;
	}
	else
	{
		debugPrint("valid power_dBm\n");
	}
	sprintf(command,"mac set_power %d", power_dBm);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 8, 100);

	/* Jika response >> Ok */
	if (strContains(resp, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return false;
	}
}

bool is_valid_power_dBm(int x)
{
	if (x == 14 || x == 16)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void mac_get_power(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_power", strlen("mac get_power"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

bool mac_set_dr(int rate)
{
	char command[99];
	char resp[99];
	memset(resp,0,99); //terima dari ACSip wajib menggunakan memset. Jika tidak maka karakter aneh akan muncul

	if (!is_valid_rate(rate))
	{
	   debugPrint("not valid rate\n");
	   return false;
	}
	else
	{
		debugPrint("valid rate\n");
	}

	sprintf(command,"mac set_dr %d", rate);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 8, 100);

	/*
	sprintf(StrBuffer,"Set DR: %s", response);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	return true;
	*/
	if (strContains(resp, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return false;
	}
}

bool is_valid_rate(int x)
{
	if (x == 0 || x == 1 || x == 2 || x == 3 || x == 4 || x == 5)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void mac_get_dr(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_dr", strlen("mac get_dr"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

bool mac_set_class(int lora_class, int timeout)
{
	char resp[99];
	memset(resp,0,99); //terima dari ACSip wajib menggunakan memset. Jika tidak maka karakter aneh akan muncul

	if(lora_class == LORA_CLASS_A)
	{
	  /* Kirim ke ACSIP */
	  HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac set_class A", strlen("mac set_class A"), 50);

	  /* Terima dari ACSIP */
	  HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 8, timeout);
	}

	else if(lora_class == LORA_CLASS_C)
	{
	  /* Kirim ke ACSIP */
	  HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac set_class C", strlen("mac set_class C"), 50);

	  /* Terima dari ACSIP */
	  HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 8, timeout);
	}

	/* Jika response >> Ok */
	if (strContains(resp, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return false;
	}
}

void mac_get_class(char* response, int timeout)
{
	memset(response,0,999);

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_class", strlen("mac get_class"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

void sip_get_hw_deveui(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip get_hw_deveui", strlen("sip get_hw_deveui"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 30, timeout);
}

void sip_get_hw_model(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip get_hw_model", strlen("sip get_hw_model"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

bool initOTAA(char* devEUI, char* appEUI, char* appKEY, int timeout)
{
	char devEUI_message[50], appEUI_message[50], appKEY_message[50], resp1[50], resp2[50], resp3[50];

	memset(resp1,0,50);
	memset(resp2,0,50);
	memset(resp3,0,50);
	sprintf(devEUI_message, "mac set_deveui %s", devEUI);
	sprintf(appEUI_message, "mac set_appeui %s", appEUI);
	sprintf(appKEY_message, "mac set_appkey %s", appKEY);

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) devEUI_message, strlen(devEUI_message), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp1, 8, timeout);

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) appEUI_message, strlen(appEUI_message), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp2, 8, timeout);

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) appKEY_message, strlen(appKEY_message), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp3, 8, timeout);

	//if(res1[5] == 'O' && res2[5] == 'O' && res3[5] == 'O') return true;
	if ((strContains(resp1, STATUS_OK)) && (strContains(resp2, STATUS_OK)) && (strContains(resp3, STATUS_OK))) return true;
	else return false;

}

void mac_save(char* response, int timeout)
{
	memset(response,0,999);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac save", strlen("mac save"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 10, timeout);
}

void mac_get_deveui(char* response, int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_deveui", strlen("mac get_deveui"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 22, timeout);
}

void mac_get_appeui(char* response, int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_appeui", strlen("mac get_appeui"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 22, timeout);
}

void mac_get_appkey(char* response, int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_appkey", strlen("mac get_appkey"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 50, timeout);
}

bool joinLoRaNetwork(int mode, char* response, int timeout)
{
	//bool ready;
	memset(response,0,999);

	if(mode == ABP)
	{
	   /* Kirim ke ACSIP */
	   HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac join abp", strlen("mac join abp"), 50);

	   /* Terima dari ACSIP */
	   HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 8, timeout);
	   //HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response2, 16, timeout2);

    }

	else if(mode == OTAA)
	{
	   /* Kirim ke ACSIP */

	   //Cek apakah ada yang diterima dari ACSIP
	   //HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response1, 10, timeout1);

	   sprintf(StrBuffer,"mac join OTAA\n");
	   HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);

	   HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac join otaa", strlen("mac join otaa"), 50);
	   //HAL_Delay(100);
	   /* Terima dari ACSIP */
	   HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 500, timeout);

	}
	if (strContains(response, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", response);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else return false;

	//memset(response,0,999);
	//HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 50, timeout2);
/*
	if (strContains(response, STATUS_ACCEPTED))
	{
		sprintf(StrBuffer,"response: %s", response);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		ready = ready & true;
	}
	else ready = ready & false;
*/
}

void receiveData(char* response, int timeout)
{
	memset(response,0,999);
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 300, timeout); // perhatikan panjangnya. Jika pendek akan terpotong nanti
	sprintf(StrBuffer,"\nreceiveData: %s\n", response);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),100);
	sprintf(StrBuffer,"Response length: %d\n", strlen(response));
	HAL_UART_Transmit(&huart1, (uint8_t *) StrBuffer, strlen(StrBuffer), 50);
}

bool acceptResponseJoin(char* response, int timeout)
{
	//char resp[99];
	//memset(buffer,0,sizeof(buffer));
	memset(response,0,999);
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 50, timeout);
	if (strContains(response, STATUS_ACCEPTED))
	{
		sprintf(StrBuffer,"response true: %s", response);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else
	{
		return false;
	}
}

void sip_sleep(int time, int interruptible, int timeout)
{
	/* Batasi waktu sleep */
	if(time<10)time = 10;
	if(time>604800)time = 604800;

	/* Masukkan waktu & interruptible ke string */
	char sleep[50];
	if(interruptible)
	{
	  sprintf(sleep,"sip sleep %d uart_on", time);
	}
	else
	{
	  sprintf(sleep,"sip sleep %d uart_off", time);
	}

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) sleep, strlen(sleep), timeout);
}

bool mac_set_adr(bool adr)
{
	char resp[99];

	memset(resp,0,99); //terima dari ACSip wajib menggunakan memset. Jika tidak maka karakter aneh akan muncul
	char sleep[50];
	if(adr)
	{
	  sprintf(sleep,"mac set_adr on");
	}
	else
	{
	  sprintf(sleep,"mac set_adr off");
	}

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) sleep, strlen(sleep), 50);



	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 8, 100);

	/* Jika response >> Ok */
	if (strContains(resp, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else
	{
		sprintf(StrBuffer,"response: %s", resp);
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return false;
	}
}

void sip_sleep_with_response(int time, int interruptible, char* response, int timeout)
{
	/* Batasi waktu sleep */
	if(time<10)time = 10;
	if(time>604800)time = 604800;

	/* Masukkan waktu & interruptible ke string */
	char sleep[99];
	if(interruptible)
	{
	  sprintf(sleep,"sip sleep %d uart_on", time);
	}
	else
	{
	  sprintf(sleep,"sip sleep %d uart_off", time);
	}

	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) sleep, strlen(sleep), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 50, timeout);
}


void sip_wakeup(char* response)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "wake", strlen("wake"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, 50);
}

void mac_get_join_status(char* response, int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "mac get_join_status", strlen("mac get_join_status"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 22, timeout);
}

void sip_factory_reset(int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip factory_reset", strlen("sip factory_reset"), timeout);
}

void sip_factory_reset_with_response(char* response, int timeout)
{
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) "sip factory_reset", strlen("sip factory_reset"), 50);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 12, timeout);
}

bool mac_sendData(int type, int port, char* datahex, char* response)
{
	char command[99];
	char* type_tx;
	memset(command,0,99);
	//memset(type_tx,0,99);
	memset(response,0,999);

	if (type == UNCONFIRMED_TX)
	{
		type_tx = "ucnf";
	}
	else if (type == CONFIRMED_TX)
	{
		type_tx = "cnf";
	}
	else
	{
		debugPrint("Error define Uplink type\n");
		return false;
	}
	//char resp[999];
	sprintf(command, "mac tx %s %d %s", type_tx, port, datahex);
	sprintf(StrBuffer,"Command length: %d\n", strlen(command));
	debugPrint(command);
	debugPrint("\n");
	debugPrint(StrBuffer);
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) command, strlen(command),10);
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, strlen(response), 100); // flush UART ACSip
	//HAL_Delay(100);
	/* Kirim ke ACSIP */
	HAL_UART_Transmit(&_ACSIP_UART, (uint8_t *) command, strlen(command), 10);

	/* Terima dari ACSIP */
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 500, 100);

	if (strContains(response, STATUS_OK))
	{
		sprintf(StrBuffer,"response: %s", response);
		debugPrint(StrBuffer);
		//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else return false;
	//HAL_Delay(100);
	/* Terima dari ACSIP */
	//HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, strlen(response), 1000);
	//sprintf(StrBuffer,"response: %s\n", response);
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	/*
	debugPrint("Response TX1\n");
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) response, strlen(response),10);

	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, strlen(response), 1000);
	debugPrint("Response TX2\n");
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) response, strlen(response),10);

	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, strlen(response), 1000);
	debugPrint("Response TX3\n");
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) response, strlen(response),10);
	*/
	/*
	if (compare_string(response, ">> Ok") == 1)
	{
		sprintf(StrBuffer,"Response >>Ok\n");
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		//HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, strlen(response), 100);
		//HAL_UART_Transmit(&_STM_UART, (uint8_t *) response, strlen(response),10);
		return true;
	}
	else
	{
		sprintf(StrBuffer,"Response failed\n");
		HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return false; //hanya contoh
	}
	*/
	//return true;
}

bool acceptTxResponse(char* response, int timeout)
{
	memset(response,0,999);

	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) response, 200, timeout); //ini harus ada nilainya, bukan strlen(response)

	debugPrint("Response:");
	debugPrint(response);

	if (strContains(response, STATUS_TX_OK))
	{
		sprintf(StrBuffer,"response: %s", response);
		debugPrint(StrBuffer);
		//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
		return true;
	}
	else return false;
}

void acceptResponse(char* response, int timeout)
{
	char resp[200];
	//memset(buffer,0,sizeof(buffer));
	memset(resp,0,strlen(resp));
	/*
	for(int i=0;i<strlen(resp);i++)
	{
		resp[i] = 0;
	}
	*/
	//sprintf(StrBuffer, "After memset():  %s", resp);
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	HAL_UART_Receive(&_ACSIP_UART, (uint8_t *) resp, 200, timeout); //ini harus ada nilainya, bukan strlen(response)
	//sprintf(StrBuffer,"acceptResponse: %s\n", response);
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	//sprintf(StrBuffer,"acceptResponse: \n");
	debugPrint("Response:");
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) resp, strlen(resp),HAL_MAX_DELAY);

	strContains(resp, ">> Ok");
	strContains(resp, ">> tx_ok");
	//sprintf(StrBuffer,"Response length: %d\n", strlen(resp));
	//HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),HAL_MAX_DELAY);
}

bool compare_string(char str[],char word[])
{
	/* Fungsi strncmp akan memberikan nilai 0 jika string sama */
	/* String yang dibandingkan hanya sampai strlen(word), string setelah itu tidak dibandingkan */
	sprintf(StrBuffer,"String diterima: %s\n", str);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Panjang String diterima: %d\n", strlen(str));
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter 0: %c\n", str[0]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter 1: %c\n", str[1]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter 2: %c\n", str[2]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter 3: %c\n", str[3]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter word 0: %c\n", word[0]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter word 1: %c\n", word[1]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter word 2: %c\n", word[2]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Karakter word 3: %c\n", word[3]);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);

	sprintf(StrBuffer,"String dicompare: %s\n", word);
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	sprintf(StrBuffer,"Panjang String dicompare: %d\n", strlen(word));
	HAL_UART_Transmit(&_STM_UART, (uint8_t *) StrBuffer, strlen(StrBuffer),10);
	if (!strncmp (str,word,strlen(word)))
	{ // if they are same
		debugPrint("\nSame\n");
		return true;                                 // light led
	}
	else
	{
		debugPrint("\nNot same\n");
		return false;
	}

	/*
    if (strcmp (word,str) != 0)
    { // if they are same
    	debugPrint("\nSame\n");
        return true;                                 // light led
    }
    else
    {
    	debugPrint("\nNot same\n");
        return false;
    }
	*/
}

/* Prints the supplied string to uart */
void debugPrint(char string[])
{
	HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), HAL_MAX_DELAY);
}


bool strContains(char* string, char* toFind) //aslinya uint32_t
{
	//Contoh yang menghasilkan -1:
	//strContain("aaa","aaaa");

    uint8_t slen = strlen(string);
    uint8_t tFlen = strlen(toFind);
    uint8_t found = 0;

    if( slen >= tFlen )
    {
        for(uint8_t s=0, t=0; s<slen; s++)
        {
            do{

                if( string[s] == toFind[t] )
                {
                    if( ++found == tFlen )
                    {
                    	debugPrint("Found!\n");
                    	return true;
                    }
                    s++;
                    t++;
                }
                else { s -= found; found=0; t=0; }

              }while(found);
        }
        debugPrint("Not found!\n");
        return false;
    }
    else return false; //-1
}
//char* getChParam(int channel)
//{
//
//}
//
//char* getChStatus(int channel)
//{
//
//}
