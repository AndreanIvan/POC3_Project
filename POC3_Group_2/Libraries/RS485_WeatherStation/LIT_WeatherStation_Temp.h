#ifndef LUNARWEATHER_H_
#define LUNARWEATHER_H_

/* Class */
#define DEBUG_LIT      2

#define GMX_GPS_OPTION

// Weather station models
#define WS_GMX100      1
#define WS_GMX101     2
#define WS_GMX200     3
#define WS_GMX240     4
#define WS_GMX300     5
#define WS_GMX301     6
#define WS_GMX400     7
#define WS_GMX500     8
#define WS_GMX501     9
#define WS_GMX531     10
#define WS_GMX541     11
#define WS_GMX550     12
#define WS_GMX551     13
#define WS_GMX600     14
/* Sesuaikan dengan STM32 yang digunakan */
#include "stm32wlxx_hal.h"

/* Untuk tipe data bool */
#include <stdbool.h>

/* Sesuaikan dengan pin UART ACSIP yang digunakan di STM32CubeMX (.ioc) */
#define	_STM_UART1	huart2
#define _STM_UART3  huart1

//typedef struct weatherStationVector gmx;

typedef struct
{
  //! Variable: stores measured wind direction in degrees
  //uint16_t windDirection;
  int windDirection;

  //! Variable: stores measured corrected wind direction in degrees
  //uint16_t correctedWindDirection;
  int correctedWindDirection;

  //! Variable: stores measured average wind direction in degrees
  //uint16_t avgWindDirection;
  int avgWindDirection;

  //! Variable: stores measured wind speed in m/s
  float windSpeed;

  //! Variable: stores measured average wind speed in m/s
  float avgWindSpeed;

  //! Variable: stores measured average wind gust direction in degrees
  uint16_t avgWindGustDirection;

  //! Variable: stores measured wind gust speed in m/s
  float avgWindGustSpeed;

  //! Variable: stores measured average corrected wind direction in degrees
  //uint16_t avgCorrectedWindDirection;
  int avgCorrectedWindDirection;

  //! Variable: stores measured wind sensor status
  char windSensorStatus[5];

  //! Variable: stores measured precipitation total in mm
  float precipTotal;

  //! Variable: stores measured precipitation intensity in mm
  float precipIntensity;

  //! Variable: stores measured precipitation status
  uint8_t precipStatus;
  //int precipStatus;

  //! Variable: stores measured compass in degrees
  //uint16_t compass;
  int compass;

  //! Variable: stores measured x tilt
  float xTilt;

  //! Variable: stores measured y tilt
  float yTilt;

  //! Variable: stores measured z orientation
  float zOrient;

  //! Variable: stores measured timestamp
  char timestamp[22];

  //! Variable: stores measured supply voltage
  float supplyVoltage;

  //! Variable: stores measured status
  char status[5];

  //! Variable: stores measured solar radiation in W/m²
  //uint16_t solarRadiation;
  float solarRadiation;

  //! Variable: stores measured sunshine hours
  float sunshineHours;

  //! Variable: stores measured sunrise time in h:min
  char sunriseTime[6];

  //! Variable: stores measured solar noon time in h:min
  char solarNoonTime[6];

  //! Variable: stores measured sunset time in h:min
  char sunsetTime[6];

  //! Variable: stores measured position of the sun in degrees:degrees
  char sunPosition[8];

  //! Variable: stores measured twilight civil time in h:min
  char twilightCivil[6];

  //! Variable: stores measured twilight nautical time in h:min
  char twilightNautical[6];

  //! Variable: stores measured twilight astronomical time in h:min
  char twilightAstronom[6];

  //! Variable: stores measured pressure in hPa
  float pressure;

  //! Variable: stores measured pressure at sea level in hPa
  float pressureSeaLevel;

  //! Variable: stores measured pressure at station level in hPa
  float pressureStation;

  //! Variable: stores measured relative humidity in %
  //uint16_t relativeHumidity;
  int relativeHumidity;

  //! Variable: stores measured  temperature in degrees celsius
  float temperature;

  //! Variable: stores measured dew point in degrees celsius
  float dewpoint;

  //! Variable: stores measured absolute humidity in gm-3
  float absoluteHumidity;

  //! Variable: stores measured air density in Kgm-3
  float airDensity;

  //! Variable: stores measured wet bulb temperature in degrees celsius
  float wetBulbTemperature;

  //! Variable: stores measured wind chill in celsius degrees
  float windChill;

  //! Variable: stores measured heat index in celsius degrees
  uint16_t heatIndex;


 // #ifdef GMX_GPS_OPTION

    //! Variable: stores measured  gps corrected speed in m/s
    float gpsCorrectedSpeed;

    //! Variable: stores measured  gps average corrected speed in m/s
    float gpsAvgCorrectedSpeed;

    //! Variable: stores measured gps corrected gust speed in m/s
    float gpsCorrectedGustSpeed;

    //! Variable: stores measured gps corrected gust direction in degrees
    uint16_t gpsCorrectedGustDirection;

    //! Variable: stores measured gps location
    char gpsLocation[29];

    //! Variable: stores measured gps heading in degrees
    uint16_t gpsHeading;

    //! Variable: stores measured gps speed in m/s
    float gpsSpeed;

    //! Variable: stores measured gps status
    char gpsStatus[5];

 // #endif
} weatherStationVector;

extern weatherStationVector gmx;

void weatherStation_ON();
uint8_t weatherStation_read();
//void debugPrint(char string[]);
void getData(char* response, int timeout);

#endif
