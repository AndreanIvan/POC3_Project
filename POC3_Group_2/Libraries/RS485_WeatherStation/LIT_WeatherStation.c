#include "LIT_WeatherStation.h"
#include "stm32wlxx_hal.h"
//#include "stm32wlxx_hal_uart.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "stdint.h"

#include "sys_app.h"
#include "main.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

#define BUFFER_SIZE	255

const unsigned char WS_Com1[1] = "9";
unsigned char WS_Resp1[BUFFER_SIZE];

const unsigned char WS_Com2[1] = "Q";
unsigned char WS_Resp2[BUFFER_SIZE];
unsigned char WS_Resp2_Formatted[BUFFER_SIZE];

unsigned char WS_Buffer;

char WS_Info_Buffer1[20];
char WS_Info_Buffer2[20];
char WS_Info_Buffer3[20];
char WS_Info_Buffer4[20];
char WS_Info_Buffer5[20];
char WS_Info_Buffer6[20];
char WS_Info_Buffer7[20];
char WS_Info_Buffer8[20];

char WS_Data_Buffer1[20];
char WS_Data_Buffer2[20];
char WS_Data_Buffer3[20];
char WS_Data_Buffer4[20];
char WS_Data_Buffer5[20];
char WS_Data_Buffer6[20];
char WS_Data_Buffer7[20];
char WS_Data_Buffer8[20];
char WS_Data_Buffer9[20];
char WS_Data_Buffer10[20];
char WS_Data_Buffer11[20];
char WS_Data_Buffer12[20];
char WS_Data_Buffer13[20];
char WS_Data_Buffer14[20];
char WS_Data_Buffer15[20];
char WS_Data_Buffer16[20];
char WS_Data_Buffer17[20];
char WS_Data_Buffer18[20];
char WS_Data_Buffer19[20];
char WS_Data_Buffer20[20];
char WS_Data_Buffer21[20];
char WS_Data_Buffer22[20];

const char info_format[] = "%s %s %s %s %s %s %s %s %s %s"; // Sensor info frame format
const char GMX_format[] = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s";

void WS_Read(void)
{
	memset(WS_Resp1, 0x00, sizeof(WS_Resp1));
	memset(WS_Resp2, 0x00, sizeof(WS_Resp2));

	memset(WS_Info_Buffer1, 0x00, sizeof(WS_Info_Buffer1));
	memset(WS_Info_Buffer2, 0x00, sizeof(WS_Info_Buffer2));
	memset(WS_Info_Buffer3, 0x00, sizeof(WS_Info_Buffer3));
	memset(WS_Info_Buffer4, 0x00, sizeof(WS_Info_Buffer4));
	memset(WS_Info_Buffer5, 0x00, sizeof(WS_Info_Buffer5));
	memset(WS_Info_Buffer6, 0x00, sizeof(WS_Info_Buffer6));
	memset(WS_Info_Buffer7, 0x00, sizeof(WS_Info_Buffer7));
	memset(WS_Info_Buffer8, 0x00, sizeof(WS_Info_Buffer8));

	memset(WS_Data_Buffer1, 0x00, sizeof(WS_Data_Buffer1));
	memset(WS_Data_Buffer2, 0x00, sizeof(WS_Data_Buffer2));
	memset(WS_Data_Buffer3, 0x00, sizeof(WS_Data_Buffer3));
	memset(WS_Data_Buffer4, 0x00, sizeof(WS_Data_Buffer4));
	memset(WS_Data_Buffer5, 0x00, sizeof(WS_Data_Buffer5));
	memset(WS_Data_Buffer6, 0x00, sizeof(WS_Data_Buffer6));
	memset(WS_Data_Buffer7, 0x00, sizeof(WS_Data_Buffer7));
	memset(WS_Data_Buffer8, 0x00, sizeof(WS_Data_Buffer8));
	memset(WS_Data_Buffer9, 0x00, sizeof(WS_Data_Buffer9));
	memset(WS_Data_Buffer10, 0x00, sizeof(WS_Data_Buffer10));
	memset(WS_Data_Buffer11, 0x00, sizeof(WS_Data_Buffer11));
	memset(WS_Data_Buffer12, 0x00, sizeof(WS_Data_Buffer12));
	memset(WS_Data_Buffer13, 0x00, sizeof(WS_Data_Buffer13));
	memset(WS_Data_Buffer14, 0x00, sizeof(WS_Data_Buffer14));
	memset(WS_Data_Buffer15, 0x00, sizeof(WS_Data_Buffer15));
	memset(WS_Data_Buffer16, 0x00, sizeof(WS_Data_Buffer16));
	memset(WS_Data_Buffer17, 0x00, sizeof(WS_Data_Buffer17));
	memset(WS_Data_Buffer18, 0x00, sizeof(WS_Data_Buffer18));
	memset(WS_Data_Buffer19, 0x00, sizeof(WS_Data_Buffer19));
	memset(WS_Data_Buffer20, 0x00, sizeof(WS_Data_Buffer20));
	memset(WS_Data_Buffer21, 0x00, sizeof(WS_Data_Buffer21));
	memset(WS_Data_Buffer22, 0x00, sizeof(WS_Data_Buffer22));

//		sprintf(WS_Com1, "9");
//		sprintf(WS_Com2, 'Q');

	HAL_UART_Transmit(&huart1, (uint8_t *)"9", strlen("9"), 1000);
	HAL_UART_Receive(&huart1, WS_Resp1, 255, 1000);

	APP_PRINTF("WS Command 1     : %s\r\n", "9");
	APP_PRINTF("WS Resp. 1       : %s\r\n", WS_Resp1);

	sscanf (WS_Resp1, info_format, WS_Info_Buffer1, WS_Info_Buffer2,
			WS_Info_Buffer3, WS_Info_Buffer4, WS_Info_Buffer5,
			WS_Info_Buffer6, WS_Info_Buffer7, WS_Info_Buffer8);

	APP_PRINTF("WS_Info_Buffer1: %s\r\n", WS_Info_Buffer1);
	APP_PRINTF("WS_Info_Buffer2: %s\r\n", WS_Info_Buffer2);
	APP_PRINTF("WS_Info_Buffer3: %s\r\n", WS_Info_Buffer3);
	APP_PRINTF("WS_Info_Buffer4: %s\r\n", WS_Info_Buffer4);
	APP_PRINTF("WS_Info_Buffer5: %s\r\n", WS_Info_Buffer5);
	APP_PRINTF("WS_Info_Buffer6: %s\r\n", WS_Info_Buffer6);
	APP_PRINTF("WS_Info_Buffer7: %s\r\n", WS_Info_Buffer7);
	APP_PRINTF("WS_Info_Buffer8: %s\r\n", WS_Info_Buffer8);

	HAL_UART_Transmit(&huart1, (uint8_t *)"Q", strlen("Q"), 1000);
	HAL_UART_Receive(&huart1, WS_Resp2, 255, 1000);

	APP_PRINTF("WS Command 2     : %s\r\n", "Q");
	APP_PRINTF("WS Resp. 2       : %s\r\n", WS_Resp2);

	for ( int i = 0; i < BUFFER_SIZE; i++)
	{
		if (WS_Resp2[i] == ',')
			WS_Resp2_Formatted[i] = ' ';
		else
			WS_Resp2_Formatted[i] = WS_Resp2[i];
	}

	APP_PRINTF("WS Resp. 2 Fmtd. : %s\r\n", WS_Resp2_Formatted);

	sscanf (WS_Resp2_Formatted, GMX_format, WS_Data_Buffer1, WS_Data_Buffer2, WS_Data_Buffer3, WS_Data_Buffer4, WS_Data_Buffer5,
			WS_Data_Buffer6, WS_Data_Buffer7, WS_Data_Buffer8, WS_Data_Buffer9, WS_Data_Buffer10, WS_Data_Buffer11,
			WS_Data_Buffer12, WS_Data_Buffer13, WS_Data_Buffer14, WS_Data_Buffer15, WS_Data_Buffer16, WS_Data_Buffer17,
			WS_Data_Buffer18, WS_Data_Buffer19, WS_Data_Buffer20, WS_Data_Buffer21, WS_Data_Buffer22
			);

	APP_PRINTF("WS_Data_Buffer1: %s\r\n", WS_Data_Buffer1);
	APP_PRINTF("WS_Data_Buffer2: %s\r\n", WS_Data_Buffer2);
	APP_PRINTF("WS_Data_Buffer3: %s\r\n", WS_Data_Buffer3);
	APP_PRINTF("WS_Data_Buffer4: %s\r\n", WS_Data_Buffer4);
	APP_PRINTF("WS_Data_Buffer5: %s\r\n", WS_Data_Buffer5);
	APP_PRINTF("WS_Data_Buffer6: %s\r\n", WS_Data_Buffer6);
	APP_PRINTF("WS_Data_Buffer7: %s\r\n", WS_Data_Buffer7);
	APP_PRINTF("WS_Data_Buffer8: %s\r\n", WS_Data_Buffer8);
	APP_PRINTF("WS_Data_Buffer9: %s\r\n", WS_Data_Buffer9);
	APP_PRINTF("WS_Data_Buffer10: %s\r\n", WS_Data_Buffer10);
	APP_PRINTF("WS_Data_Buffer11: %s\r\n", WS_Data_Buffer11);
	APP_PRINTF("WS_Data_Buffer12: %s\r\n", WS_Data_Buffer12);
	APP_PRINTF("WS_Data_Buffer13: %s\r\n", WS_Data_Buffer13);
	APP_PRINTF("WS_Data_Buffer14: %s\r\n", WS_Data_Buffer14);
	APP_PRINTF("WS_Data_Buffer15: %s\r\n", WS_Data_Buffer15);
	APP_PRINTF("WS_Data_Buffer16: %s\r\n", WS_Data_Buffer16);
	APP_PRINTF("WS_Data_Buffer17: %s\r\n", WS_Data_Buffer17);
	APP_PRINTF("WS_Data_Buffer18: %s\r\n", WS_Data_Buffer18);
	APP_PRINTF("WS_Data_Buffer19: %s\r\n", WS_Data_Buffer19);
	APP_PRINTF("WS_Data_Buffer20: %s\r\n", WS_Data_Buffer20);
	APP_PRINTF("WS_Data_Buffer21: %s\r\n", WS_Data_Buffer21);
	APP_PRINTF("WS_Data_Buffer22: %s\r\n", WS_Data_Buffer22);
}

char *replace_char(char str[], char find, char replace){
	int i = 0;
	char str_rep[255];

	while (str[i] != NULL)
	{
		if(str[i] == ',')
			str_rep[i] = ' ';
		else
			str_rep[i] = str[i];
	}

    return str_rep;
}
