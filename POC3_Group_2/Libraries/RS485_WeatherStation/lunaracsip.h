#ifndef LUNARACSIP_H_
#define LUNARACSIP_H_

/* Class */
#define LORA_CLASS_A 0
#define LORA_CLASS_C 1

#define OTAA        0
#define ABP         1

#define NOT_INTERRUPTIBLE 0
#define INTERRUPTIBLE     1

#define UNCONFIRMED_TX	0
#define CONFIRMED_TX	1

#define STATUS_OK  ">> Ok"
#define STATUS_ACCEPTED	">> accepted"
#define STATUS_TX_OK  ">> tx_ok"

/* Sesuaikan dengan STM32 yang digunakan */
#include "stm32wlxx_hal.h"

/* Untuk tipe data bool */
#include <stdbool.h>

/* Sesuaikan dengan pin UART ACSIP yang digunakan di STM32CubeMX (.ioc) */
#define	_ACSIP_UART	huart1
#define _STM_UART huart2

/* Get Version */
void sip_get_version(char* response, int timeout);

void sip_set_log_debug(char* response, int timeout);

/* Get Band */
void mac_get_band(char* response, int timeout);

void mac_get_ch_para(char* response, int ch, int timeout);

void mac_set_ch_status(char* response, char* status, int ch, int timeout);

void mac_get_ch_status(char* response, int ch, int timeout);

void mac_set_ch_freq(char* response, int ch_id, long int freq, int timeout);

void mac_set_ch_dr_range(char* response, int ch_id, int min_dr, int max_dr, int timeout);

void mac_set_rx2(char* response, int dr, long int freq, int timeout);

void mac_get_rx2(char* response, int timeout);

void mac_set_join_ch(char* response, char* status, int ch, int timeout);

void mac_get_join_ch(char* response, int timeout);

bool mac_set_power(int power_dBm);

bool is_valid_power_dBm(int x);

bool mac_set_adr(bool adr);

bool mac_set_dr(int rate);

bool is_valid_rate(int x);

void mac_get_power(char* response, int timeout);

void mac_get_dr(char* response, int timeout);

void mac_get_join_status(char* response, int timeout);

/* Get Class */
void mac_get_class(char* response, int timeout);

/* Mac Save */
void mac_save(char* response, int timeout);

/* Mac Set Class */
bool mac_set_class(int lora_class, int timeout);

void mac_get_deveui(char* response, int timeout);

void mac_get_appeui(char* response, int timeout);

void mac_get_appkey(char* response, int timeout);

/* Init OTAA */
bool initOTAA(char* devEUI, char* appEUI, char* appKEY, int timeout);

/* Join dengan OTAA atau ABP */
bool joinLoRaNetwork(int mode, char* response, int timeout);

void receiveData(char* response, int timeout);

bool acceptResponseJoin(char* response, int timeout);

bool mac_sendData(int type, int port, char* datahex, char* response);

bool acceptTxResponse(char* response, int timeout);

void acceptResponse(char* response, int timeout);
/* Factory Reset
 * Gunakan versi with_response jika ingin mendapat string response */
void sip_factory_reset(int timeout);
void sip_factory_reset_with_response(char* response, int timeout);

/* Get HW DEVEUI */
void sip_get_hw_deveui(char* response,int timeout);

/* Get HW Model */
void sip_get_hw_model(char* response, int timeout);

/* Reset
 * Gunakan versi with_response jika ingin mendapat string response */
void sip_reset(int timeout);
void sip_reset_with_response(char* response, int timeout);

/* Sleep
 * Parameter time diisi angka 10 - 604800 (dalam sekon)
 * Parameter interruptible diisi 1 jika ACSIP ingin diinterupsi melalui UART saat sleep
 * dan 0 jika ACSIP tidak ingin diinterupsi melalui UART saat sleep
 * Gunakan versi with_response jika ingin mendapat string response */
void sip_sleep(int time, int interruptible, int timeout);
void sip_sleep_with_response(int time, int interruptible, char* response, int timeout);



/* Wake up */
void sip_wakeup(char* response);

bool compare_string(char str[],char word[]);

void debugPrint(char string[]);

//void debugPrint2(char string[]);

bool strContains(char* string, char* toFind);

#endif
