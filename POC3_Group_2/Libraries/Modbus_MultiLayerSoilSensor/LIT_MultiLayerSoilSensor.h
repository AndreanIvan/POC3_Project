/**
  ******************************************************************************
  * @file           : LIT_MultilayerSoilSensor.h
  * @brief          : Header file for RK520-04 Multilayer Conduit Type Soil
  *                   Temperature and Humidity Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_MULTI_LAYER_SOIL_SENSOR_H__
#define __LIT_MULTI_LAYER_SOIL_SENSOR_H__

/* Includes ------------------------------------------------------------------*/
#include <inttypes.h>

/* Exported constants --------------------------------------------------------*/
#define MULTILAYER_SOIL_READY_DURATION_MS         12000
#define SENSOR_READY_DURATION_MS                  MULTILAYER_SOIL_READY_DURATION_MS

/* Exported types ------------------------------------------------------------*/
typedef struct
{
  uint16_t Moisture1;
  uint16_t Moisture2;
  uint16_t Moisture3;
  uint16_t Moisture4;
  uint16_t Temp1;
  uint16_t Temp2;
  uint16_t Temp3;
  uint16_t Temp4;

} MultilayerSoil_t;

/* Exported functions prototypes ---------------------------------------------*/
void MultilayerSoilSensor_Init( void );
void MultilayerSoilSensor_Read( MultilayerSoil_t *object );

#endif /* __LIT_MULTI_LAYER_SOIL_SENSOR_H__ */
