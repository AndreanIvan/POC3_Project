/**
  ******************************************************************************
  * @file           : LIT_MultilayerSoilSensor.c
  * @brief          : Source file for RK520-04 Multilayer Conduit Type Soil
  *                   Temperature and Humidity Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include "LIT_MultilayerSoilSensor.h"
#include "LIT_ModbusMaster.h"
#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "stm32wlxx.h"
#include "math.h"

//#include "sys_app.h"

/* Private constants ----------------------------------------------------------*/
#define baud							9600
#define timeout							1500
#define polling							1000
#define retry_count						10
#define LED								9
#define TOTAL_NO_OF_REGISTERS			100

/* Private types -------------------------------------------------------------*/
enum
{
  READ_REGISTER,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

/* Private variables ---------------------------------------------------------*/
// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

/* Exported functions ---------------------------------------------------------*/
void MultilayerSoilSensor_Init( void )
{
   /*
   * Construct Packet untuk setiap req yang ada
   * - packet
   * - slave address
   * - function
   * - register address
   * - unsigned int count
   * - local reg address
   */
  modbus_construct(&packets[READ_REGISTER], 1, READ_HOLDING_REGISTERS, 0, 8, 0);

   /*
	* Konfigurasi pengiriman
	* - Baud Rate
	* - Timeout (ms)
	* - polling time (ms)
	* - retry count
	* - packets struct
	* - total packet number
	* - local register variable
	*/
  modbus_configure(baud, timeout, polling, retry_count, packets, TOTAL_NO_OF_PACKETS, regs);
}

void MultilayerSoilSensor_Read( MultilayerSoil_t *object )
{
	memset(regs, 0x00, sizeof(regs));

	modbus_update();

	Modbus_PRINTF(2, "%d",packets[0].id);
	Modbus_PRINTF(2, "%d",packets[0].function);
	Modbus_PRINTF(2, "%d",packets[0].address);
	Modbus_PRINTF(2, "%d",packets[0].data);
	Modbus_PRINTF(2, "%d\n",packets[0].local_start_address);

	Modbus_PRINTF(2, "%d",packets[0].requests);
	Modbus_PRINTF(2, "%d",packets[0].successful_requests);
	Modbus_PRINTF(2, "%d",packets[0].failed_requests);
	Modbus_PRINTF(2, "%d",packets[0].exception_errors);
	Modbus_PRINTF(2, "%d",packets[0].retries);
	Modbus_PRINTF(2, "%d\n",packets[0].connection);

	object->Moisture1 = regs[0];
	object->Temp1 = regs[1];
	object->Moisture2 = regs[2];
	object->Temp2 = regs[3];
	object->Moisture3 = regs[4];
	object->Temp3 = regs[5];
	object->Moisture4 = regs[6];
	object->Temp4 = regs[7];

	Modbus_PRINTF(1, "Moisture1: %d%% | ", regs[0]);
	Modbus_PRINTF(1, "Temp1: %d.%d °C | ", regs[1] / 10, regs[1] % 10);
	Modbus_PRINTF(1, "Moisture2: %d%% | ", regs[2]);
	Modbus_PRINTF(1, "Temp2: %d.%d °C | ", regs[3] / 10, regs[3] % 10);
	Modbus_PRINTF(1, "Moisture3: %d%% | ", regs[4]);
	Modbus_PRINTF(1, "Temp3: %d.%d °C | ", regs[5] / 10, regs[5] % 10);
	Modbus_PRINTF(1, "Moisture4: %d%% | ", regs[6]);
	Modbus_PRINTF(1, "Temp4: %d.%d °C\r\n", regs[7] / 10, regs[7] % 10);
}
