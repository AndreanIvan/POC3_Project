/**
  ******************************************************************************
  * @file           : LIT_WaterLevelSensor.h
  * @brief          : Header file for RKL-02 Radar Liquid Level Transmitter Library
  * @date           : January 2022
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_WATER_LEVEL_SENSOR_H__
#define __LIT_WATER_LEVEL_SENSOR_H__

/* Includes ------------------------------------------------------------------*/
#include <inttypes.h>

/* Exported constants --------------------------------------------------------*/
#define WATER_LEVEL_READY_DURATION_MS             25000
#define SENSOR_READY_DURATION_MS                  WATER_LEVEL_READY_DURATION_MS

/* Exported types ------------------------------------------------------------*/
typedef struct
{
  uint16_t Distance;

} WaterLevel_t;

/* Exported functions prototypes ---------------------------------------------*/
void WaterLevelSensor_Init( void );
void WaterLevelSensor_Read( WaterLevel_t *object );

#endif /* __LIT_WATER_LEVEL_SENSOR_H__ */