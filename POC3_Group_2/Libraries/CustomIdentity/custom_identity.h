/**
  ******************************************************************************
  * @file           : custom_identity.h
  * @brief          : Identity definition for POC 3 Project
  * @author         : Andrean I.
  * @date           : December 2021
  ******************************************************************************
  */

#ifndef __CUSTOM_IDENTITY_H__
#define __CUSTOM_IDENTITY_H__

/* Device EUI ------------------------------------------------------------------------------------------ */

/*** Group 1 ***/
#define POC3_Test_Node_DEV_EUI                      { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77 }
#define POC3_SolenoidValve_Board_001_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x01 }
#define POC3_SolenoidValve_Board_002_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x02 }
#define POC3_SolenoidValve_Board_003_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x03 }
#define POC3_Waterflow_Board_001_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x01 }
#define POC3_Waterflow_Board_002_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x02 }
#define POC3_Waterflow_Board_003_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x03 }
#define POC3_Pressure_Board_001_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x01 }
#define POC3_Pressure_Board_002_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x02 }
#define POC3_Pressure_Board_003_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x03 }
#define POC3_WatermarkVWC_Board_001_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x01 }
#define POC3_WatermarkVWC_Board_002_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x02 }
#define POC3_WatermarkVWC_Board_003_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x03 }

/*** Group 2 ***/
#define POC3_WaterLevel_Board_001_DEV_EUI           { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x06, 0x00, 0x01 }
#define POC3_WaterLevel_Board_002_DEV_EUI           { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x06, 0x00, 0x02 }
#define POC3_WaterLevel_Board_003_DEV_EUI           { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x06, 0x00, 0x03 }
#define POC3_WeatherStation_Board_001_DEV_EUI       { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x07, 0x00, 0x01 }
#define POC3_WeatherStation_Board_002_DEV_EUI       { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x07, 0x00, 0x02 }
#define POC3_WeatherStation_Board_003_DEV_EUI       { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x07, 0x00, 0x03 }
#define POC3_MultiLayerSoilSensor_Board_001_DEV_EUI { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x08, 0x00, 0x01 }
#define POC3_MultiLayerSoilSensor_Board_002_DEV_EUI { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x08, 0x00, 0x02 }
#define POC3_MultiLayerSoilSensor_Board_003_DEV_EUI { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x08, 0x00, 0x03 }
#define POC3_Turbidity_Board_001_DEV_EUI            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x09, 0x00, 0x01 }
#define POC3_Turbidity_Board_002_DEV_EUI            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x09, 0x00, 0x02 }
#define POC3_Turbidity_Board_003_DEV_EUI            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x09, 0x00, 0x03 }
#define POC3_Evaporation_Board_001_DEV_EUI          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x10, 0x00, 0x01 }
#define POC3_Evaporation_Board_002_DEV_EUI          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x10, 0x00, 0x02 }
#define POC3_Evaporation_Board_003_DEV_EUI          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x10, 0x00, 0x03 }
#define POC3_TestingMPTT_Board_DEV_EUI	        	  { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x00, 0x01 }
#define POC3_Node_Testing_RS485_Board_DEV_EUI       { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x11, 0x01 }

/* Application Key  ------------------------------------------------------------------------------------ */

/*** Group 1 ***/
#define POC3_Test_Node_APP_KEY                       00,11,22,33,44,55,66,77,88,99,aa,bb,cc,dd,ee,ff
#define POC3_SolenoidValve_Board_001_APP_KEY         77,f2,68,91,00,63,d6,1d,00,03,c4,e0,4a,bd,b8,a3
#define POC3_SolenoidValve_Board_002_APP_KEY         1d,21,29,9d,2b,92,24,99,1a,8b,62,af,31,36,f7,c5
#define POC3_SolenoidValve_Board_003_APP_KEY         94,bb,58,00,42,64,e1,cf,99,db,79,80,b0,c8,b0,61
#define POC3_Waterflow_Board_001_APP_KEY             3d,57,86,fb,e8,39,5a,88,e2,e3,45,47,f2,10,40,12
#define POC3_Waterflow_Board_002_APP_KEY             46,18,bf,76,a0,62,97,12,a3,6c,3f,58,a8,68,5a,a1
#define POC3_Waterflow_Board_003_APP_KEY             a5,78,a0,96,f6,fd,e4,13,49,53,7f,7b,88,07,30,7d
#define POC3_Pressure_Board_001_APP_KEY              9d,75,61,a7,30,cd,af,84,2b,dc,d7,81,a1,7a,eb,52
#define POC3_Pressure_Board_002_APP_KEY              c2,7c,30,8e,1f,aa,9b,a3,47,4d,73,34,9b,93,f9,71
#define POC3_Pressure_Board_003_APP_KEY              66,62,1b,46,15,d9,7c,e3,15,a8,95,5a,11,ae,97,da
#define POC3_WatermarkVWC_Board_001_APP_KEY          0b,21,c0,25,50,1e,8f,7f,90,21,f1,a4,47,94,9c,4a
#define POC3_WatermarkVWC_Board_002_APP_KEY          17,fa,c5,85,9a,83,37,80,f8,d8,96,81,4c,51,e9,a0
#define POC3_WatermarkVWC_Board_003_APP_KEY          b6,57,d3,45,79,d3,16,d0,4e,1a,9d,17,50,b0,da,ef
#define POC3_TestingMPTT_Board_APP_KEY          	   82,c8,c7,f0,c7,d3,fb,d9,b8,9a,91,2b,6d,31,a6,b9

/*** Group 2 ***/
#define POC3_WaterLevel_Board_001_APP_KEY            96,46,c8,7f,80,27,00,e8,3c,5b,e0,54,bd,ef,2e,85
#define POC3_WaterLevel_Board_002_APP_KEY            
#define POC3_WaterLevel_Board_003_APP_KEY            
#define POC3_WeatherStation_Board_001_APP_KEY        6f,b5,a8,80,93,39,2e,b5,45,d9,03,90,86,39,c5,99
#define POC3_WeatherStation_Board_002_APP_KEY        
#define POC3_WeatherStation_Board_003_APP_KEY        
#define POC3_MultiLayerSoilSensor_Board_001_APP_KEY  ac,ca,59,9f,f6,2f,e9,45,8b,60,89,64,40,35,b4,e1
#define POC3_MultiLayerSoilSensor_Board_002_APP_KEY  
#define POC3_MultiLayerSoilSensor_Board_003_APP_KEY  
#define POC3_Turbidity_Board_001_APP_KEY             59,a6,cd,a1,53,dd,d5,b1,6e,da,1a,27,96,18,9d,f1
#define POC3_Turbidity_Board_002_APP_KEY             
#define POC3_Turbidity_Board_003_APP_KEY             
#define POC3_Evaporation_Board_001_APP_KEY           ab,1d,de,c0,81,a6,3b,17,e8,b7,54,e8,48,6c,b8,db
#define POC3_Evaporation_Board_002_APP_KEY           
#define POC3_Evaporation_Board_003_APP_KEY           
#define POC3_Node_Testing_RS485_Board_APP_KEY        F4,F2,78,37,5B,2E,BB,07,3D,D0,52,42,3A,6E,BF,76

/* Device Intro String --------------------------------------------------------------------------------- */

/*** Group 1 ***/
#define POC3_Test_Node_String                        "POC 3 Node - Test Node"
#define POC3_SolenoidValve_Board_001_String          "POC 3 Node - Solenoid Valve 001"
#define POC3_SolenoidValve_Board_002_String          "POC 3 Node - Solenoid Valve 002"
#define POC3_SolenoidValve_Board_003_String          "POC 3 Node - Solenoid Valve 003"
#define POC3_Waterflow_Board_001_String              "POC 3 Node - Waterflow Meter 001"
#define POC3_Waterflow_Board_002_String              "POC 3 Node - Waterflow Meter 002"
#define POC3_Waterflow_Board_003_String              "POC 3 Node - Waterflow Meter 003"
#define POC3_Pressure_Board_001_String               "POC 3 Node - Water Pressure Sensor 001"
#define POC3_Pressure_Board_002_String               "POC 3 Node - Water Pressure Sensor 002"
#define POC3_Pressure_Board_003_String               "POC 3 Node - Water Pressure Sensor 003"
#define POC3_WatermarkVWC_Board_001_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 001"
#define POC3_WatermarkVWC_Board_002_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 002"
#define POC3_WatermarkVWC_Board_003_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 003"
#define POC3_TestingMPTT_Board_String	          	   "POC 3 Node - MPPT Charging Test"

/*** Group 2 ***/
#define POC3_WaterLevel_Board_001_String             "POC 3 Node - Water Level Sensor 001"
#define POC3_WaterLevel_Board_002_String             "POC 3 Node - Water Level Sensor 002"
#define POC3_WaterLevel_Board_003_String             "POC 3 Node - Water Level Sensor 003"
#define POC3_WeatherStation_Board_001_String         "POC 3 Node - Weather Station 001"
#define POC3_WeatherStation_Board_002_String         "POC 3 Node - Weather Station 002"
#define POC3_WeatherStation_Board_003_String         "POC 3 Node - Weather Station 003"
#define POC3_MultiLayerSoilSensor_Board_001_String   "POC 3 Node - Multi Layer Soil Sensor 001"
#define POC3_MultiLayerSoilSensor_Board_002_String   "POC 3 Node - Multi Layer Soil Sensor 002"
#define POC3_MultiLayerSoilSensor_Board_003_String   "POC 3 Node - Multi Layer Soil Sensor 003"
#define POC3_Turbidity_Board_001_String              "POC 3 Node - Turbidity Sensor 001"
#define POC3_Turbidity_Board_002_String              "POC 3 Node - Turbidity Sensor 002"
#define POC3_Turbidity_Board_003_String              "POC 3 Node - Turbidity Sensor 003"
#define POC3_Evaporation_Board_001_String            "POC 3 Node - Evaporation Sensor 001"
#define POC3_Evaporation_Board_002_String            "POC 3 Node - Evaporation Sensor 002"
#define POC3_Evaporation_Board_003_String            "POC 3 Node - Evaporation Sensor 003"
#define POC3_Node_Testing_RS485_Board_String         "POC 3 Node - Testing RS485 Board"

#endif /* __CUSTOM_IDENTITY_H__ */