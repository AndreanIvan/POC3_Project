/**
  ******************************************************************************
  * @file           : LIT_EvaporationSensor.c
  * @brief          : Source file for for RK400-10 Evaporation Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include "LIT_EvaporationSensor.h"
#include "LIT_ModbusMaster.h"
#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "stm32wlxx.h"
#include "math.h"

/* Private constants ----------------------------------------------------------*/
#define baud							9600
#define timeout							1500
#define polling							1000
#define retry_count						10
#define LED								9
#define TOTAL_NO_OF_REGISTERS			100

/* Private types -------------------------------------------------------------*/
enum
{
  READ_REGISTER,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

/* Private variables ---------------------------------------------------------*/
// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

/* Exported functions ---------------------------------------------------------*/
void EvaporationSensor_Init( void )
{
    /*
   * Construct Packet untuk setiap req yang ada
   * - packet
   * - slave address
   * - function
   * - register address
   * - unsigned int count
   * - local reg address
   */
  modbus_construct(&packets[READ_REGISTER], 1, READ_HOLDING_REGISTERS, 0, 1, 0);

   /*
	* Konfigurasi pengiriman
	* - Baud Rate
	* - Timeout (ms)
	* - polling time (ms)
	* - retry count
	* - packets struct
	* - total packet number
	* - local register variable
	*/
  modbus_configure(baud, timeout, polling, retry_count, packets, TOTAL_NO_OF_PACKETS, regs);
}

uint8_t EvaporationSensor_Read( Evaporation_t *object )
{
	memset(regs, 0x00, sizeof(regs));

	modbus_update();

	Modbus_PRINTF(2, "%d",packets[0].id);
	Modbus_PRINTF(2, "%d",packets[0].function);
	Modbus_PRINTF(2, "%d",packets[0].address);
	Modbus_PRINTF(2, "%d",packets[0].data);
	Modbus_PRINTF(2, "%d\n",packets[0].local_start_address);

	Modbus_PRINTF(2, "%d",packets[0].requests);
	Modbus_PRINTF(2, "%d",packets[0].successful_requests);
	Modbus_PRINTF(2, "%d",packets[0].failed_requests);
	Modbus_PRINTF(2, "%d",packets[0].exception_errors);
	Modbus_PRINTF(2, "%d",packets[0].retries);
	Modbus_PRINTF(2, "%d\n",packets[0].connection);

	object->evaporation = regs[0];

	Modbus_PRINTF(1, "Evaporation: %d.%d mm\r\n", (uint16_t)(regs[0] / 10), (uint16_t)(regs[0] % 10));

	if ( packets[0].successful_requests == 0 )
		return 1;
	else
		return 0;

}
