/**
  ******************************************************************************
  * @file           : LIT_EvaporationSensor.h
  * @brief          : Header file for RK400-10 Evaporation Sensor Library
  * @date           : January 2022
  ******************************************************************************
  */
 
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_EVAPORATION_SENSOR_H__
#define __LIT_EVAPORATION_SENSOR_H__

/* Includes ------------------------------------------------------------------*/
#include <inttypes.h>

/* Exported constants --------------------------------------------------------*/
#define EVAPORATION_READY_DURATION_MS             1000
#define SENSOR_READY_DURATION_MS                  EVAPORATION_READY_DURATION_MS

/* Exported types ------------------------------------------------------------*/
typedef struct
{
  uint16_t evaporation;

} Evaporation_t;

/* Exported functions prototypes ---------------------------------------------*/
void EvaporationSensor_Init( void );
uint8_t EvaporationSensor_Read( Evaporation_t *object );

#endif /* __LIT_EVAPORATION_SENSOR_H__ */
