#ifndef __LIT_MPPT_TEST_H__
#define __LIT_MPPT_TEST_H__

#include "main.h"

#define SOLAR_ADC_CHANNEL				ADC_CHANNEL_10
#define	STEP_UP_12V_ADC_CHANNEL			ADC_CHANNEL_11
#define BATT_ADC_CHANNEL				ADC_CHANNEL_4
#define ICHARGE_ADC_CHANNEL				ADC_CHANNEL_2

#ifndef RS485_EN_GPIO_Port
#define RS485_EN_GPIO_Port          	GPIOA
#endif

#ifndef RS485_EN_Pin
#define RS485_EN_Pin                	GPIO_PIN_8
#endif

void RS485_Init(void);

void RS485_On(void);

void RS485_Off(void);

uint32_t MPPT_GetSolarVoltage(void);
uint32_t MPPT_GetStepUpVoltage(void);
uint32_t MPPT_GetBattVoltage(void);
uint32_t MPPT_GetIChargeVoltage(void);

uint32_t MPPT_GetIChargeCurrent(void);

#endif /* __LIT_MPPT_TEST_H__ */
