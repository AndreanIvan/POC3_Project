/* Includes -------------------------------------------------------------------*/
#include "LIT_MPPT_Test.h"
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "adc_if.h"
#include "sys_app.h"

/* External variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef hadc;
extern UART_HandleTypeDef 					huart2;

/* Private constants ----------------------------------------------------------*/
#define NUMBER_OF_SAMPLE					15
#define MAX_ADC_VALUE                       4095
#define MPPT_LOG_LEVEL                      3

/* Private function prototypes -----------------------------------------------*/
static void MPPT_PRINTF(uint8_t verbose, const char *format, ...);
static void GetBattery(void);
static uint32_t ADC_GetADC(uint32_t channel);
static uint32_t ADC_ReadChannels(uint32_t channel);
static uint16_t meanOf(uint16_t arr[]);

/* Private variables ---------------------------------------------------------*/
uint16_t BatteryLevel = 0;

/* Exported functions ---------------------------------------------------------*/

void RS485_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RS485_EN_GPIO_Port, RS485_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : RS485_EN_Pin */
  GPIO_InitStruct.Pin = RS485_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RS485_EN_GPIO_Port, &GPIO_InitStruct);

  RS485_On();
}

void RS485_On(void)
{
    HAL_GPIO_WritePin(RS485_EN_GPIO_Port, RS485_EN_Pin, GPIO_PIN_SET);
}

void RS485_Off(void)
{
    HAL_GPIO_WritePin(RS485_EN_GPIO_Port, RS485_EN_Pin, GPIO_PIN_RESET);
}

uint32_t MPPT_GetSolarVoltage(void)
{
	MPPT_PRINTF(1, "Get Solar Voltage: %d\r\n", 1);
    uint32_t SolarADC;
    uint32_t SolarVoltage;
    uint16_t R1 = 3300, R2 = 500;

    GetBattery();

    SolarADC = ADC_GetADC(SOLAR_ADC_CHANNEL);
    SolarVoltage = (uint32_t)(SolarADC * BatteryLevel / MAX_ADC_VALUE * (R1 + R2) / R2);

    MPPT_PRINTF(3, "Batt Level : %d | ", BatteryLevel);
    MPPT_PRINTF(2, "ADC Value : %d | ", SolarADC);
    MPPT_PRINTF(1, "Solar Voltage : %d\r\n", SolarVoltage);

    return SolarVoltage;
}

uint32_t MPPT_GetStepUpVoltage(void)
{
    uint32_t StepUpADC;
    uint32_t StepUpVoltage;
    uint16_t R1 = 3300, R2 = 1000;

    GetBattery();

    StepUpADC = ADC_GetADC(STEP_UP_12V_ADC_CHANNEL);
    StepUpVoltage = (uint32_t)(StepUpADC * BatteryLevel / MAX_ADC_VALUE * (R1 + R2) / R2);

    MPPT_PRINTF(3, "Batt Level : %d | ", BatteryLevel);
    MPPT_PRINTF(2, "ADC Value : %d | ", StepUpADC);
    MPPT_PRINTF(1, "StepUp Voltage : %d\r\n", StepUpVoltage);

    return StepUpVoltage;
}

uint32_t MPPT_GetBattVoltage(void)
{
    uint32_t BattADC;
    uint32_t BattVoltage;
    uint16_t R1 = 1000, R2 = 3300;

    GetBattery();

    BattADC = ADC_GetADC(BATT_ADC_CHANNEL);
    BattVoltage = (uint32_t)(BattADC * BatteryLevel / MAX_ADC_VALUE * (R1 + R2) / R2);

    MPPT_PRINTF(3, "Batt Level : %d | ", BatteryLevel);
    MPPT_PRINTF(2, "ADC Value : %d | ", BattADC);
    MPPT_PRINTF(1, "Batt Voltage : %d\r\n", BattVoltage);

    return BattVoltage;
}

uint32_t MPPT_GetIChargeVoltage(void)
{
    uint32_t IChargeADC;
    uint32_t IChargeVoltage;

    GetBattery();

    IChargeADC = ADC_GetADC(ICHARGE_ADC_CHANNEL);
    IChargeVoltage = (uint32_t)(IChargeADC * BatteryLevel / MAX_ADC_VALUE);

    MPPT_PRINTF(3, "Batt Level : %d | ", BatteryLevel);
    MPPT_PRINTF(2, "ADC Value : %d | ", IChargeADC);
    MPPT_PRINTF(1, "ICharge Voltage : %d\r\n", IChargeVoltage);

    return IChargeVoltage;
}

uint32_t MPPT_GetIChargeCurrent(void)
{
    uint32_t IChargeVoltage;
    uint32_t IChargeCurrent;

    IChargeVoltage = MPPT_GetIChargeVoltage();
    IChargeCurrent = (uint32_t)(IChargeVoltage * 470.66 / 1000 - 6.7441);

    MPPT_PRINTF(1, "ICharge Current : %d\r\n", IChargeCurrent);

    return IChargeCurrent;
}

/* Private functions ---------------------------------------------------------*/
void MPPT_PRINTF(uint8_t verbose, const char *format, ...)
{
	if (MPPT_LOG_LEVEL >= verbose)
	{
		static char ll_msg_buf_[127];
	    va_list args;
	    size_t len;

	    /* Format the string */
	    __builtin_va_start(args, format);
	    len = vsnprintf(ll_msg_buf_, 127, &format[0], args);
	    __builtin_va_end(args);
	    HAL_UART_Transmit(&huart2, (uint8_t *)&ll_msg_buf_, len, 0xFFFF);
	}
}

void GetBattery(void)
{
    uint16_t batt_level[NUMBER_OF_SAMPLE];

    for (int i = 0; i < NUMBER_OF_SAMPLE; i++)
    {
        batt_level[i] = SYS_GetBatteryLevel();
    }

    BatteryLevel = meanOf(batt_level);
}

uint32_t ADC_GetADC(uint32_t channel)
{
    uint16_t ADC_Sampled[NUMBER_OF_SAMPLE];
    uint16_t ADC_Measured;

    /* Read ADC */
    for (int i = 0; i < NUMBER_OF_SAMPLE; i++)
    {
    	ADC_Sampled[i] = ADC_ReadChannels(channel);
    	HAL_Delay(10);
    }

    ADC_Measured = meanOf(ADC_Sampled);

    return ADC_Measured;
}

uint32_t ADC_ReadChannels(uint32_t channel)
{
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;

}

uint16_t meanOf(uint16_t arr[])
{
	/* Calculate the array length */
    size_t arr_len = NUMBER_OF_SAMPLE;
    
    /* Sort the array using bubble sort algorithm */
    uint8_t i, j;
    for (i = 0; i < arr_len-1; i++)      
        for (j = 0; j < arr_len-i-1; j++) 
           if (arr[j] > arr[j+1])
           {
           		arr[j] 	 = arr[j] + arr[j+1];
           		arr[j+1] = arr[j] - arr[j+1];
           		arr[j]   = arr[j] - arr[j+1];
		   }
    
//    APP_PRINTF("Sorted:");
//    for (i = 0; i < arr_len; i++)
//    	APP_PRINTF(" %d", arr[i]);
//
//    APP_PRINTF("\r\n");

    /* Calculate the interquartile range */
    uint8_t	q1_idx = 0, q3_idx = 0;
    float Q1, Q3, IQR;

    q1_idx = (uint8_t)((arr_len + 1) / 4);
    q3_idx = (uint8_t)((arr_len + 1) * 3 / 4);

    if ((arr_len+1)/4 % 2 != 0)
    {
    	Q1 = (arr[q1_idx - 1] + arr[q1_idx]) / 2;
    	Q3 = (arr[q3_idx - 1] + arr[q3_idx]) / 2;
    }
    else
    {
    	Q1 = arr[q1_idx - 1];
    	Q3 = arr[q3_idx - 1];
    }

    IQR = Q3-Q1;

    /* Set the outlier */
    float outlier_low, outlier_high;
    outlier_low  = Q1 - 3/2*IQR;
    outlier_high = Q3 + 3/2*IQR;

//    APP_PRINTF("ArrLen: %d | q1_idx: %d | q3_idx: %d | Q1: %d | Q3: %d | IQR: %d | out_low: %d | out_high: %d | Outlier(s):", arr_len, q1_idx, q3_idx, (uint16_t)Q1, (uint16_t)Q3, (uint16_t)IQR, (uint16_t)outlier_low, (uint16_t)outlier_high);

    /* If a sample is not outlier, then sum it */
	uint8_t sample_length = 0;
    uint32_t sample_sum = 0, sample_mean = 0;
    for (i = 0; i < arr_len; i++)
    {
    	if ((arr[i] >= outlier_low) && (arr[i] <= outlier_high))
    	{
    		sample_sum += arr[i];
    		sample_length++;
    	} else {
//    		APP_PRINTF(" %d", arr[i]);
    	}
	}
//    APP_PRINTF("\r\n");

//    /* Ver 2: Sum it anyway */
//    for (i = 0; i < arr_len; i++)
//    {
//    	sample_sum += arr[i];
//    	sample_length++;
//    }
	
	/* Calculate the mean of the sample without the outlier */
	sample_mean = sample_sum/sample_length;

    return sample_mean;
}
