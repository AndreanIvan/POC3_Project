#ifndef __LIT_RS485_INTERFACE_H__
#define __LIT_RS485_INTERFACE_H__

#include "main.h"

#define ARRAY_LEN(X)           			sizeof(X)/sizeof(X[0])

#define SOLAR_ADC_CHANNEL				ADC_CHANNEL_10
#define	STEP_UP_12V_ADC_CHANNEL			ADC_CHANNEL_11
#define BATT_ADC_CHANNEL				ADC_CHANNEL_4
#define ICHARGE_ADC_CHANNEL				ADC_CHANNEL_2

#ifndef RS485_EN_GPIO_Port
#define RS485_EN_GPIO_Port          	GPIOA
#endif

#ifndef RS485_EN_Pin
#define RS485_EN_Pin                	GPIO_PIN_0
#endif

#ifndef STEP_UP_12V_GPIO_Port
#define STEP_UP_12V_GPIO_Port          	GPIOA
#endif

#ifndef STEP_UP_12V_Pin
#define STEP_UP_12V_Pin                	GPIO_PIN_8
#endif

void RS485_Init(void);

void RS485_On(void);

void RS485_Off(void);

void RS485_StepUp_On(void);

void RS485_StepUp_Off(void);

void RS485_RestartInterface(uint16_t offDelay, uint16_t onDelay);

uint32_t MPPT_GetSolarVoltage(void);
uint32_t MPPT_GetStepUpVoltage(void);
uint32_t MPPT_GetBattVoltage(void);
uint32_t MPPT_GetIChargeVoltage(void);

uint32_t MPPT_GetIChargeCurrent(void);
uint16_t MPPT_GetMCUTemp(void);

uint16_t Mean_WithoutOutlier(uint16_t arr[], uint16_t NbData);

uint16_t Mode_Array(uint16_t arr[], uint16_t NbData);

#endif /* __LIT_RS485_INTERFACE_H__ */
