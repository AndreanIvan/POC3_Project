/** 
  ****************************************************************************** 
  * @file           : custom_board_setting.h 
  * @brief          : Custom board setting for POC 3 Project 
  * @author         : Andrean I. 
  * @date           : December 2021 
  ****************************************************************************** 
  */ 
 
#ifndef __CUSTOM_BOARD_SETTING_H__ 
#define __CUSTOM_BOARD_SETTING_H__ 

/* Board Settings ------------------------------------------------------------*/

/* Device selection:
 * - POC3_WaterLevel_Board_001
 * - POC3_WaterLevel_Board_002
 * - POC3_WaterLevel_Board_003
 * - POC3_WeatherStation_Board_001
 * - POC3_WeatherStation_Board_002
 * - POC3_WeatherStation_Board_003
 * - POC3_MultiLayerSoilSensor_Board_001
 * - POC3_MultiLayerSoilSensor_Board_002
 * - POC3_MultiLayerSoilSensor_Board_003
 * - POC3_Turbidity_Board_001
 * - POC3_Turbidity_Board_002
 * - POC3_Turbidity_Board_003
 * - POC3_Evaporation_Board_001
 * - POC3_Evaporation_Board_002
 * - POC3_Evaporation_Board_003
 * - POC3_TestingMPTT_Board
 * - POC3_Node_Testing_RS485_Board
 */
#define DEVICE_SELECTION                        POC3_WeatherStation_Board_001

#define CUSTOM_APP_TX_UPLINK_DUTYCYCLE          ( 60000 * 30 )

#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_SUPPORTED

/* Firmware Version ----------------------------------------------------------*/
#define USE_CUSTOM_VERSION
#define FW_VER_MAJ_CUSTOM                       0x01
#define FW_VER_MIN_CUSTOM                       0x00
#define FW_VER_BF_CUSTOM                        0x03

/* Token paste macros --------------------------------------------------------*/
#define TOKENPASTE( x, y )                      x ## y
#define DEV_EUI_USED(x)                         TOKENPASTE(x, _DEV_EUI)
#define APP_KEY_USED(x)                         TOKENPASTE(x, _APP_KEY)
#define BOARD_NAME(x)                           TOKENPASTE(x, _String)

#define CUSTOM_DEV_EUI                          DEV_EUI_USED(DEVICE_SELECTION)
#define CUSTOM_APP_KEY                          APP_KEY_USED(DEVICE_SELECTION)
#define CUSTOM_BOARD_NAME                       BOARD_NAME(DEVICE_SELECTION)

#endif /* __CUSTOM_IDENTITY_H__ */
