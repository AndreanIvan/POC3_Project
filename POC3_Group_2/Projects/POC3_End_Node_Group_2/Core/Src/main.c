/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_lorawan.h"
#include "usart.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "stdbool.h"
#include "sys_app.h"
#include "LIT_RS485_Interface.h"
#if defined(WATER_LEVEL)
#include "LIT_WaterLevelSensor.h"
#elif defined(MULTI_LAYER_SOIL)
#include "LIT_MultilayerSoilSensor.h"
#elif defined(WEATHER_STATION)

#elif defined(TURBIDITY)
#include "LIT_TurbiditySensor.h"
#elif defined(EVAPORATION)
#include "LIT_EvaporationSensor.h"
#endif
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#if defined(SENSOR_READING_MODE)

bool readSensor = false;

#if defined(WATER_LEVEL)
	WaterLevel_t WaterLevel;
#elif defined(WEATHER_STATION)


#elif defined(MULTI_LAYER_SOIL)
	MultilayerSoil_t MultilayerSoil;
#elif defined(TURBIDITY)
	Turbidity_t Turbidity;
#elif defined(EVAPORATION)
	Evaporation_t Evaporation;
#endif
#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
#if defined(SENSOR_READING_MODE)
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch (GPIO_Pin)
	{
	case GPIO_PIN_13:
		readSensor = true;
		break;
	default:
		break;
	}
}
#endif

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_LoRaWAN_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
#if defined(SENSOR_READING_MODE)

//  MultilayerSoilSensor_Init();
  RS485_Init();
  RS485_On();
  RS485_StepUp_On();

#if defined(WATER_LEVEL)
	APP_PRINTF("Waiting Water Level Sensor to be ready...\r\n");
//	HAL_Delay(WATER_LEVEL_READY_DURATION_MS);
	APP_PRINTF("Sensor Ready.\r\n");
#elif defined(MULTI_LAYER_SOIL)
	APP_PRINTF("Waiting Multilayer Soil Sensor to be ready...\r\n");
//	HAL_Delay(MULTILAYER_SOIL_READY_DURATION_MS);
	APP_PRINTF("Sensor Ready.\r\n");
#elif defined(WEATHER_STATION)
	APP_PRINTF("Waiting Weather Station to be ready...\r\n");
//	HAL_Delay(1000);
	APP_PRINTF("Sensor Ready.\r\n");
//	weatherStation_ON();
#elif defined(TURBIDITY)
	APP_PRINTF("Waiting Turbidity Sensor to be ready...\r\n");
//	HAL_Delay(TURBIDITY_READY_DURATION_MS);
	APP_PRINTF("Sensor Ready.\r\n");
#elif defined(EVAPORATION)
	APP_PRINTF("Waiting Evaporation Sensor to be ready...\r\n");
//	HAL_Delay(EVAPORATION_READY_DURATION_MS);
	APP_PRINTF("Sensor Ready.\r\n");
#endif

#endif

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
#if !defined(SENSOR_READING_MODE)
    /* USER CODE END WHILE */
    MX_LoRaWAN_Process();

    /* USER CODE BEGIN 3 */
#endif

#if defined(SENSOR_READING_MODE)

    if(readSensor == true)
    {

#if defined(WATER_LEVEL)

		APP_PRINTF("Read Water Level Sensor...\r\n");
		WaterLevelSensor_Read(&WaterLevel);
		APP_PRINTF("Distance : %4d cm\r\n", WaterLevel.Distance);

#elif defined(WEATHER_STATION)

//		weatherStation_read();
		WS_Read();


#elif defined(MULTI_LAYER_SOIL)

//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
//		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

		APP_PRINTF("Read Multilayer Soil Sensor...\r\n");
		MultilayerSoilSensor_Read(&MultilayerSoil);
//	    APP_PRINTF("M1: %3d | T1: %3d | M2: %3d | T2: %3d | M3: %3d | T3: %3d | M4: %3d | T4: %3d\r\n",
//	    		MultilayerSoil.Moisture1, MultilayerSoil.Temp1,
//				MultilayerSoil.Moisture2, MultilayerSoil.Temp2,
//				MultilayerSoil.Moisture3, MultilayerSoil.Temp3,
//				MultilayerSoil.Moisture4, MultilayerSoil.Temp4);

#elif defined(TURBIDITY)

		APP_PRINTF("Read Turbidity Sensor...\r\n");
		TurbiditySensor_Read(&Turbidity);

#elif defined(EVAPORATION)

		APP_PRINTF("Read Evaporation Sensor...\r\n");
		EvaporationSensor_Read(&Evaporation);

#elif defined(CALIBRATION_MODE)

		unsigned char evap_com[8] = { 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x84, 0x0A };
		unsigned char calib_com[8] = { 0x01, 0xA1, 0x00, 0x01, 0x00, 0x00, 0xED, 0xD3 };
		unsigned char evap_resp[20];
		unsigned char calib_resp[20];

		memset(evap_resp, 0x00, 20);
		memset(calib_resp, 0x00, 20);

		HAL_UART_Transmit(&huart1, evap_com, 8, 500);

		APP_PRINTF("\nEvap Command  :");
		for (int i = 0; i < 8; i++ )
			APP_PRINTF(" %02X", evap_com[i]);
		APP_PRINTF("\r\n");

		HAL_UART_Receive(&huart1, evap_resp, 20, 500);

		APP_PRINTF("Evap Receive  :");
		for (int i = 0; i < 20; i++ )
			APP_PRINTF(" %02X", evap_resp[i]);
		APP_PRINTF("\r\n");

		HAL_Delay(1000);

//		HAL_UART_Transmit(&huart1, calib_com, 8, 500);
//
//		APP_PRINTF("\nCalib Command :");
//		for (int i = 0; i < 8; i++ )
//			APP_PRINTF(" %02X", calib_com[i]);
//		APP_PRINTF("\r\n");
//
//		HAL_UART_Receive(&huart1, calib_resp, 20, 500);
//
//		APP_PRINTF("Calib Receive :");
//		for (int i = 0; i < 20; i++ )
//			APP_PRINTF(" %02X", calib_resp[i]);
//		APP_PRINTF("\r\n");

		APP_PRINTF("\n========================================\r\n");

		memset(evap_com, 0x00, 8);
		memset(calib_com, 0x00, 8);
		memset(evap_resp, 0x00, 20);
		memset(calib_resp, 0x00, 20);

#else

    APP_PRINTF("Sensor not selected yet!\r\n");

#endif

    	readSensor = false;
    }

#endif

//#define PERIODIC_READ
#if defined(PERIODIC_READ)
//    HAL_Delay(2500);
    readSensor = true;
#endif

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK3|RCC_CLOCKTYPE_HCLK
                              |RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1
                              |RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  while (1)
  {
  }
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

