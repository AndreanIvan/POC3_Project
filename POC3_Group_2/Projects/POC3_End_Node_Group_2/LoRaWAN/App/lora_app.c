/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_app.c
  * @author  MCD Application Team
  * @brief   Application of the LRWAN Middleware
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "Region.h" /* Needed for LORAWAN_DEFAULT_DATA_RATE */
#include "sys_app.h"
#include "lora_app.h"
#include "stm32_seq.h"
#include "stm32_timer.h"
#include "utilities_def.h"
#include "lora_app_version.h"
#include "lorawan_version.h"
#include "subghz_phy_version.h"
#include "lora_info.h"
#include "LmHandler.h"
#include "stm32_lpm.h"
#include "adc_if.h"
#include "sys_conf.h"
#include "CayenneLpp.h"
#include "sys_sensors.h"

/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "usart.h"
#include "LIT_FirmwareVersion.h"
#include "LIT_RS485_Interface.h"

#if defined(WATER_LEVEL)
#include "LIT_WaterLevelSensor.h"
#elif defined(MULTI_LAYER_SOIL)
#include "LIT_MultilayerSoilSensor.h"
#elif defined(TURBIDITY)
#include "LIT_TurbiditySensor.h"
#elif defined(EVAPORATION)
#include "LIT_EvaporationSensor.h"
#else
#define SENSOR_READY_DURATION_MS	1000
#endif

extern UART_HandleTypeDef huart1;

/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/**
  * @brief If confirmed message state is true, specifies the uplink trials of unconfirmed message
  */
uint8_t NbTrials = DEFAULT_NB_TRIALS;

/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/**
  * @brief LoRa State Machine states
  */
typedef enum TxEventType_e
{
  /**
    * @brief Appdata Transmission issue based on timer every TxDutyCycleTime
    */
  TX_ON_TIMER,
  /**
    * @brief Appdata Transmission external event plugged on OnSendEvent( )
    */
  TX_ON_EVENT
  /* USER CODE BEGIN TxEventType_t */

  /* USER CODE END TxEventType_t */
} TxEventType_t;

/* USER CODE BEGIN PTD */
typedef enum DeviceJoinStatus_e
{
	DEVICE_NOT_JOINED = 0,
	DEVICE_JOINED = !DEVICE_NOT_JOINED
} DeviceJoinStatus_t;

typedef struct SafeMode_s
{
	bool Status;				/* Indicate current safe mode status */
	bool PreviousStatus;		/* Indicate previous safe mode status */
	uint16_t BattVoltageLow;	/* Indicate low battery level in mV threshold to enable safemode */
	uint16_t BattVoltageHigh;	/* Indicate high battery level in mV threshold to disable safemode */
} SafeMode_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private function prototypes -----------------------------------------------*/
/**
  * @brief  LoRa End Node send request
  */
static void SendTxData(void);

/**
  * @brief  TX timer callback function
  * @param  context ptr of timer context
  */
static void OnTxTimerEvent(void *context);

/**
  * @brief  join event callback function
  * @param  joinParams status of join
  */
static void OnJoinRequest(LmHandlerJoinParams_t *joinParams);

/**
  * @brief  tx event callback function
  * @param  params status of last Tx
  */
static void OnTxData(LmHandlerTxParams_t *params);

/**
  * @brief callback when LoRa application has received a frame
  * @param appData data received in the last Rx
  * @param params status of last Rx
  */
static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params);

/*!
 * Will be called each time a Radio IRQ is handled by the MAC layer
 *
 */
static void OnMacProcessNotify(void);

/* USER CODE BEGIN PFP */

/**
  * @brief  LED Tx timer callback function
  * @param  context ptr of LED context
  */
static void OnTxTimerLedEvent(void *context);

/**
  * @brief  LED Rx timer callback function
  * @param  context ptr of LED context
  */
static void OnRxTimerLedEvent(void *context);

/**
  * @brief  LED Join timer callback function
  * @param  context ptr of LED context
  */
static void OnJoinTimerLedEvent(void *context);

/**
  * @brief  Print the remaining Tx Time for the next periodic uplink
  */
static void PrintRemainingTxTime(void);

/* USER CODE END PFP */

/* Private variables ---------------------------------------------------------*/
static ActivationType_t ActivationType = LORAWAN_DEFAULT_ACTIVATION_TYPE;

/**
  * @brief LoRaWAN handler Callbacks
  */
static LmHandlerCallbacks_t LmHandlerCallbacks =
{
  .GetBatteryLevel =           GetBatteryLevel,
  .GetTemperature =            GetTemperatureLevel,
  .GetUniqueId =               GetUniqueId,
  .GetDevAddr =                GetDevAddr,
  .OnMacProcess =              OnMacProcessNotify,
  .OnJoinRequest =             OnJoinRequest,
  .OnTxData =                  OnTxData,
  .OnRxData =                  OnRxData
};

/**
  * @brief LoRaWAN handler parameters
  */
static LmHandlerParams_t LmHandlerParams =
{
  .ActiveRegion =             ACTIVE_REGION,
  .DefaultClass =             LORAWAN_DEFAULT_CLASS,
  .AdrEnable =                LORAWAN_ADR_STATE,
  .TxDatarate =               LORAWAN_DEFAULT_DATA_RATE,
  .PingPeriodicity =          LORAWAN_DEFAULT_PING_SLOT_PERIODICITY
};

/**
  * @brief Type of Event to generate application Tx
  */
static TxEventType_t EventType = TX_ON_TIMER;

/**
  * @brief Timer to handle the application Tx
  */
static UTIL_TIMER_Object_t TxTimer;

/* USER CODE BEGIN PV */
/**
  * @brief User application buffer
  */
static uint8_t AppDataBuffer[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];

/**
  * @brief User application data structure
  */
static LmHandlerAppData_t AppData = { 0, 0, AppDataBuffer };

/**
  * @brief Specifies the state of the application LED
  */
static uint8_t AppLedStateOn = RESET;

/**
  * @brief Timer to handle the application Tx Led to toggle
  */
static UTIL_TIMER_Object_t TxLedTimer;

/**
  * @brief Timer to handle the application Rx Led to toggle
  */
static UTIL_TIMER_Object_t RxLedTimer;

/**
  * @brief Timer to handle the application Join Led to toggle
  */
static UTIL_TIMER_Object_t JoinLedTimer;

/**
  * @brief Specifies the device join status
  */
static uint8_t JoinStatus = DEVICE_NOT_JOINED;

/**
  * @brief Specifies the device join request attempts
  */
static uint8_t JoinAttempt = 0;

/**
  * @brief Specifies the application uplink duty cycle
  */
static uint32_t UplinkDutyCycle	= APP_TX_UPLINK_DUTYCYCLE;

/**
  * @brief Specifies the application uplink duty cycle maximum random value
  */
static uint32_t UplinkDutyCycle_MaxDeltaRandom = 0;

/**
  * @brief Specifies the application join request duty cycle maximum random value
  */
static uint32_t JoinDutyCycle_MaxDeltaRandom = 0;

/**
  * @brief Specifies Tx period of the next transmission
  */
static uint32_t NextTxPeriod = 0;

/**
  * @brief Specifies LoRaWAN App Port
  */
static uint8_t AppPort = LORAWAN_DEFAULT_PORT;

/**
  * @brief Specifies the confirmed message state
  */
static uint8_t ConfirmMsgState = LORAWAN_DEFAULT_CONFIRMED_MSG_STATE;

/**
  * @brief Variable to store last downlink RSSI
  */
static int8_t LastDownlinkRSSI = 0xFF;

/**
  * @brief Variable to store last downlink SNR
  */
static int8_t LastDownlinkSNR = -128;

/**
  * @brief Variable to store last downlink Tx Power
  */
static int8_t CurrentUplinkTxPower = 0;

/**
  * @brief Indicate safe mode status based on battery condition.
  * 	   false : Safe mode OFF, sensor always turned on
  * 	   true  : Safe mode ON, only turned on sensor when reading needed
  */
static SafeMode_t SafeMode = { false, true, DEFAULT_LOW_BATTERY_THRESHOLD, DEFAULT_HIGH_BATTERY_THRESHOLD};

/**
 *
 */

/* USER CODE END PV */

/* Exported functions ---------------------------------------------------------*/
/* USER CODE BEGIN EF */

/* USER CODE END EF */

void LoRaWAN_Init(void)
{
  /* USER CODE BEGIN LoRaWAN_Init_1 */

#define USE_LED
#define USE_LEDTIM
#define USE_PB
//#define SAFEMODE_ALWAYS_ON

#if defined(USE_LED)
  SYS_LED_Init(SYS_LED_BLUE);
#endif
#if defined(USE_PB)
  SYS_PB_Init(SYS_BUTTON1, SYS_BUTTON_MODE_EXTI);
#endif

  /* Get LIT Modified Firmware Version*/
  APP_LOG(TS_OFF, VLEVEL_M, "LIT_FW_VERSION:     V%X.%X.%X\r\n",
          (uint8_t)(__FW_VERSION_MAJOR),
          (uint8_t)(__FW_VERSION_MINOR),
          (uint8_t)(__FW_VERSION_BUGFIX));

#if defined(USE_LED) && defined(USE_LEDTIM)
  UTIL_TIMER_Create(&TxLedTimer, 0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnTxTimerLedEvent, NULL);
  UTIL_TIMER_Create(&RxLedTimer, 0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnRxTimerLedEvent, NULL);
  UTIL_TIMER_Create(&JoinLedTimer, 0xFFFFFFFFU, UTIL_TIMER_PERIODIC, OnJoinTimerLedEvent, NULL);
  UTIL_TIMER_SetPeriod(&TxLedTimer, 500);
  UTIL_TIMER_SetPeriod(&RxLedTimer, 500);
  UTIL_TIMER_SetPeriod(&JoinLedTimer, 500);
#endif

  /* USER CODE END LoRaWAN_Init_1 */

  UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LmHandlerProcess), UTIL_SEQ_RFU, LmHandlerProcess);
  UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), UTIL_SEQ_RFU, SendTxData);
  /* Init Info table used by LmHandler*/
  LoraInfo_Init();

  /* Init the Lora Stack*/
  LmHandlerInit(&LmHandlerCallbacks);

  LmHandlerConfigure(&LmHandlerParams);

  /* USER CODE BEGIN LoRaWAN_Init_2 */

  /* Display some parameter */
  APP_LOG(TS_OFF, VLEVEL_L, "Join Period : %2d %s\r\n",
		  APP_TX_JOIN_DUTYCYCLE < 60000 ? APP_TX_JOIN_DUTYCYCLE / 1000 : APP_TX_JOIN_DUTYCYCLE / 60000,
		  APP_TX_JOIN_DUTYCYCLE < 60000 ? "seconds" : "minutes");
  APP_LOG(TS_OFF, VLEVEL_L, "Tx Period   : %2d %s\r\n",
		  APP_TX_UPLINK_DUTYCYCLE < 60000 ? APP_TX_UPLINK_DUTYCYCLE / 1000 : APP_TX_UPLINK_DUTYCYCLE / 60000,
		  APP_TX_UPLINK_DUTYCYCLE < 60000 ? "seconds" : "minutes");


#if defined(USE_LED) && defined(USE_LEDTIM)
  UTIL_TIMER_Start(&JoinLedTimer);
#endif
  SYS_PB_Init(BUTTON_SW1, BUTTON_MODE_EXTI);
  /* USER CODE END LoRaWAN_Init_2 */

  LmHandlerJoin(ActivationType);

  if (EventType == TX_ON_TIMER)
  {
    /* send every time timer elapses */
    UTIL_TIMER_Create(&TxTimer,  0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnTxTimerEvent, NULL);
    UTIL_TIMER_SetPeriod(&TxTimer,  APP_TX_DUTYCYCLE);
    UTIL_TIMER_Start(&TxTimer);
  }
  else
  {
    /* USER CODE BEGIN LoRaWAN_Init_3 */
#if defined(USE_PB)
    /* send every time button is pushed */
    SYS_PB_Init(BUTTON_SW1, BUTTON_MODE_EXTI);
#endif
    /* USER CODE END LoRaWAN_Init_3 */
  }

  /* USER CODE BEGIN LoRaWAN_Init_Last */

  /* USER CODE END LoRaWAN_Init_Last */
}

/* USER CODE BEGIN PB_Callbacks */
#if !defined(SENSOR_READING_MODE)
/* Note: Current the stm32wlxx_it.c generated by STM32CubeMX does not support BSP for PB in EXTI mode. */
/* In order to get a push button IRS by code automatically generated */
/* HAL_GPIO_EXTI_Callback is today the only available possibility. */
/* Using HAL_GPIO_EXTI_Callback() shortcuts the BSP. */
/* If users wants to go through the BSP, stm32wlxx_it.c should be updated  */
/* in the USER CODE SESSION of the correspondent EXTIn_IRQHandler() */
/* to call the SYS_PB_IRQHandler() or the HAL_EXTI_IRQHandler(&H_EXTI_n);. */
/* Then the below HAL_GPIO_EXTI_Callback() can be replaced by BSP callback */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch (GPIO_Pin)
  {
    case  SYS_BUTTON1_PIN:
    {
    	APP_PRINTF("================================================================================\r\n\n");
    	APP_PRINTF("User button pressed. Sending data...\r\n");

    	PrintRemainingTxTime();

    	UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    }
      break;
    default:
      break;
  }
}
#endif

/* USER CODE END PB_Callbacks */

/* Private functions ---------------------------------------------------------*/
/* USER CODE BEGIN PrFD */
static void PrintRemainingTxTime(void)
{
	uint32_t TxTimerRemainingMs, TxTimerRemainingSecond, TxTimerRemainingMinute;
	UTIL_TIMER_GetRemainingTime(&TxTimer, &TxTimerRemainingMs);

	TxTimerRemainingSecond = (uint16_t)(TxTimerRemainingMs % 60000 / 1000);
	TxTimerRemainingMinute = (uint16_t)(TxTimerRemainingMs / 60000);

	APP_PRINTF("Time remaining for the next periodic uplink: %d %s %d %s\r\n",
			TxTimerRemainingMinute, TxTimerRemainingMinute > 1 ? "minutes" : "minute",
			TxTimerRemainingSecond, TxTimerRemainingSecond > 1 ? "seconds" : "second"
			);

}

static char *TimeMstoMinSec(uint16_t timeMs)
{
	uint16_t timeMin, timeSec;
	char* retString = malloc(40);

	memset(retString, 0x00, 40);

	timeMin = (uint16_t)(timeMs / 60000);
	timeSec = (uint16_t)(timeMs / 1000) % 60;

	snprintf(retString, 40, "%d %s %d %s",
			timeMin, timeMin > 1 ? "minutes" : "minute",
			timeSec, timeSec > 1 ? "seconds" : "second");

	return retString;

}
/* USER CODE END PrFD */

static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params)
{
  /* USER CODE BEGIN OnRxData_1 */
  if ((appData != NULL) || (params != NULL))
  {
#if defined(USE_LED)
    SYS_LED_On(SYS_LED_BLUE) ;
#endif
#if defined(USE_LED) && defined(USE_LEDTIM)
    UTIL_TIMER_Start(&RxLedTimer);
#endif
    static const char *slotStrings[] = { "1", "2", "C", "C Multicast", "B Ping-Slot", "B Multicast Ping-Slot" };

    APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### ========== MCPS-Indication ==========\r\n");
    APP_LOG(TS_OFF, VLEVEL_H, "###### D/L FRAME:%04d | SLOT:%s | PORT:%d | DR:%d | RSSI:%d | SNR:%d\r\n",
            params->DownlinkCounter, slotStrings[params->RxSlot], appData->Port, params->Datarate, params->Rssi, params->Snr);

    LastDownlinkRSSI = params->Rssi;
    LastDownlinkSNR = params->Snr;

    if (appData->Port != 0)
    	APP_LOG(TS_OFF, VLEVEL_L, "Downlink on port %d:\r\n", appData->Port);

    switch (appData->Port)
    {
      case LORAWAN_SWITCH_CLASS_PORT:
        /*this port switches the class*/
        if (appData->BufferSize == 1)
        {
          switch (appData->Buffer[0])
          {
            case 0:
            {
              LmHandlerRequestClass(CLASS_A);
              break;
            }
            case 1:
            {
              LmHandlerRequestClass(CLASS_B);
              break;
            }
            case 2:
            {
              LmHandlerRequestClass(CLASS_C);
              break;
            }
            default:
              break;
          }
        }
        break;
      case LORAWAN_USER_APP_PORT:
        if (appData->BufferSize == 1)
        {
          AppLedStateOn = appData->Buffer[0] & 0x01;
          if (AppLedStateOn == RESET)
          {
            APP_LOG(TS_OFF, VLEVEL_H,   "LED OFF\r\n");
#if defined(USE_LED)
            SYS_LED_Off(SYS_LED_BLUE) ;
#endif
          }
          else
          {
            APP_LOG(TS_OFF, VLEVEL_H, "LED ON\r\n");
#if defined(USE_LED)
            SYS_LED_On(SYS_LED_BLUE) ;
#endif
          }
        }
        break;

      // Port 128
      case LORAWAN_DUTYCYCLE_CONFIG_PORT:
    	if (appData->BufferSize == 1)
    	{
    		if (appData->Buffer[0] != 0x00)
    		{
				AppPort = LORAWAN_DUTYCYCLE_CONFIG_PORT;

    			UplinkDutyCycle = appData->Buffer[0] * 60000;
    			APP_LOG(TS_OFF, VLEVEL_L, "Set Tx duty cycle to %d minute(s)\r\n", appData->Buffer[0]);

    			UTIL_TIMER_Stop(&TxTimer);

    			UplinkDutyCycle_MaxDeltaRandom = ( UplinkDutyCycle * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
    			NextTxPeriod = randr(UplinkDutyCycle - UplinkDutyCycle_MaxDeltaRandom, UplinkDutyCycle + UplinkDutyCycle_MaxDeltaRandom);

    			UTIL_TIMER_SetPeriod(&TxTimer, NextTxPeriod);

				UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);

   				APP_PRINTF("Change next uplink attempt to %d ms\r\n", NextTxPeriod);

    			/*Wait for next tx slot*/
    			UTIL_TIMER_Start(&TxTimer);

    		}
    		else
    		{
    			APP_LOG(TS_OFF, VLEVEL_L, "Payload of Tx duty cycle error!\r\n", appData->Buffer[0]);
    		}
    	}
      break;

      // Port 129
      case LORAWAN_STATUS_REQUEST_PORT:
    	if (appData->BufferSize == 1)
    	{
    		if (appData->Buffer[0] == 0x01)
    		{
    			APP_LOG(TS_OFF, VLEVEL_L, "Preparing to send status request...\r\n");
    			AppPort = LORAWAN_STATUS_REQUEST_PORT;
    			UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    		}
    	}
      break;

      // Port 130
      case LORAWAN_CONFIRMED_MSG_CONFIG_PORT:
    	if (appData->BufferSize == 1)
    	{
        AppPort = LORAWAN_CONFIRMED_MSG_CONFIG_PORT;
    		if (appData->Buffer[0] == 0x00)
    		{
    			ConfirmMsgState = LORAMAC_HANDLER_UNCONFIRMED_MSG;
    			APP_LOG(TS_OFF, VLEVEL_L, "Confirm message state is set to unconfirmed.\r\n");
    		}
    		else
    		{
    			ConfirmMsgState = LORAMAC_HANDLER_CONFIRMED_MSG;
    			NbTrials = appData->Buffer[0];
    			APP_LOG(TS_OFF, VLEVEL_L, "Confirm message state is set to confirmed with %d NbTrials.\r\n", NbTrials);
    		}
        UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    	}
      break;

      default:
        break;
    }
  }
  /* USER CODE END OnRxData_1 */
}

static void SendTxData(void)
{
  /* USER CODE BEGIN SendTxData_1 */

  UTIL_TIMER_Time_t nextTxIn = 0;
  bool ProceedToSend = true;
  uint8_t SendTrial = 0;

  switch (AppPort)
  {
    case LORAWAN_USER_APP_PORT:
    {
      AppData.Port = LORAWAN_USER_APP_PORT;

      uint8_t i = 0;
      char sendString1[512], sendString2[512];

      MX_USART1_UART_Init();

//#define SENSOR_READY_DURATION_MS	5000

  	  if ( SafeMode.PreviousStatus == true )
  	  {
  		  RS485_StepUp_On();
  		  RS485_On();

  		  APP_PRINTF("Safe mode ON. Waiting sensor to be ready... [%d %s]\r\n",
  				  SENSOR_READY_DURATION_MS / 1000,
				  SENSOR_READY_DURATION_MS / 1000 > 1 ? "seconds" : "second");

  		  HAL_Delay(SENSOR_READY_DURATION_MS);
//  		  HAL_Delay(5000);
  		  APP_PRINTF("Sensor ready. Retrieving data...\r\n");
  	  }
  	  else
  	  {
  		  APP_PRINTF("Safe mode OFF. Retrieving data...\r\n");
  	  }

      /*
		┌───────┬───────────────────────�?
		│ Index │        Payload        │
		├───────┼───────────────────────┤
		│   0   │ Batt_Voltage          │
		│   1   │ Safemode_Status       │
		│   2   │ I_Charge_Voltage[0]   │
		│   3   │ I_Charge_Voltage[1]   │
		│   4   │ V_Step_Up[0]          │
		│   5   │ V_Step_Up[1]          │
		│   6   │ V_Solar[0]            │
		│   7   │ V_Solar[1]            │
		│   8   │ MCU_Temp              │
		└───────┴───────────────────────┘
	  */

  	  uint16_t V_Solar, V_Step_Up, BattVoltage, I_Charge_Voltage, MCU_Temp;

  	  V_Solar			= MPPT_GetSolarVoltage();
  	  V_Step_Up			= MPPT_GetStepUpVoltage();
  	  BattVoltage		= MPPT_GetBattVoltage();
  	  I_Charge_Voltage	= MPPT_GetIChargeVoltage();
//  	  IChargeCurrent	= MPPT_GetIChargeCurrent();
  	  MCU_Temp			= (SYS_GetTemperatureLevel() >> 8);

#if defined(SAFEMODE_ALWAYS_ON)
  	  SafeMode.Status = true;
  	  APP_PRINTF("SAFEMODE_ALWAYS_ON Mode Selected\r\n");
#else

  	  if (BattVoltage < SafeMode.BattVoltageLow)
  	  {
  		  SafeMode.Status = true;
  	  }

  	  if (BattVoltage > SafeMode.BattVoltageHigh)
  	  {
  		  SafeMode.Status = false;
  	  }

#endif
  	  AppData.Buffer[i++] = (uint8_t)( BattVoltage / 10 - 180) & 0xFF;
  	  AppData.Buffer[i++] = SafeMode.Status;
  	  AppData.Buffer[i++] = (uint8_t)(I_Charge_Voltage >> 8) & 0xFF;
  	  AppData.Buffer[i++] = (uint8_t)(I_Charge_Voltage & 0xFF);
  	  AppData.Buffer[i++] = (uint8_t)(V_Step_Up >> 8) & 0xFF;
  	  AppData.Buffer[i++] = (uint8_t)(V_Step_Up & 0xFF);
  	  AppData.Buffer[i++] = (uint8_t)(V_Solar >> 8) & 0xFF;
  	  AppData.Buffer[i++] = (uint8_t)(V_Solar & 0xFF);
  	  AppData.Buffer[i++] = (uint8_t)MCU_Temp;

  	  sprintf(sendString1, "Send on LORAWAN_USER_APP_PORT (%d):\r\n"
  			  	  	  	  	  	"- BattVoltage    : %2d.%03d V\r\n"
                				"- SafemodeStatus : %s\r\n"
  	                            "- IChargeVoltage : %2d.%03d V\r\n"
  	                            "- StepUpVoltage  : %2d.%03d V\r\n"
  	     		  	  	  	 	"- SolarVoltage   : %2d.%03d V\r\n"
  			  	  	  	  	  	"- MCU Temp       : %2d °C\r\n",
  	  							LORAWAN_USER_APP_PORT,
								(uint16_t)(BattVoltage / 1000), (uint16_t)(BattVoltage % 1000),
								(SafeMode.Status == 0 ? "OFF" : "ON"),
								(uint16_t)(I_Charge_Voltage / 1000), (uint16_t)(I_Charge_Voltage % 1000),
								(uint16_t)(V_Step_Up / 1000), (uint16_t)(V_Step_Up % 1000),
								(uint16_t)(V_Solar / 1000), (uint16_t)(V_Solar % 1000),
								MCU_Temp);

#if defined(WATER_LEVEL)

      /*
		┌───────┬───────────────────────�?
		│ Index │        Payload        │
		├───────┼───────────────────────┤
		│   9   │ Distance[0]           │
		│  10   │ Distance[1]           │
		└───────┴───────────────────────┘
	  */

  	  WaterLevel_t WaterLevel;

  	  WaterLevelSensor_Read(&WaterLevel);

	  AppData.Buffer[i++] = (uint8_t)(WaterLevel.Distance >> 8) & 0xFF;
	  AppData.Buffer[i++] = (uint8_t)(WaterLevel.Distance) & 0xFF;

	  sprintf(sendString2, 	"- Distance       : %d cm\r\n",
			  	  	  	  	WaterLevel.Distance
							);

#elif defined(WEATHER_STATION)
#elif defined(MULTI_LAYER_SOIL)

      /*
		┌───────┬───────────────────────�?┌───────┬───────────────────────�?
		│ Index │        Payload        ││ Index │        Payload        │
		├───────┼───────────────────────┤├───────┼───────────────────────┤
		│   9   │ Moisture1[0]          ││  17   │ Moisture3[0]          │
		│  10   │ Moisture1[1]          ││  18   │ Moisture3[1]          │
		│  11   │ Temp1[0]              ││  19   │ Temp3[0]              │
		│  12   │ Temp1[1]              ││  20   │ Temp3[1]              │
		│  13   │ Moisture2[0]          ││  21   │ Moisture4[0]          │
		│  14   │ Moisture2[1]          ││  22   │ Moisture4[1]          │
		│  15   │ Temp2[0]              ││  23   │ Temp4[0]              │
		│  16   │ Temp2[1]              ││  24   │ Temp4[1]              │
		└───────┴───────────────────────┘└───────┴───────────────────────┘
	  */

	  MultilayerSoil_t MultilayerSoil;

	  MultilayerSoilSensor_Read(&MultilayerSoil);

	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture1 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture1 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp1 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp1 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture2 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture2 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp2 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp2 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture3 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture3 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp3 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp3 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture4 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Moisture4 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp4 >> 8 & 0xFF);
	  AppData.Buffer[i++] = (uint8_t)(MultilayerSoil.Temp4 & 0xFF);

	  sprintf(sendString2, 		"- Moisture1      : %2d.%d %%\r\n"
								"- Temp1          : %2d.%d °C\r\n"
								"- Moisture2      : %2d.%d %%\r\n"
								"- Temp2          : %2d.%d °C\r\n"
								"- Moisture3      : %2d.%d %%\r\n"
								"- Temp3          : %2d.%d °C\r\n"
								"- Moisture4      : %2d.%d %%\r\n"
								"- Temp4          : %2d.%d °C\r\n",
								(uint16_t)(MultilayerSoil.Moisture1 / 10), (uint16_t)(MultilayerSoil.Moisture1 % 10),
								(uint16_t)(MultilayerSoil.Temp1 / 10),     (uint16_t)(MultilayerSoil.Temp1 % 10),
								(uint16_t)(MultilayerSoil.Moisture2 / 10), (uint16_t)(MultilayerSoil.Moisture2 % 10),
								(uint16_t)(MultilayerSoil.Temp2 / 10),     (uint16_t)(MultilayerSoil.Temp2 % 10),
								(uint16_t)(MultilayerSoil.Moisture3 / 10), (uint16_t)(MultilayerSoil.Moisture3 % 10),
								(uint16_t)(MultilayerSoil.Temp3 / 10),     (uint16_t)(MultilayerSoil.Temp3 % 10),
								(uint16_t)(MultilayerSoil.Moisture4 / 10), (uint16_t)(MultilayerSoil.Moisture4 % 10),
								(uint16_t)(MultilayerSoil.Temp4 / 10),     (uint16_t)(MultilayerSoil.Temp4 % 10)
								);

#elif defined(TURBIDITY)

      /*
		┌───────┬───────────────────────�?
		│ Index │        Payload        │
		├───────┼───────────────────────┤
		│   9   │ Turbidity[0]          │
		│  10   │ Turbidity[1]          │
		│  11   │ Turbidity[2]          │
		│  12   │ Turbidity[3]          │
		└───────┴───────────────────────┘
	  */

	  Turbidity_t Turbidity;

	  uint8_t turbidity_framed[4];

	  TurbiditySensor_Read(&Turbidity);

	  *(float*)(turbidity_framed) =  Turbidity.turbidity;

	  AppData.Buffer[i++] = turbidity_framed[0];
	  AppData.Buffer[i++] = turbidity_framed[1];
	  AppData.Buffer[i++] = turbidity_framed[2];
	  AppData.Buffer[i++] = turbidity_framed[3];

	  sprintf(sendString2, 		"- Turbidity      : %2d.%03d NTU\r\n",
			  	  	  	  	  	(uint16_t)(Turbidity.turbidity),
								(uint16_t)(Turbidity.turbidity * 1000) % 1000
								);

#elif defined(EVAPORATION)

      /*
		┌───────┬───────────────────────�?
		│ Index │        Payload        │
		├───────┼───────────────────────┤
		│   9   │ Evaporation[0]        │
		│  10   │ Evaporation[1]        │
		└───────┴───────────────────────┘
	  */

	  Evaporation_t Evaporation;
	  uint8_t Evap_ReadTrials = 0;

	  while ( (EvaporationSensor_Read(&Evaporation) != 0) && Evap_ReadTrials < 1 )
	  {
		  APP_PRINTF("Reading error, restarting interface...\r\n");
		  RS485_RestartInterface(5000, 5000);
		  Evap_ReadTrials++;
		  ProceedToSend = false;
	  }

	  AppData.Buffer[i++] = (uint8_t)(Evaporation.evaporation >> 8 ) & 0xFF;
	  AppData.Buffer[i++] = (uint8_t)(Evaporation.evaporation & 0xFF);

	  sprintf(sendString2, 	"- Evaporation     : %2d.%d mm\r\n",
			  	  	  	  	(uint16_t)(Evaporation.evaporation / 10),
							(uint16_t)(Evaporation.evaporation % 10)
							);

#else

#endif

	  if ( SafeMode.Status == true )
	  {
		  if ( SafeMode.PreviousStatus == false )
			  APP_PRINTF("Low battery voltage. Turning on safe mode.\r\n");

		  APP_PRINTF("Turning off sensor...\r\n");
		  RS485_StepUp_Off();
		  RS485_Off();
	  }
	  else
	  {
		  if ( SafeMode.PreviousStatus == true )
			  APP_PRINTF("Low battery voltage. Turning on safe mode. ");

		  APP_PRINTF("Sensor stay ON.\r\n");
	  }

	  SafeMode.PreviousStatus = SafeMode.Status;

//	  APP_LOG(TS_OFF, VLEVEL_M, "%s", sendString);
      APP_LOG(TS_OFF, VLEVEL_M, "%s%s", sendString1, sendString2);

      AppData.BufferSize = i;
    } /* LORAWAN_USER_APP_PORT */
    break;

    case LORAWAN_DUTYCYCLE_CONFIG_PORT:
    {
      AppData.Port = LORAWAN_DUTYCYCLE_CONFIG_PORT;
      AppData.Buffer[0] = (uint8_t)(UplinkDutyCycle / 60000);
      AppData.BufferSize = 1;

      AppPort = LORAWAN_DEFAULT_PORT;

      APP_LOG(TS_OFF, VLEVEL_L, "Send on LORAWAN_DUTYCYCLE_CONFIG_PORT (128): %d minute(s) duty cycle.\r\n", AppData.Buffer[0]);
    }
    break;
    case LORAWAN_STATUS_REQUEST_PORT:
    {
    	// Crosscheck dengan payload
      AppData.Port = LORAWAN_STATUS_REQUEST_PORT;

      LmHandlerGetTxPower(&CurrentUplinkTxPower);

      AppData.Buffer[0] = (uint8_t)(LastDownlinkRSSI + 180) & 0xFF;
      AppData.Buffer[1] = (uint8_t)LastDownlinkSNR;
      AppData.Buffer[2] = (uint8_t)CurrentUplinkTxPower;
      AppData.Buffer[3] = (uint8_t)(UplinkDutyCycle / 60000);
      AppData.Buffer[4] = (uint8_t)(SYS_GetBatteryLevel()/10 - 180);
      AppData.Buffer[5] = (uint8_t)(SYS_GetTemperatureLevel() >> 8);
      AppData.Buffer[6] = (uint8_t)(__FW_VERSION_MAJOR << 4 | __FW_VERSION_MINOR);
      AppData.Buffer[7] = (uint8_t)(__FW_VERSION_BUGFIX);

      AppData.BufferSize = 8;

      AppPort = LORAWAN_DEFAULT_PORT;

      APP_LOG(TS_OFF, VLEVEL_L, "Send on LORAWAN_STATUS_REQUEST_PORT (129):\r\n"
    		  "LastDownlinkRSSI     : %d dBm\r\n"
    		  "LastDownlinkSNR      : %d.%d dB\r\n"
    		  "CurrentUplinkTxPower : %d dBm\r\n"
    		  "UplinkDutyCycle      : %d minutes\r\n"
    		  "BatteryLevel         : %d.%d V\r\n"
    		  "MCU Temperature      : %d ºC\r\n"
    		  "Firmware Version     : %d.%d.%d\r\n",
			  AppData.Buffer[0]-180, (uint16_t)(AppData.Buffer[1] / 4), (uint16_t)(AppData.Buffer[1] * 100 / 4) % 100,
			  20 - 2 * AppData.Buffer[2], AppData.Buffer[3], (uint16_t)((AppData.Buffer[4] + 180) / 100), (uint16_t)((AppData.Buffer[4] + 180) % 100),
			  AppData.Buffer[5], AppData.Buffer[6] >> 4, AppData.Buffer[6] & 0x0F, AppData.Buffer[7]);


      LastDownlinkRSSI = 0xFF;
      LastDownlinkSNR = -128;

    }
    break;
    case LORAWAN_CONFIRMED_MSG_CONFIG_PORT:
    {
      AppData.Port = LORAWAN_CONFIRMED_MSG_CONFIG_PORT;
      if (ConfirmMsgState == LORAMAC_HANDLER_UNCONFIRMED_MSG)
      {
        AppData.Buffer[0] = 0x00;
        APP_PRINTF("Send on LORAWAN_CONFIRMED_MSG_CONFIG_PORT: unconfirmed message.\r\n");
      }
      else
      {
        AppData.Buffer[0] = NbTrials;
        APP_PRINTF("Send on LORAWAN_CONFIRMED_MSG_CONFIG_PORT: confirmed message with %d NbTrials\r\n", AppData.Buffer[0]);
      }
      AppData.BufferSize = 1;
      AppPort = LORAWAN_DEFAULT_PORT;
    }
    break;
    default:
    break;

  }

  if ( ( ProceedToSend == true ) || SendTrial > 2 )
  {
	  if (LORAMAC_HANDLER_SUCCESS == LmHandlerSend(&AppData, ConfirmMsgState, &nextTxIn, false))
	  {
		APP_LOG(TS_ON, VLEVEL_L, "SEND REQUEST\r\n");
	  }
	  else if (nextTxIn > 0)
	  {
		APP_LOG(TS_ON, VLEVEL_L, "Next Tx in  : ~%d second(s)\r\n", (nextTxIn / 1000));
	  }
  }
  else
  {
	  APP_PRINTF("Failed to proceed to send. Retrying...\r\n");
	  SendTrial++;
	  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
  }


  /* USER CODE END SendTxData_1 */
}

static void OnTxTimerEvent(void *context)
{
  /* USER CODE BEGIN OnTxTimerEvent_1 */
    APP_PRINTF("======================================================================\r\n\n");

	if (JoinStatus == DEVICE_NOT_JOINED && JoinAttempt < JOIN_DUTYCYCLE_MAX_ATTEMPT)
	{
		JoinDutyCycle_MaxDeltaRandom = ( APP_TX_JOIN_DUTYCYCLE * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
		NextTxPeriod = randr(APP_TX_JOIN_DUTYCYCLE - JoinDutyCycle_MaxDeltaRandom, APP_TX_JOIN_DUTYCYCLE + JoinDutyCycle_MaxDeltaRandom);


		APP_PRINTF("Next uplink or join attempt in %s\r\n", TimeMstoMinSec(NextTxPeriod));

	}
	else
	{
		if (JoinAttempt == JOIN_DUTYCYCLE_MAX_ATTEMPT)
		{
			APP_PRINTF("Maximum attempt using join dutycycle reached!\r\n");
		}

		UplinkDutyCycle_MaxDeltaRandom = ( UplinkDutyCycle * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
		NextTxPeriod = randr(UplinkDutyCycle - UplinkDutyCycle_MaxDeltaRandom, UplinkDutyCycle + UplinkDutyCycle_MaxDeltaRandom);

		if (JoinStatus == DEVICE_NOT_JOINED)
		{
			APP_PRINTF("Next uplink or join attempt in %s\r\n", TimeMstoMinSec(NextTxPeriod));
		}
		else
		{
			APP_PRINTF("Next uplink in %s\r\n", TimeMstoMinSec(NextTxPeriod));
		}
	}

	UTIL_TIMER_SetPeriod(&TxTimer, NextTxPeriod);
  /* USER CODE END OnTxTimerEvent_1 */
  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);

  /*Wait for next tx slot*/
  UTIL_TIMER_Start(&TxTimer);
  /* USER CODE BEGIN OnTxTimerEvent_2 */

  /* USER CODE END OnTxTimerEvent_2 */
}

/* USER CODE BEGIN PrFD_LedEvents */
static void OnTxTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Off(SYS_LED_BLUE) ;
#endif
}

static void OnRxTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Off(SYS_LED_BLUE) ;
#endif
}

static void OnJoinTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Toggle(SYS_LED_BLUE) ;
#endif
}

/* USER CODE END PrFD_LedEvents */

static void OnTxData(LmHandlerTxParams_t *params)
{
  /* USER CODE BEGIN OnTxData_1 */
  if ((params != NULL))
  {
    /* Process Tx event only if its a mcps response to prevent some internal events (mlme) */
    if (params->IsMcpsConfirm != 0)
    {
#if defined(USE_LED)
      SYS_LED_On(SYS_LED_BLUE) ;
#endif
#if defined(USE_LED) && defined(USE_LEDTIM)
      UTIL_TIMER_Start(&TxLedTimer);
#endif

      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### ========== MCPS-Confirm =============\r\n");
      APP_LOG(TS_OFF, VLEVEL_H, "###### U/L FRAME:%04d | PORT:%d | DR:%d | PWR:%d", params->UplinkCounter,
              params->AppData.Port, params->Datarate, params->TxPower);

      APP_LOG(TS_OFF, VLEVEL_H, " | MSG TYPE:");
      if (params->MsgType == LORAMAC_HANDLER_CONFIRMED_MSG)
      {
        APP_LOG(TS_OFF, VLEVEL_H, "CONFIRMED [%s]\r\n", (params->AckReceived != 0) ? "ACK" : "NACK");
      }
      else
      {
        APP_LOG(TS_OFF, VLEVEL_H, "UNCONFIRMED\r\n");
      }
    }
  }
  CurrentUplinkTxPower = params->TxPower;
  /* USER CODE END OnTxData_1 */
}

static void OnJoinRequest(LmHandlerJoinParams_t *joinParams)
{
  /* USER CODE BEGIN OnJoinRequest_1 */
  if (joinParams != NULL)
  {
    if (joinParams->Status == LORAMAC_HANDLER_SUCCESS)
    {
#if defined(USE_LED) && defined(USE_LEDTIM)
      UTIL_TIMER_Stop(&JoinLedTimer);
#endif
#if defined(USE_LED)
      SYS_LED_Off(SYS_LED_BLUE) ;
#endif

      JoinStatus = DEVICE_JOINED;

      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### = JOINED = ");
      if (joinParams->Mode == ACTIVATION_TYPE_ABP)
      {
        APP_LOG(TS_OFF, VLEVEL_M, "ABP ======================\r\n");
      }
      else
      {
        APP_LOG(TS_OFF, VLEVEL_M, "OTAA =====================\r\n");
      }
      UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    }
    else
    {
      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### = JOIN FAILED\r\n");

      JoinStatus = DEVICE_NOT_JOINED;
      JoinAttempt++;
      APP_PRINTF("Join attempt count: %d\r\n", JoinAttempt);
    }
    PrintRemainingTxTime();
  }
  /* USER CODE END OnJoinRequest_1 */
}

static void OnMacProcessNotify(void)
{
  /* USER CODE BEGIN OnMacProcessNotify_1 */

  /* USER CODE END OnMacProcessNotify_1 */
  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LmHandlerProcess), CFG_SEQ_Prio_0);

  /* USER CODE BEGIN OnMacProcessNotify_2 */

  /* USER CODE END OnMacProcessNotify_2 */
}
