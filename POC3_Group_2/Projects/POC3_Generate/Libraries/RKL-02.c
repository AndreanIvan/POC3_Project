#include "RKL-02.h"
#include "ModbusMaster.h"
#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "stm32wlxx.h"
#include "math.h"

#define baud 9600
#define timeout 1500
#define polling 1000
#define retry_count 10
#define LED 9
#define TOTAL_NO_OF_REGISTERS 100

extern void PRINTF(const char *format, ...);

enum
{
  READ_REGISTER,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

void RKL_02Init( void )
{
    /*
   * Construct Packet untuk setiap req yang ada
   * - packet
   * - slave address
   * - function
   * - register address
   * - unsigned int count
   * - local reg address
   */
  modbus_construct(&packets[READ_REGISTER], 1, READ_HOLDING_REGISTERS, 2, 1, 0);

   /*
	* Konfigurasi pengiriman
	* - Baud Rate
	* - Timeout (ms)
	* - polling time (ms)
	* - retry count
	* - packets struct
	* - total packet number
	* - local register variable
	*/
  modbus_configure(baud, timeout, polling, retry_count, packets, TOTAL_NO_OF_PACKETS, regs);
}

void RKL_02Read( RKL_02Object_t *object )
{
	memset(regs, 0x00, sizeof(regs));

	modbus_update();

	PRINTF("%d",packets[0].id);
	PRINTF("%d",packets[0].function);
	PRINTF("%d",packets[0].address);
	PRINTF("%d",packets[0].data);
	PRINTF("%d\n",packets[0].local_start_address);

	PRINTF("%d",packets[0].requests);
	PRINTF("%d",packets[0].successful_requests);
	PRINTF("%d",packets[0].failed_requests);
	PRINTF("%d",packets[0].exception_errors);
	PRINTF("%d",packets[0].retries);
	PRINTF("%d\n",packets[0].connection);

	object->Distance = regs[0];

	PRINTF("Distance: %d\r\n", regs[0]);

}
