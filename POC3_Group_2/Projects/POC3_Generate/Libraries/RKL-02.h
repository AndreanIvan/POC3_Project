#ifndef RKL_02_H
#define RKL_02_H

#include <inttypes.h>

typedef struct
{
  uint16_t Distance;

} RKL_02Object_t;

void RKL_02Init( void );
void RKL_02Read( RKL_02Object_t *object );

#endif