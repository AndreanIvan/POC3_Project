#ifndef RK520_04_H
#define RK520_04_H

#include <inttypes.h>

typedef struct
{
  uint16_t Moisture1;
  uint16_t Moisture2;
  uint16_t Moisture3;
  uint16_t Moisture4;
  uint16_t Temp1;
  uint16_t Temp2;
  uint16_t Temp3;
  uint16_t Temp4;

} RK520_04Object_t;

void RK520_04Init( void );
void RK520_04Read( RK520_04Object_t *object );

#endif