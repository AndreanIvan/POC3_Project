#ifndef RK500_07_H
#define RK500_07_H

#include <inttypes.h>

typedef struct
{
  float turbidity;

} RK500_07Object_t;

void RK500_07Init( void );
void RK500_07Read( RK500_07Object_t *object );

#endif