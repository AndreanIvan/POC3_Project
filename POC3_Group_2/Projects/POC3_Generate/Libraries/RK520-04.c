#include "RK520-04.h"
#include "ModbusMaster.h"
#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "stm32wlxx.h"
#include "math.h"

#define baud 9600
#define timeout 1500
#define polling 1000
#define retry_count 10
#define LED 9
#define TOTAL_NO_OF_REGISTERS 100

extern void PRINTF(const char *format, ...);

enum
{
  READ_REGISTER,
  TOTAL_NO_OF_PACKETS // leave this last entry
};

// Create an array of Packets to be configured
Packet packets[TOTAL_NO_OF_PACKETS];

// Masters register array
unsigned int regs[TOTAL_NO_OF_REGISTERS];

void RK520_04Init( void )
{
    /*
   * Construct Packet untuk setiap req yang ada
   * - packet
   * - slave address
   * - function
   * - register address
   * - unsigned int count
   * - local reg address
   */
  modbus_construct(&packets[READ_REGISTER], 1, READ_HOLDING_REGISTERS, 0, 8, 0);

   /*
	* Konfigurasi pengiriman
	* - Baud Rate
	* - Timeout (ms)
	* - polling time (ms)
	* - retry count
	* - packets struct
	* - total packet number
	* - local register variable
	*/
  modbus_configure(baud, timeout, polling, retry_count, packets, TOTAL_NO_OF_PACKETS, regs);
}

void RK520_04Read( RK520_04Object_t *object )
{
	memset(regs, 0x00, sizeof(regs));

	modbus_update();

	PRINTF("%d",packets[0].id);
	PRINTF("%d",packets[0].function);
	PRINTF("%d",packets[0].address);
	PRINTF("%d",packets[0].data);
	PRINTF("%d\n",packets[0].local_start_address);

	PRINTF("%d",packets[0].requests);
	PRINTF("%d",packets[0].successful_requests);
	PRINTF("%d",packets[0].failed_requests);
	PRINTF("%d",packets[0].exception_errors);
	PRINTF("%d",packets[0].retries);
	PRINTF("%d\n",packets[0].connection);

	object->Moisture1 = regs[0];
	object->Temp1 = regs[1];
	object->Moisture2 = regs[2];
	object->Temp2 = regs[3];
	object->Moisture3 = regs[4];
	object->Temp3 = regs[5];
	object->Moisture4 = regs[6];
	object->Temp4 = regs[7];

	PRINTF("Moisture1: %d | ", regs[0]);
	PRINTF("Temp1: %d | ", regs[1]);
	PRINTF("Moisture2: %d | ", regs[2]);
	PRINTF("Temp2: %d | ", regs[3]);
	PRINTF("Moisture3: %d | ", regs[4]);
	PRINTF("Temp3: %d | ", regs[5]);
	PRINTF("Moisture4: %d | ", regs[6]);
	PRINTF("Temp4: %d\r\n", regs[7]);
}
