#ifndef __LIT_MPPT_TEST_H__
#define __LIT_MPPT_TEST_H__

#include "main.h"

#define SOLAR_ADC_CHANNEL				ADC_CHANNEL_10
#define	STEP_UP_12V_ADC_CHANNEL			ADC_CHANNEL_11
#define BATT_ADC_CHANNEL				ADC_CHANNEL_4
#define ICHARGE_ADC_CHANNEL				ADC_CHANNEL_2

#ifndef RS485_EN_GPIO_Port
#define RS485_EN_GPIO_Port          	GPIOA
#endif

#ifndef RS485_EN_Pin
#define RS485_EN_Pin                	GPIO_PIN_8
#endif

void RS485_Init(void);

void RS485_On(void);

void RS485_Off(void);

uint32_t MPPT_GetSolarVoltage(void);
uint32_t MPPT_GetStepUpVoltage(void);
uint32_t MPPT_GetBattVoltage(void);
uint32_t MPPT_GetIChargeVoltage(void);

uint32_t MPPT_GetIChargeCurrent(void);

uint32_t SolarVoltage, StepUpVoltage, BattVoltage, IChargeVoltage, IChargeCurrent;

SolarVoltage   = MPPT_GetSolarVoltage();
StepUpVoltage  = MPPT_GetStepUpVoltage();
BattVoltage    = MPPT_GetBattVoltage();
IChargeVoltage = MPPT_GetIChargeVoltage();
IChargeCurrent = MPPT_GetIChargeCurrent();

AppData.Buffer[0] = (uint8_t)(SolarVoltage >> 8) & 0xFF;
AppData.Buffer[1] = (uint8_t)(SolarVoltage & 0xFF);
AppData.Buffer[2] = (uint8_t)(StepUpVoltage >> 8) & 0xFF;
AppData.Buffer[3] = (uint8_t)(StepUpVoltage & 0xFF);
AppData.Buffer[4] = (uint8_t)(BattVoltage >> 8) & 0xFF;
AppData.Buffer[5] = (uint8_t)(BattVoltage & 0xFF);
AppData.Buffer[6] = (uint8_t)(IChargeVoltage >> 8) & 0xFF;
AppData.Buffer[7] = (uint8_t)(IChargeVoltage & 0xFF);
AppData.Buffer[8] = (uint8_t)(IChargeCurrent >> 8) & 0xFF;
AppData.Buffer[9] = (uint8_t)(IChargeCurrent & 0xFF);

     APP_LOG(TS_OFF, VLEVEL_M, "Send on LORAWAN_USER_APP_PORT (%d):\r\n"
   		  	  	  	  	  	  "- SolarVoltage   : %d\r\n"
                              "- StepUpVoltage  : %d\r\n"
                              "- BattVoltage    : %d\r\n"
                              "- IChargeVoltage : %d\r\n"
                              "- IChargeCurrent : %d\r\n"
								  LORAWAN_USER_APP_PORT,
								  SolarVoltage, StepUpVoltage, BattVoltage,
                                  IChargeVoltage, IChargeCurrent);

decoded.pressure = ( bytes[0] << 8 | bytes[1] )

decoded.SolarVoltage = ( bytes[0] << 8 | bytes[1] )
decoded.StepUpVoltage = ( bytes[2] << 8 | bytes[3] )
decoded.BattVoltage = ( bytes[4] << 8 | bytes[5] )
decoded.IChargeVoltage = ( bytes[6] << 8 | bytes[7] )
decoded.IChargeCurrent = ( bytes[8] << 8 | bytes[9] )

	APP_PRINTF("Channel 2  : %4d | %4d\r\n", c2, (uint16_t)(c2*3300 / 4095) );
	APP_PRINTF("Channel 3  : %4d | %4d\r\n", c3, (uint16_t)(c3*3300 / 4095) );
	APP_PRINTF("Channel 4  : %4d | %4d\r\n", c4, (uint16_t)(c4*3300 / 4095) );
	APP_PRINTF("Channel 5  : %4d | %4d\r\n", c5, (uint16_t)(c5*3300 / 4095) );
	APP_PRINTF("Channel 6  : %4d | %4d\r\n", c6, (uint16_t)(c6*3300 / 4095) );
	APP_PRINTF("Channel 7  : %4d | %4d\r\n", c7, (uint16_t)(c7*3300 / 4095) );
	APP_PRINTF("Channel 8  : %4d | %4d\r\n", c8, (uint16_t)(c8*3300 / 4095) );
	APP_PRINTF("Channel 9  : %4d | %4d\r\n", c9, (uint16_t)(c9*3300 / 4095) );
	APP_PRINTF("Channel 10 : %4d | %4d\r\n", c10, (uint16_t)(c10*3300 / 4095) );
	APP_PRINTF("Channel 11 : %4d | %4d\r\n", c11, (uint16_t)(c11*3300 / 4095) );
	APP_PRINTF("Channel 12 : %4d | %4d\r\n", c12, (uint16_t)(c12*3300 / 4095) );
	APP_PRINTF("Channel 13 : %4d | %4d\r\n", c13, (uint16_t)(c13*3300 / 4095) );
	APP_PRINTF("Channel 14 : %4d | %4d\r\n", c14, (uint16_t)(c14*3300 / 4095) );
	APP_PRINTF("Channel 15 : %4d | %4d\r\n", c15, (uint16_t)(c15*3300 / 4095) );
	APP_PRINTF("Channel 16 : %4d | %4d\r\n", c16, (uint16_t)(c16*3300 / 4095) );
	APP_PRINTF("Channel 17 : %4d | %4d\r\n", c17, (uint16_t)(c17*3300 / 4095) );

#endif /* __LIT_MPPT_TEST_H__ */
