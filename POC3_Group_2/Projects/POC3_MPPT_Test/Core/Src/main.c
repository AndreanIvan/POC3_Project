/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_lorawan.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sys_app.h"
#include "LIT_MPPT_Test.h"
#include "adc_if.h"
#include "stm32wlxx_hal_adc.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//#define SENSOR_READING_MODE
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#ifdef SENSOR_READING_MODE

	uint16_t SolarVoltage, StepUpVoltage, BattVoltage, IChargeVoltage, IChargeCurrent;

	uint16_t c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17;

	static uint8_t AdcInitialized = 0;

#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
static uint32_t ADC_ReadChannels(uint32_t channel);
//static void HW_AdcInit( void );
//static uint16_t HW_AdcReadChannel( uint32_t Channel );

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#if defined(SENSOR_READING_MODE)

#endif
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_LoRaWAN_Init();
  /* USER CODE BEGIN 2 */

#if defined(SENSOR_READING_MODE)
  APP_PRINTF("===== Continous Sensor Reading on POC3 Board =====\r\n");
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
#if !defined(SENSOR_READING_MODE)
    /* USER CODE END WHILE */
    MX_LoRaWAN_Process();

    /* USER CODE BEGIN 3 */
#else

//	  SolarVoltage   = MPPT_GetSolarVoltage();
//	  StepUpVoltage  = MPPT_GetStepUpVoltage();
//	  BattVoltage    = MPPT_GetBattVoltage();
//	  IChargeVoltage = MPPT_GetIChargeVoltage();
//	  IChargeCurrent = MPPT_GetIChargeCurrent();


//  c2 = HW_AdcReadChannel(ADC_CHANNEL_2);
//	c3 = HW_AdcReadChannel(ADC_CHANNEL_3);
//	c4 = HW_AdcReadChannel(ADC_CHANNEL_4);
//	c5 = HW_AdcReadChannel(ADC_CHANNEL_5);
//	c6 = HW_AdcReadChannel(ADC_CHANNEL_6);
//	c7 = HW_AdcReadChannel(ADC_CHANNEL_7);
//	c8 = HW_AdcReadChannel(ADC_CHANNEL_8);
//	c9 = HW_AdcReadChannel(ADC_CHANNEL_9);
//	c10 = HW_AdcReadChannel(ADC_CHANNEL_10);
//	c11 = HW_AdcReadChannel(ADC_CHANNEL_11);
//	c12 = HW_AdcReadChannel(ADC_CHANNEL_12);
//	c13 = HW_AdcReadChannel(ADC_CHANNEL_13);
//	c14 = HW_AdcReadChannel(ADC_CHANNEL_14);
//	c15 = HW_AdcReadChannel(ADC_CHANNEL_15);
//	c16 = HW_AdcReadChannel(ADC_CHANNEL_16);
//	c17 = HW_AdcReadChannel(ADC_CHANNEL_17);

    c2 = ADC_ReadChannels(ADC_CHANNEL_2);
    c3 = ADC_ReadChannels(ADC_CHANNEL_3);
    c4 = ADC_ReadChannels(ADC_CHANNEL_4);
    c6 = ADC_ReadChannels(ADC_CHANNEL_6);
    c7 = ADC_ReadChannels(ADC_CHANNEL_7);
    c8 = ADC_ReadChannels(ADC_CHANNEL_8);
    c9 = ADC_ReadChannels(ADC_CHANNEL_9);
    c10 = ADC_ReadChannels(ADC_CHANNEL_10);
    c11 = ADC_ReadChannels(ADC_CHANNEL_11);

    APP_PRINTF("Channel 2  : %4d | %4d mV  (PB3)\r\n", c2, (uint16_t)(c2*3300 / 4095) );
    APP_PRINTF("Channel 3  : %4d | %4d mV  (PB4)\r\n", c3, (uint16_t)(c3*3300 / 4095) );
    APP_PRINTF("Channel 4  : %4d | %4d mV  (PB2)\r\n", c4, (uint16_t)(c4*3300 / 4095) );
    APP_PRINTF("Channel 6  : %4d | %4d mV  (PA10)\r\n", c6, (uint16_t)(c6*3300 / 4095) );
    APP_PRINTF("Channel 7  : %4d | %4d mV  (PA11)\r\n", c7, (uint16_t)(c7*3300 / 4095) );
    APP_PRINTF("Channel 8  : %4d | %4d mV  (PA12)\r\n", c8, (uint16_t)(c8*3300 / 4095) );
    APP_PRINTF("Channel 9  : %4d | %4d mV  (PA13)\r\n", c9, (uint16_t)(c9*3300 / 4095) );
    APP_PRINTF("Channel 10 : %4d | %4d mV  (PA14)\r\n", c10, (uint16_t)(c10*3300 / 4095) );
    APP_PRINTF("Channel 11 : %4d | %4d mV  (PA15)\r\n", c11, (uint16_t)(c11*3300 / 4095) );

    APP_PRINTF("=================================================================\r\n");


    HAL_Delay(2000);

#endif
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK3|RCC_CLOCKTYPE_HCLK
                              |RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1
                              |RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
static uint32_t ADC_ReadChannels(uint32_t channel)
{
  /* USER CODE BEGIN ADC_ReadChannels_1 */

  /* USER CODE END ADC_ReadChannels_1 */
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;
  /* USER CODE BEGIN ADC_ReadChannels_2 */

  /* USER CODE END ADC_ReadChannels_2 */
}

//void HW_AdcInit( void )
//{
//  if( AdcInitialized == 0 )
//  {
//    AdcInitialized = 1;
//
//    hadc.Instance  = ADC;
//    hadc.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;
//
//    hadc.Init.LowPowerAutoPowerOff  = DISABLE;
//    hadc.Init.LowPowerAutoWait      = DISABLE;
//    hadc.Init.Resolution            = ADC_RESOLUTION_12B;
//    hadc.Init.ScanConvMode          = ADC_SCAN_DISABLE;
//
//    hadc.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
//    hadc.Init.ContinuousConvMode    = DISABLE;
//    hadc.Init.DiscontinuousConvMode = DISABLE;
//    hadc.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
//    hadc.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
//    hadc.Init.DMAContinuousRequests = DISABLE;
//
//	__HAL_RCC_ADC_CLK_ENABLE();
//
//    HAL_ADC_Init( &hadc );
//
//  }
//}

//uint16_t HW_AdcReadChannel( uint32_t Channel )
//{
//  APP_PRINTF("Entering HW_AdcReadChannel\n");
//  ADC_ChannelConfTypeDef adcConf;
//
//  uint16_t adcData = 0;
//  AdcInitialized = 0;
//  HW_AdcInit();
//  if( AdcInitialized == 1 )
//  {
//    /* wait the the Vrefint used by adc is set */
//    while (__HAL_PWR_GET_FLAG(PWR_FLAG_VREFINTRDY) == RESET) {};
//	__HAL_RCC_ADC_CLK_ENABLE();
//    adcConf.Channel = (uint32_t)0x0007FFFFU;
//    adcConf.Rank = (uint32_t)0x00001001U;
//    HAL_ADC_ConfigChannel( &hadc, &adcConf);
//    adcConf.Channel = Channel;
//    adcConf.Rank = ADC_REGULAR_RANK_1;
//    adcConf.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
//    HAL_ADC_ConfigChannel( &hadc, &adcConf);
//
//    /* Start the conversion process */
//    HAL_ADC_Start( &hadc);
//
//    /* Wait for the end of conversion */
//    HAL_ADC_PollForConversion( &hadc, HAL_MAX_DELAY );
//
//    /* Get the converted value of regular channel */
//    adcData = HAL_ADC_GetValue ( &hadc);
//    APP_PRINTF("HAL_ADC_GetValue = %d\n",adcData);
//    __HAL_ADC_DISABLE( &hadc) ;
//    HAL_ADC_DeInit( &hadc );
//	__HAL_RCC_ADC_CLK_DISABLE() ;
//	AdcInitialized = 0;
//  }
//  return adcData;
//}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  while (1)
  {
  }
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
