/**
  ******************************************************************************
  * @file    LIT_FirmwareVersion.h
  * @author  Andrean I.
  * @brief   Definition of the firmware version
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "custom_board_setting.h"

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_FIRMWARE_VERSION_H__
#define __LIT_FIRMWARE_VERSION_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Exported constants --------------------------------------------------------*/
#ifndef USE_CUSTOM_VERSION
#define __FW_VERSION_MAJOR    (0x01U) /*!< [15:12] major version */
#define __FW_VERSION_MINOR    (0x00U) /*!< [11:8] minor version */
#define __FW_VERSION_BUGFIX   (0x02U) /*!< [7:0]  bug fix version */
#else
#define __FW_VERSION_MAJOR    FW_VER_MAJ_CUSTOM
#define __FW_VERSION_MINOR    FW_VER_MIN_CUSTOM
#define __FW_VERSION_BUGFIX   FW_VER_BF_CUSTOM
#endif

#define __FW_VERSION_MAJOR_SHIFT    12  /*!< major byte shift */
#define __FW_VERSION_MINOR_SHIFT    8   /*!< minor byte shift */
#define __FW_VERSION_BUGFIX_SHIFT   0   /*!< bug fix byte shift */

/* Exported macros -----------------------------------------------------------*/
/**
  * @brief Application version
  */
#define __FW_VERSION          ((__FW_VERSION_MAJOR  << __FW_VERSION_MAJOR_SHIFT)\
                               |(__FW_VERSION_MINOR << __FW_VERSION_MINOR_SHIFT)\
                               |(__FW_VERSION_BUGFIX << __FW_VERSION_BUGFIX_SHIFT))

/**
  * @brief LoRaWAN application version
  */
#define __LIT_FW_VERSION            __FW_VERSION

#ifdef __cplusplus
}
#endif

#endif /*__LIT_FIRMWARE_VERSION_H__*/