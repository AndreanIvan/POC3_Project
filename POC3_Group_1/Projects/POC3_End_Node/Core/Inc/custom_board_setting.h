/** 
  ****************************************************************************** 
  * @file           : custom_board_setting.h 
  * @brief          : Custom board setting for POC 3 Project 
  * @author         : Andrean I. 
  * @date           : December 2021 
  ****************************************************************************** 
  */ 
 
#ifndef __CUSTOM_BOARD_SETTING_H__ 
#define __CUSTOM_BOARD_SETTING_H__ 

/* Board Selection:
 * - POC3_Test_Node
 */
#define POC3_Test_Node

#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_NOT_SUPPORTED

#define CUSTOM_APP_TX_UPLINK_DUTYCYCLE          15000
 
#endif /* __CUSTOM_IDENTITY_H__ */
