/** 
  ****************************************************************************** 
  * @file           : custom_board_setting.h 
  * @brief          : Custom board setting for POC 3 Project 
  * @author         : Andrean I. 
  * @date           : December 2021 
  ****************************************************************************** 
  */ 
 
#ifndef __CUSTOM_BOARD_SETTING_H__ 
#define __CUSTOM_BOARD_SETTING_H__ 

/* Board Selection:
 * - POC3_Test_Node_Watermark_Tensio
 */
#define POC3_Test_Node_Watermark_Tensio

#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_SUPPORTED

#define CUSTOM_APP_TX_UPLINK_DUTYCYCLE          300000

/* Firmware Version */
#define USE_CUSTOM_VERSION
#define FW_VER_MAJ_CUSTOM                       0x01
#define FW_VER_MIN_CUSTOM                       0x00
#define FW_VER_BF_CUSTOM                        0x03

#endif /* __CUSTOM_IDENTITY_H__ */
