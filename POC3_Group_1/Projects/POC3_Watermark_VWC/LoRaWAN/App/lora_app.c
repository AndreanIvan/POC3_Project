/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    lora_app.c
  * @author  MCD Application Team
  * @brief   Application of the LRWAN Middleware
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "Region.h" /* Needed for LORAWAN_DEFAULT_DATA_RATE */
#include "sys_app.h"
#include "lora_app.h"
#include "stm32_seq.h"
#include "stm32_timer.h"
#include "utilities_def.h"
#include "lora_app_version.h"
#include "lorawan_version.h"
#include "subghz_phy_version.h"
#include "lora_info.h"
#include "LmHandler.h"
#include "stm32_lpm.h"
#include "adc_if.h"
#include "sys_conf.h"
#include "CayenneLpp.h"
#include "sys_sensors.h"

/* USER CODE BEGIN Includes */
#include "LIT_FirmwareVersion.h"
#include "LIT_Watermark.h"
#include "LIT_SDI12.h"
/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */

/**
  * @brief If confirmed message state is true, specifies the uplink trials of unconfirmed message
  */
uint8_t NbTrials = DEFAULT_NB_TRIALS;

/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/**
  * @brief LoRa State Machine states
  */
typedef enum TxEventType_e
{
  /**
    * @brief Appdata Transmission issue based on timer every TxDutyCycleTime
    */
  TX_ON_TIMER,
  /**
    * @brief Appdata Transmission external event plugged on OnSendEvent( )
    */
  TX_ON_EVENT
  /* USER CODE BEGIN TxEventType_t */

  /* USER CODE END TxEventType_t */
} TxEventType_t;

/* USER CODE BEGIN PTD */
typedef enum DeviceJoinStatus_e
{
	DEVICE_NOT_JOINED = 0,
	DEVICE_JOINED = !DEVICE_NOT_JOINED
} DeviceJoinStatus_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private function prototypes -----------------------------------------------*/
/**
  * @brief  LoRa End Node send request
  */
static void SendTxData(void);

/**
  * @brief  TX timer callback function
  * @param  context ptr of timer context
  */
static void OnTxTimerEvent(void *context);

/**
  * @brief  join event callback function
  * @param  joinParams status of join
  */
static void OnJoinRequest(LmHandlerJoinParams_t *joinParams);

/**
  * @brief  tx event callback function
  * @param  params status of last Tx
  */
static void OnTxData(LmHandlerTxParams_t *params);

/**
  * @brief callback when LoRa application has received a frame
  * @param appData data received in the last Rx
  * @param params status of last Rx
  */
static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params);

/*!
 * Will be called each time a Radio IRQ is handled by the MAC layer
 *
 */
static void OnMacProcessNotify(void);

/* USER CODE BEGIN PFP */

/**
  * @brief  LED Tx timer callback function
  * @param  context ptr of LED context
  */
static void OnTxTimerLedEvent(void *context);

/**
  * @brief  LED Rx timer callback function
  * @param  context ptr of LED context
  */
static void OnRxTimerLedEvent(void *context);

/**
  * @brief  LED Join timer callback function
  * @param  context ptr of LED context
  */
static void OnJoinTimerLedEvent(void *context);

/* USER CODE END PFP */

/* Private variables ---------------------------------------------------------*/
static ActivationType_t ActivationType = LORAWAN_DEFAULT_ACTIVATION_TYPE;

/**
  * @brief LoRaWAN handler Callbacks
  */
static LmHandlerCallbacks_t LmHandlerCallbacks =
{
  .GetBatteryLevel =           GetBatteryLevel,
  .GetTemperature =            GetTemperatureLevel,
  .GetUniqueId =               GetUniqueId,
  .GetDevAddr =                GetDevAddr,
  .OnMacProcess =              OnMacProcessNotify,
  .OnJoinRequest =             OnJoinRequest,
  .OnTxData =                  OnTxData,
  .OnRxData =                  OnRxData
};

/**
  * @brief LoRaWAN handler parameters
  */
static LmHandlerParams_t LmHandlerParams =
{
  .ActiveRegion =             ACTIVE_REGION,
  .DefaultClass =             LORAWAN_DEFAULT_CLASS,
  .AdrEnable =                LORAWAN_ADR_STATE,
  .TxDatarate =               LORAWAN_DEFAULT_DATA_RATE,
  .PingPeriodicity =          LORAWAN_DEFAULT_PING_SLOT_PERIODICITY
};

/**
  * @brief Type of Event to generate application Tx
  */
static TxEventType_t EventType = TX_ON_TIMER;

/**
  * @brief Timer to handle the application Tx
  */
static UTIL_TIMER_Object_t TxTimer;

/* USER CODE BEGIN PV */
/**
  * @brief User application buffer
  */
static uint8_t AppDataBuffer[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];

/**
  * @brief User application data structure
  */
static LmHandlerAppData_t AppData = { 0, 0, AppDataBuffer };

/**
  * @brief Specifies the state of the application LED
  */
static uint8_t AppLedStateOn = RESET;

/**
  * @brief Timer to handle the application Tx Led to toggle
  */
static UTIL_TIMER_Object_t TxLedTimer;

/**
  * @brief Timer to handle the application Rx Led to toggle
  */
static UTIL_TIMER_Object_t RxLedTimer;

/**
  * @brief Timer to handle the application Join Led to toggle
  */
static UTIL_TIMER_Object_t JoinLedTimer;

/**
  * @brief Specifies the device join status
  */
static uint8_t JoinStatus = DEVICE_NOT_JOINED;

/**
  * @brief Specifies the device join request attempts
  */
static uint8_t JoinAttempt = 0;

/**
  * @brief Specifies the application uplink duty cycle
  */
static uint32_t UplinkDutyCycle	= APP_TX_UPLINK_DUTYCYCLE;

/**
  * @brief Specifies the application uplink duty cycle maximum random value
  */
static uint32_t UplinkDutyCycle_MaxDeltaRandom = 0;

/**
  * @brief Specifies the application join request duty cycle maximum random value
  */
static uint32_t JoinDutyCycle_MaxDeltaRandom = 0;

/**
  * @brief Specifies Tx period of the next transmission
  */
static uint32_t NextTxPeriod = 0;

/**
  * @brief Specifies LoRaWAN App Port
  */
static uint8_t AppPort = LORAWAN_DEFAULT_PORT;

/**
  * @brief Specifies the confirmed message state
  */
static uint8_t ConfirmMsgState = LORAWAN_DEFAULT_CONFIRMED_MSG_STATE;

/**
  * @brief Variable to store last downlink RSSI
  */
static int8_t LastDownlinkRSSI = 0xFF;

/**
  * @brief Variable to store last downlink SNR
  */
static int8_t LastDownlinkSNR = -128;

/**
  * @brief Variable to store last downlink Tx Power
  */
static int8_t CurrentUplinkTxPower = 0;

/**
  * @brief Store last temperature from 5TE soil sensor in Celcius degree
  */
static uint32_t SDI_LastTemperature = 240;

/* USER CODE END PV */

/* Exported functions ---------------------------------------------------------*/
/* USER CODE BEGIN EF */

/* USER CODE END EF */

void LoRaWAN_Init(void)
{
  /* USER CODE BEGIN LoRaWAN_Init_1 */

//#define USE_LED
//#define USE_LEDTIM
#define USE_PB

	SYS_LED_Init(SYS_LED_BLUE);

#if defined(USE_LED)
  SYS_LED_Init(SYS_LED_BLUE);
#endif
#if defined(USE_PB)
  SYS_PB_Init(SYS_BUTTON1, SYS_BUTTON_MODE_EXTI);
#endif

  /* Get LIT Modified Firmware Version*/
  APP_LOG(TS_OFF, VLEVEL_M, "LIT_FW_VERSION:     V%X.%X.%X\r\n",
          (uint8_t)(__FW_VERSION_MAJOR),
          (uint8_t)(__FW_VERSION_MINOR),
          (uint8_t)(__FW_VERSION_BUGFIX));

  /* Get LoRa APP version*/
  APP_LOG(TS_OFF, VLEVEL_M, "APP_VERSION:        V%X.%X.%X\r\n",
          (uint8_t)(__LORA_APP_VERSION >> __APP_VERSION_MAIN_SHIFT),
          (uint8_t)(__LORA_APP_VERSION >> __APP_VERSION_SUB1_SHIFT),
          (uint8_t)(__LORA_APP_VERSION >> __APP_VERSION_SUB2_SHIFT));

  /* Get MW LoraWAN info */
  APP_LOG(TS_OFF, VLEVEL_M, "MW_LORAWAN_VERSION: V%X.%X.%X\r\n",
          (uint8_t)(__LORAWAN_VERSION >> __APP_VERSION_MAIN_SHIFT),
          (uint8_t)(__LORAWAN_VERSION >> __APP_VERSION_SUB1_SHIFT),
          (uint8_t)(__LORAWAN_VERSION >> __APP_VERSION_SUB2_SHIFT));

  /* Get MW SubGhz_Phy info */
  APP_LOG(TS_OFF, VLEVEL_M, "MW_RADIO_VERSION:   V%X.%X.%X\r\n",
          (uint8_t)(__SUBGHZ_PHY_VERSION >> __APP_VERSION_MAIN_SHIFT),
          (uint8_t)(__SUBGHZ_PHY_VERSION >> __APP_VERSION_SUB1_SHIFT),
          (uint8_t)(__SUBGHZ_PHY_VERSION >> __APP_VERSION_SUB2_SHIFT));

#if defined(USE_LED) && defined(USE_LEDTIM)
  UTIL_TIMER_Create(&TxLedTimer, 0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnTxTimerLedEvent, NULL);
  UTIL_TIMER_Create(&RxLedTimer, 0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnRxTimerLedEvent, NULL);
  UTIL_TIMER_Create(&JoinLedTimer, 0xFFFFFFFFU, UTIL_TIMER_PERIODIC, OnJoinTimerLedEvent, NULL);
  UTIL_TIMER_SetPeriod(&TxLedTimer, 500);
  UTIL_TIMER_SetPeriod(&RxLedTimer, 500);
  UTIL_TIMER_SetPeriod(&JoinLedTimer, 500);
#endif

  /* USER CODE END LoRaWAN_Init_1 */

  UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LmHandlerProcess), UTIL_SEQ_RFU, LmHandlerProcess);
  UTIL_SEQ_RegTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), UTIL_SEQ_RFU, SendTxData);
  /* Init Info table used by LmHandler*/
  LoraInfo_Init();

  /* Init the Lora Stack*/
  LmHandlerInit(&LmHandlerCallbacks);

  LmHandlerConfigure(&LmHandlerParams);

  /* USER CODE BEGIN LoRaWAN_Init_2 */
#if defined(USE_LED) && defined(USE_LEDTIM)
  UTIL_TIMER_Start(&JoinLedTimer);
#endif
  SYS_PB_Init(BUTTON_SW1, BUTTON_MODE_EXTI);
  /* USER CODE END LoRaWAN_Init_2 */

  LmHandlerJoin(ActivationType);

  if (EventType == TX_ON_TIMER)
  {
    /* send every time timer elapses */
    UTIL_TIMER_Create(&TxTimer,  0xFFFFFFFFU, UTIL_TIMER_ONESHOT, OnTxTimerEvent, NULL);
    UTIL_TIMER_SetPeriod(&TxTimer,  APP_TX_DUTYCYCLE);
    UTIL_TIMER_Start(&TxTimer);
  }
  else
  {
    /* USER CODE BEGIN LoRaWAN_Init_3 */
#if defined(USE_PB)
    /* send every time button is pushed */
    SYS_PB_Init(BUTTON_SW1, BUTTON_MODE_EXTI);
#endif
    /* USER CODE END LoRaWAN_Init_3 */
  }

  /* USER CODE BEGIN LoRaWAN_Init_Last */

  /* USER CODE END LoRaWAN_Init_Last */
}

/* USER CODE BEGIN PB_Callbacks */
/* Note: Current the stm32wlxx_it.c generated by STM32CubeMX does not support BSP for PB in EXTI mode. */
/* In order to get a push button IRS by code automatically generated */
/* HAL_GPIO_EXTI_Callback is today the only available possibility. */
/* Using HAL_GPIO_EXTI_Callback() shortcuts the BSP. */
/* If users wants to go through the BSP, stm32wlxx_it.c should be updated  */
/* in the USER CODE SESSION of the correspondent EXTIn_IRQHandler() */
/* to call the SYS_PB_IRQHandler() or the HAL_EXTI_IRQHandler(&H_EXTI_n);. */
/* Then the below HAL_GPIO_EXTI_Callback() can be replaced by BSP callback */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  switch (GPIO_Pin)
  {
    case  SYS_BUTTON1_PIN:
    {
    	uint32_t TxTimerRemaining;
        UTIL_TIMER_GetRemainingTime(&TxTimer, &TxTimerRemaining);
        APP_PRINTF("================================================================================\r\n\n");

        APP_PRINTF("User button pressed. Sending data...\r\n");
        APP_PRINTF("Time remaining for the next periodic uplink: %d ms\r\n", TxTimerRemaining);
        UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    }
      break;
    default:
      break;
  }
}

/* USER CODE END PB_Callbacks */

/* Private functions ---------------------------------------------------------*/
/* USER CODE BEGIN PrFD */

/* USER CODE END PrFD */

static void OnRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params)
{
  /* USER CODE BEGIN OnRxData_1 */
  if ((appData != NULL) || (params != NULL))
  {
#if defined(USE_LED)
    SYS_LED_On(SYS_LED_BLUE) ;
#endif
#if defined(USE_LED) && defined(USE_LEDTIM)
    UTIL_TIMER_Start(&RxLedTimer);
#endif
    static const char *slotStrings[] = { "1", "2", "C", "C Multicast", "B Ping-Slot", "B Multicast Ping-Slot" };

    APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### ========== MCPS-Indication ==========\r\n");
    APP_LOG(TS_OFF, VLEVEL_H, "###### D/L FRAME:%04d | SLOT:%s | PORT:%d | DR:%d | RSSI:%d | SNR:%d\r\n",
            params->DownlinkCounter, slotStrings[params->RxSlot], appData->Port, params->Datarate, params->Rssi, params->Snr);

    LastDownlinkRSSI = params->Rssi;
    LastDownlinkSNR = params->Snr;

    if (appData->Port != 0)
    	APP_LOG(TS_OFF, VLEVEL_L, "Downlink on port %d:\r\n", appData->Port);

    switch (appData->Port)
    {
      case LORAWAN_SWITCH_CLASS_PORT:
        /*this port switches the class*/
        if (appData->BufferSize == 1)
        {
          switch (appData->Buffer[0])
          {
            case 0:
            {
              LmHandlerRequestClass(CLASS_A);
              break;
            }
            case 1:
            {
              LmHandlerRequestClass(CLASS_B);
              break;
            }
            case 2:
            {
              LmHandlerRequestClass(CLASS_C);
              break;
            }
            default:
              break;
          }
        }
        break;
      case LORAWAN_USER_APP_PORT:
        if (appData->BufferSize == 1)
        {
          AppLedStateOn = appData->Buffer[0] & 0x01;
          if (AppLedStateOn == RESET)
          {
            APP_LOG(TS_OFF, VLEVEL_H,   "LED OFF\r\n");
#if defined(USE_LED)
            SYS_LED_Off(SYS_LED_BLUE) ;
#endif
          }
          else
          {
            APP_LOG(TS_OFF, VLEVEL_H, "LED ON\r\n");
#if defined(USE_LED)
            SYS_LED_On(SYS_LED_BLUE) ;
#endif
          }
        }
        break;

      // Port 128
      case LORAWAN_DUTYCYCLE_CONFIG_PORT:
    	if (appData->BufferSize == 1)
    	{
    		if (appData->Buffer[0] != 0x00)
    		{
				AppPort = LORAWAN_DUTYCYCLE_CONFIG_PORT;

    			UplinkDutyCycle = appData->Buffer[0] * 60000;
    			APP_LOG(TS_OFF, VLEVEL_L, "Set Tx duty cycle to %d minute(s)\r\n", appData->Buffer[0]);

    			UTIL_TIMER_Stop(&TxTimer);

    			UplinkDutyCycle_MaxDeltaRandom = ( UplinkDutyCycle * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
    			NextTxPeriod = randr(UplinkDutyCycle - UplinkDutyCycle_MaxDeltaRandom, UplinkDutyCycle + UplinkDutyCycle_MaxDeltaRandom);

    			UTIL_TIMER_SetPeriod(&TxTimer, NextTxPeriod);

				UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);

   				APP_PRINTF("Change next uplink attempt to %d ms\r\n", NextTxPeriod);

    			/*Wait for next tx slot*/
    			UTIL_TIMER_Start(&TxTimer);

    		}
    		else
    		{
    			APP_LOG(TS_OFF, VLEVEL_L, "Payload of Tx duty cycle error!\r\n", appData->Buffer[0]);
    		}
    	}
      break;

      // Port 129
      case LORAWAN_STATUS_REQUEST_PORT:
    	if (appData->BufferSize == 1)
    	{
    		if (appData->Buffer[0] == 0x01)
    		{
    			APP_LOG(TS_OFF, VLEVEL_L, "Preparing to send status request...\r\n");
    			AppPort = LORAWAN_STATUS_REQUEST_PORT;
    			UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    		}
    	}
      break;

      // Port 130
      case LORAWAN_CONFIRMED_MSG_CONFIG_PORT:
    	if (appData->BufferSize == 1)
    	{
        AppPort = LORAWAN_CONFIRMED_MSG_CONFIG_PORT;
    		if (appData->Buffer[0] == 0x00)
    		{
    			ConfirmMsgState = LORAMAC_HANDLER_UNCONFIRMED_MSG;
    			APP_LOG(TS_OFF, VLEVEL_L, "Confirm message state is set to unconfirmed.\r\n");
    		}
    		else
    		{
    			ConfirmMsgState = LORAMAC_HANDLER_CONFIRMED_MSG;
    			NbTrials = appData->Buffer[0];
    			APP_LOG(TS_OFF, VLEVEL_L, "Confirm message state is set to confirmed with %d NbTrials.\r\n", NbTrials);
    		}
        UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    	}
      break;

      default:
        break;
    }
  }
  /* USER CODE END OnRxData_1 */
}

static void SendTxData(void)
{
  /* USER CODE BEGIN SendTxData_1 */

  UTIL_TIMER_Time_t nextTxIn = 0;

  switch (AppPort)
  {
    case LORAWAN_USER_APP_PORT:
    {
      AppData.Port = LORAWAN_USER_APP_PORT;

#define READ_WM_DEBUG
#if !defined(READ_WM_DEBUG)

      float SDI_param1 = 0, SDI_param2 = 0, SDI_param3 = 0;
      uint32_t dielectricPermittivity = 0, electricConductivity = 0, temperature = 0;
      uint8_t SDI12_MeasureTrial = 0;

      uint16_t Resistance_1 = 0;
      uint16_t Resistance_2 = 0;

      SDI12_Measures(&SDI_param1, &SDI_param2, &SDI_param3, NULL);
      SDI12_Measures(&SDI_param1, &SDI_param2, &SDI_param3, NULL);

      switch (Watermark_GetResistance(&Resistance_1, 0))
      {
      	  case SENSOR_SHORTED :
      		AppData.Buffer[0] = 0xFF;
      		AppData.Buffer[1] = 0xFF;
      		break;
      	  default:
      		AppData.Buffer[0] = (uint8_t)((Resistance_1 >> 8) & 0xFF);
      	    AppData.Buffer[1] = (uint8_t)(Resistance_1 & 0xFF);
      	    break;
      }

      switch (Watermark_GetResistance(&Resistance_2, 1))
      {
		  case SENSOR_SHORTED :
			AppData.Buffer[2] = 0xFF;
			AppData.Buffer[3] = 0xFF;
			break;
		  default:
			AppData.Buffer[2] = (uint8_t)((Resistance_2 >> 8) & 0xFF);
			AppData.Buffer[3] = (uint8_t)(Resistance_2 & 0xFF);
			break;
      }

      /*** Error handling and notification ***/

      if (Resistance_1 < 100)
    	  APP_PRINTF("Error on WM1: Sensor shorted\r\n")
      else if (Resistance_1 >= 30000)
    	  APP_PRINTF("Warning on WM1: Sensor not connected or soil is too dry\r\n");

      if (Resistance_2 < 100)
    	  APP_PRINTF("Error on WM2: Sensor shorted\r\n")
      else if (Resistance_2 >= 30000)
    	  APP_PRINTF("Warning on WM2: Sensor not connected or soil is too dry\r\n");

      while ( (SDI12_MeasureTrial < 5) && (SDI_param1 == 0.0) && (SDI_param1 == 0.0) && (SDI_param1 == 0.0) )
      {
    	  APP_PRINTF("Failed to read 5TE sensor. Reading trials: %d. Retrying to read...\r\n", SDI12_MeasureTrial);
    	  HAL_Delay(2000);
    	  SDI12_MeasureTrial++;

		  SDI12_Measures(&SDI_param1, &SDI_param2, &SDI_param3, NULL);
		  SDI12_Measures(&SDI_param1, &SDI_param2, &SDI_param3, NULL);
      }

      dielectricPermittivity = (uint16_t)(SDI_param1 * 100);
	  electricConductivity	 = (uint16_t)(SDI_param2 * 100);
	  temperature			 = (uint16_t)(SDI_param3 * 10);

      if (SDI12_MeasureTrial < 5)
      {
          AppData.Buffer[4] = (uint8_t)((dielectricPermittivity >> 8) & 0xFF);
          AppData.Buffer[5] = (uint8_t)(dielectricPermittivity & 0xFF);
          AppData.Buffer[6] = (uint8_t)((temperature >> 8) & 0xFF);
          AppData.Buffer[7] = (uint8_t)(temperature & 0xFF);
          AppData.Buffer[8] = (uint8_t)((electricConductivity >> 8) & 0xFF);
          AppData.Buffer[9] = (uint8_t)(electricConductivity & 0xFF);

    	  SDI_LastTemperature = temperature;
      }
      else
      {
    	  APP_PRINTF("Failed to read 5TE sensor. Check sensor connection.\r\n");

          AppData.Buffer[4] = 0xFF;
          AppData.Buffer[5] = 0xFF;
          AppData.Buffer[6] = (uint8_t)((SDI_LastTemperature >> 8) & 0xFF);
          AppData.Buffer[7] = (uint8_t)(SDI_LastTemperature & 0xFF);
          AppData.Buffer[8] = 0xFF;
          AppData.Buffer[9] = 0xFF;
      }

      AppData.BufferSize = 10;

//      int i;
//      for (i=0; i < 10; i++)
//      {
//    	  APP_PRINTF("Buffer %d: %d\r\n", i, AppData.Buffer[i]);
//      }

      APP_LOG(TS_OFF, VLEVEL_M, "Send on LORAWAN_USER_APP_PORT (%d):\r\n"
    		  	  	  	  	  	  "- Watermark Ch1 Resistance : %4d ohm\r\n"
	  	  	  	  	  	  	  	  "- Watermark Ch2 Resistance : %4d ohm\r\n"
	  	  	  	  	  	  	  	  "- Dielectric Permittivity  : %2d.%02d \r\n"
	  	  	  	  	  	  	  	  "- Electric Conductivity    : %d uS/cm\r\n"
			  	  	  	  	  	  "- Temperature              : %2d.%d ºC\r\n",
								  LORAWAN_USER_APP_PORT,
								  Resistance_1, Resistance_2,
								  dielectricPermittivity / 100, dielectricPermittivity % 100,
								  electricConductivity * 10,
								  temperature / 10, temperature % 10);

#else

      uint16_t Resistance_1 = 0;
      uint16_t Resistance_2 = 0;
      uint16_t BattLevel = SYS_GetBatteryLevel();
      uint16_t VRef, Res_Cal;

      SYS_LED_On(SYS_LED_BLUE) ;

      switch (Watermark_GetResistance(&Resistance_1, WM_CHANNEL_1))
      {
      	  case SENSOR_SHORTED :
      		AppData.Buffer[0] = 0xFF;
      		AppData.Buffer[1] = 0xFF;
      		break;
      	  default:
      		AppData.Buffer[0] = (uint8_t)((Resistance_1 >> 8) & 0xFF);
      	    AppData.Buffer[1] = (uint8_t)(Resistance_1 & 0xFF);
      	    break;
      }

      switch (Watermark_GetResistance(&Resistance_2, WM_CHANNEL_2))
      {
		  case SENSOR_SHORTED :
			AppData.Buffer[2] = 0xFF;
			AppData.Buffer[3] = 0xFF;
			break;
		  default:
			AppData.Buffer[2] = (uint8_t)((Resistance_2 >> 8) & 0xFF);
			AppData.Buffer[3] = (uint8_t)(Resistance_2 & 0xFF);
			break;
      }

      Watermark_GetDebugInfo(&VRef, &Res_Cal);

//      SYS_LED_Off(SYS_LED_BLUE) ;

      /*** Error handling and notification ***/

      if (Resistance_1 < 100)
    	  APP_PRINTF("Error on WM1: Sensor shorted\r\n")
      else if (Resistance_1 >= 30000)
    	  APP_PRINTF("Warning on WM1: Sensor not connected or soil is too dry\r\n");

      if (Resistance_2 < 100)
    	  APP_PRINTF("Error on WM2: Sensor shorted\r\n")
      else if (Resistance_2 >= 30000)
    	  APP_PRINTF("Warning on WM2: Sensor not connected or soil is too dry\r\n");

      AppData.Buffer[4] = (uint8_t)((VRef >> 8) & 0xFF);
      AppData.Buffer[5] = (uint8_t)(VRef & 0xFF);
      AppData.Buffer[6] = (uint8_t)((BattLevel >> 8) & 0xFF);
      AppData.Buffer[7] = (uint8_t)(BattLevel & 0xFF);
      AppData.Buffer[8] = (uint8_t)((Res_Cal >> 8) & 0xFF);
      AppData.Buffer[9] = (uint8_t)(Res_Cal & 0xFF);


      APP_LOG(TS_OFF, VLEVEL_M, "Send on LORAWAN_USER_APP_PORT (%d):\r\n"
    		  	  	  	  	  	  "- WM1 Resistance : %5d ohm\r\n"
	  	  	  	  	  	  	  	  "- WM2 Resistance : %5d ohm\r\n"
    		  	  	  	  	  	  "- WM VRef        : %2d.%03d V\r\n"
    		  	  	  	  	  	  "- Batt Level     : %2d.%03d V\r\n"
								  "- Callib Res     : %2d.%03d ohm\r\n",
								  LORAWAN_USER_APP_PORT,
								  Resistance_1, Resistance_2,
								  VRef / 1000, VRef % 1000,
								  BattLevel / 1000, BattLevel % 1000,
								  Res_Cal / 1000, Res_Cal % 1000
								  );

      AppData.BufferSize = 10;

#endif

    } /* LORAWAN_USER_APP_PORT */
    break;
    case LORAWAN_DUTYCYCLE_CONFIG_PORT:
    {
      AppData.Port = LORAWAN_DUTYCYCLE_CONFIG_PORT;
      AppData.Buffer[0] = (uint8_t)(UplinkDutyCycle / 60000);
      AppData.BufferSize = 1;

      AppPort = LORAWAN_DEFAULT_PORT;

      APP_LOG(TS_OFF, VLEVEL_L, "Send on LORAWAN_DUTYCYCLE_CONFIG_PORT (128): %d minute(s) duty cycle.\r\n", AppData.Buffer[0]);
    }
    break;
    case LORAWAN_STATUS_REQUEST_PORT:
    {
    	// Crosscheck dengan payload
      AppData.Port = LORAWAN_STATUS_REQUEST_PORT;

      LmHandlerGetTxPower(&CurrentUplinkTxPower);

      AppData.Buffer[0] = (uint8_t)(LastDownlinkRSSI + 180) & 0xFF;
      AppData.Buffer[1] = (uint8_t)LastDownlinkSNR;
      AppData.Buffer[2] = (uint8_t)CurrentUplinkTxPower;
      AppData.Buffer[3] = (uint8_t)(UplinkDutyCycle / 60000);
      AppData.Buffer[4] = (uint8_t)(SYS_GetBatteryLevel()/10 - 200);
      AppData.Buffer[5] = (uint8_t)(SYS_GetTemperatureLevel() >> 8);
      AppData.Buffer[6] = (uint8_t)(__FW_VERSION_MAJOR << 4 | __FW_VERSION_MINOR);
      AppData.Buffer[7] = (uint8_t)(__FW_VERSION_BUGFIX);

      AppData.BufferSize = 8;

      AppPort = LORAWAN_DEFAULT_PORT;

      APP_LOG(TS_OFF, VLEVEL_L, "Send on LORAWAN_STATUS_REQUEST_PORT (129):\r\n"
    		  "LastDownlinkRSSI     : %d\r\n"
    		  "LastDownlinkSNR      : %d\r\n"
    		  "CurrentUplinkTxPower : %d\r\n"
    		  "UplinkDutyCycle      : %d\r\n"
    		  "BatteryLevel         : %d\r\n"
    		  "Temperature          : %d\r\n"
    		  "Version              : %d.%d.%d\r\n"
    		  , AppData.Buffer[0]-180, AppData.Buffer[1], AppData.Buffer[2], AppData.Buffer[3],
			  AppData.Buffer[4], AppData.Buffer[5], AppData.Buffer[6] >> 4, AppData.Buffer[6] & 0x0F, AppData.Buffer[7]);

      LastDownlinkRSSI = 0xFF;
      LastDownlinkSNR = -128;

    }
    break;
    case LORAWAN_CONFIRMED_MSG_CONFIG_PORT:
    {
      AppData.Port = LORAWAN_CONFIRMED_MSG_CONFIG_PORT;
      if (ConfirmMsgState == LORAMAC_HANDLER_UNCONFIRMED_MSG)
      {
        AppData.Buffer[0] = 0x00;
        APP_PRINTF("Send on LORAWAN_CONFIRMED_MSG_CONFIG_PORT: unconfirmed message.\r\n");
      }
      else
      {
        AppData.Buffer[0] = NbTrials;
        APP_PRINTF("Send on LORAWAN_CONFIRMED_MSG_CONFIG_PORT: confirmed message with %d NbTrials\r\n", AppData.Buffer[0]);
      }
      AppData.BufferSize = 1;
      AppPort = LORAWAN_DEFAULT_PORT;
    }
    break;
    default:
    break;

  }

  if (LORAMAC_HANDLER_SUCCESS == LmHandlerSend(&AppData, ConfirmMsgState, &nextTxIn, false))
  {
    APP_LOG(TS_ON, VLEVEL_L, "SEND REQUEST\r\n");
  }
  else if (nextTxIn > 0)
  {
    APP_LOG(TS_ON, VLEVEL_L, "Next Tx in  : ~%d second(s)\r\n", (nextTxIn / 1000));
  }


  /* USER CODE END SendTxData_1 */
}

static void OnTxTimerEvent(void *context)
{
  /* USER CODE BEGIN OnTxTimerEvent_1 */
    APP_PRINTF("======================================================================\r\n\n");

	if (JoinStatus == DEVICE_NOT_JOINED && JoinAttempt < JOIN_DUTYCYCLE_MAX_ATTEMPT)
	{
		JoinDutyCycle_MaxDeltaRandom = ( APP_TX_JOIN_DUTYCYCLE * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
		NextTxPeriod = randr(APP_TX_JOIN_DUTYCYCLE - JoinDutyCycle_MaxDeltaRandom, APP_TX_JOIN_DUTYCYCLE + JoinDutyCycle_MaxDeltaRandom);

		APP_PRINTF("Next uplink or join attempt in %d ms\r\n", NextTxPeriod);
	}
	else
	{
		if (JoinAttempt == JOIN_DUTYCYCLE_MAX_ATTEMPT)
		{
			APP_PRINTF("Maximum attempt using join dutycycle reached!\r\n");
		}

		UplinkDutyCycle_MaxDeltaRandom = ( UplinkDutyCycle * APP_TX_DUTYCYCLE_RANDOMIZE_PERCENTAGE / 100 / 2 );
		NextTxPeriod = randr(UplinkDutyCycle - UplinkDutyCycle_MaxDeltaRandom, UplinkDutyCycle + UplinkDutyCycle_MaxDeltaRandom);

		if (JoinStatus == DEVICE_NOT_JOINED)
		{
			APP_PRINTF("Next uplink or join attempt in %d ms\r\n", NextTxPeriod);
		}
		else
		{
			APP_PRINTF("Next uplink attempt in %d ms\r\n", NextTxPeriod);
		}
	}

	UTIL_TIMER_SetPeriod(&TxTimer, NextTxPeriod);
  /* USER CODE END OnTxTimerEvent_1 */
  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);

  /*Wait for next tx slot*/
  UTIL_TIMER_Start(&TxTimer);
  /* USER CODE BEGIN OnTxTimerEvent_2 */

  /* USER CODE END OnTxTimerEvent_2 */
}

/* USER CODE BEGIN PrFD_LedEvents */
static void OnTxTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Off(SYS_LED_BLUE) ;
#endif
}

static void OnRxTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Off(SYS_LED_BLUE) ;
#endif
}

static void OnJoinTimerLedEvent(void *context)
{
#if defined(USE_LED)
  SYS_LED_Toggle(SYS_LED_BLUE) ;
#endif
}

/* USER CODE END PrFD_LedEvents */

static void OnTxData(LmHandlerTxParams_t *params)
{
  /* USER CODE BEGIN OnTxData_1 */
  if ((params != NULL))
  {
    /* Process Tx event only if its a mcps response to prevent some internal events (mlme) */
    if (params->IsMcpsConfirm != 0)
    {
#if defined(USE_LED)
      SYS_LED_On(SYS_LED_BLUE) ;
#endif
#if defined(USE_LED) && defined(USE_LEDTIM)
      UTIL_TIMER_Start(&TxLedTimer);
#endif

      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### ========== MCPS-Confirm =============\r\n");
      APP_LOG(TS_OFF, VLEVEL_H, "###### U/L FRAME:%04d | PORT:%d | DR:%d | PWR:%d", params->UplinkCounter,
              params->AppData.Port, params->Datarate, params->TxPower);

      APP_LOG(TS_OFF, VLEVEL_H, " | MSG TYPE:");
      if (params->MsgType == LORAMAC_HANDLER_CONFIRMED_MSG)
      {
        APP_LOG(TS_OFF, VLEVEL_H, "CONFIRMED [%s]\r\n", (params->AckReceived != 0) ? "ACK" : "NACK");
      }
      else
      {
        APP_LOG(TS_OFF, VLEVEL_H, "UNCONFIRMED\r\n");
      }
    }
  }
  CurrentUplinkTxPower = params->TxPower;
  /* USER CODE END OnTxData_1 */
}

static void OnJoinRequest(LmHandlerJoinParams_t *joinParams)
{
  /* USER CODE BEGIN OnJoinRequest_1 */
  if (joinParams != NULL)
  {
    if (joinParams->Status == LORAMAC_HANDLER_SUCCESS)
    {
#if defined(USE_LED) && defined(USE_LEDTIM)
      UTIL_TIMER_Stop(&JoinLedTimer);
#endif
#if defined(USE_LED)
      SYS_LED_Off(SYS_LED_BLUE) ;
#endif

      JoinStatus = DEVICE_JOINED;

      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### = JOINED = ");
      if (joinParams->Mode == ACTIVATION_TYPE_ABP)
      {
        APP_LOG(TS_OFF, VLEVEL_M, "ABP ======================\r\n");
      }
      else
      {
        APP_LOG(TS_OFF, VLEVEL_M, "OTAA =====================\r\n");
      }
      UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);
    }
    else
    {
      APP_LOG(TS_OFF, VLEVEL_M, "\r\n###### = JOIN FAILED\r\n");

      JoinStatus = DEVICE_NOT_JOINED;
      JoinAttempt++;
      APP_PRINTF("Join attempt count: %d\r\n", JoinAttempt);
    }
  }
  /* USER CODE END OnJoinRequest_1 */
}

static void OnMacProcessNotify(void)
{
  /* USER CODE BEGIN OnMacProcessNotify_1 */

  /* USER CODE END OnMacProcessNotify_1 */
  UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LmHandlerProcess), CFG_SEQ_Prio_0);

  /* USER CODE BEGIN OnMacProcessNotify_2 */

  /* USER CODE END OnMacProcessNotify_2 */
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
