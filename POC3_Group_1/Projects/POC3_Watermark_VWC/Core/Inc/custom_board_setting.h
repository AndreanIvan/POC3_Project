/** 
  ****************************************************************************** 
  * @file           : custom_board_setting.h 
  * @brief          : Custom board setting for POC 3 Project 
  * @author         : Andrean I. 
  * @date           : December 2021 
  ****************************************************************************** 
  */ 
 
#ifndef __CUSTOM_BOARD_SETTING_H__ 
#define __CUSTOM_BOARD_SETTING_H__ 

/* Board Selection:
 * - POC3_WatermarkVWC_Board_001
 * - POC3_WatermarkVWC_Board_002
 * - POC3_WatermarkVWC_Board_003
 * - POC3_Test_Node_WatermarkVWC
 * - POC3_Improvement_Node_WatermarkVWC
 */
#define POC3_Improvement_Node_WatermarkVWC

#if defined(POC3_WatermarkVWC_Board_001)
#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_NOT_SUPPORTED
#elif defined(POC3_WatermarkVWC_Board_002)
#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_NOT_SUPPORTED
#elif defined(POC3_WatermarkVWC_Board_003)
#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_NOT_SUPPORTED
#elif defined(POC3_Improvement_Node_WatermarkVWC)
#define CUSTOM_IS_TCXO_SUPPORTED                RADIO_CONF_TCXO_SUPPORTED
#endif

#define CUSTOM_APP_TX_UPLINK_DUTYCYCLE          1800000

/* Firmware Version */
#define USE_CUSTOM_VERSION
#define FW_VER_MAJ_CUSTOM                       0x01
#define FW_VER_MIN_CUSTOM                       0x00
#define FW_VER_BF_CUSTOM                        0x03

#endif /* __CUSTOM_IDENTITY_H__ */