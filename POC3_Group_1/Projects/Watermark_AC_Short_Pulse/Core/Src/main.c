/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"

#include "dwt_delay.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum WM_Pin_t
{
	MUX_A,
	MUX_B,
	MUX_ON,
	PWM_1,
	PWM_2
} WM_Pin_e;

typedef enum WM_Channel_t
{
	WM_CHANNEL_1 = 0,
	WM_CHANNEL_2 = 1,
	WM_CHANNEL_3 = 2,
	WM_CHANNEL_4 = 3,
} WM_Channel_e;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define	WM_ADC_CHANNEL	ADC_CHANNEL_10
#define	HIGH			GPIO_PIN_SET
#define LOW				GPIO_PIN_RESET

#define	WM_READ_COUNT	50
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

#if 0
uint32_t ARead_A1 = 0, ARead_A2 = 0;
const uint16_t Ref_Resistance = 4920;
const uint16_t SupplyV = 3300;
double Calib_Resistance = 1.0;
double VM1, VM2;
double WM1_ResistanceA, WM1_ResistanceB, WM1_Resistance;
#endif

uint8_t state = 1;
uint32_t adcValue;
uint32_t mvValue;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
void PRINTF(const char *format, ...);
uint32_t ADC_ReadChannels(uint32_t channel);
uint16_t ADC_ReadWithoutOutliers(uint32_t ADC_Channel);

void WM_InitADC(uint32_t ADC_channel);
void WM_Select(uint8_t channel);
double WM_ReadSensor(uint8_t channel);

void writePin(uint8_t Pin, uint8_t PinState)
{
	switch (Pin)
	{
		case MUX_A	: HAL_GPIO_WritePin(MUX_A_GPIO_Port, MUX_A_Pin, PinState); break;
		case MUX_B	: HAL_GPIO_WritePin(MUX_B_GPIO_Port, MUX_B_Pin, PinState); break;
		case MUX_ON	: HAL_GPIO_WritePin(MUX_ON_GPIO_Port, MUX_ON_Pin, PinState); break;
		case PWM_1	: HAL_GPIO_WritePin(PWM_1_GPIO_Port, PWM_1_Pin, PinState); break;
		case PWM_2	: HAL_GPIO_WritePin(PWM_2_GPIO_Port, PWM_2_Pin, PinState); break;
		default		: break;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
	case GPIO_PIN_13:
	{
		if (state == 1)
		{
			PRINTF("Channel 2 selected.\r\n");
			WM_Select(WM_CHANNEL_2);

			state = 2;
		} else if (state == 2)
		{
			PRINTF("Channel 3 selected.\r\n");
			WM_Select(WM_CHANNEL_3);


			state = 3;
		} else if (state == 3)
		{
			PRINTF("Channel 4 selected.\r\n");
			WM_Select(WM_CHANNEL_4);

			state = 4;
		} else if (state == 4)
		{
			PRINTF("Channel 1 selected.\r\n");
			WM_Select(WM_CHANNEL_1);

			state = 1;
		}
	}
		break;
	default:
		break;
	}
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	DWT_Init();

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  PRINTF("===== Watermark Pseudo AC Short-Pulse =====\r\n");

  WM_InitADC(WM_ADC_CHANNEL);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  PRINTF("======== Channel 1 ========\r\n");
	  WM_ReadSensor(WM_CHANNEL_1);
	  PRINTF("======== Channel 2 ========\r\n");
	  WM_ReadSensor(WM_CHANNEL_2);
	  PRINTF("======== Channel 3 ========\r\n");
	  WM_ReadSensor(WM_CHANNEL_3);
	  PRINTF("======== Channel 4 ========\r\n");
	  WM_ReadSensor(WM_CHANNEL_4);

	  HAL_Delay(2500);

//	  writePin(PWM_1, HIGH);
//	  writePin(PWM_2, LOW);
//	  PRINTF("PWM_1 HIGH, PWM_2 LOW ----> ");
//	  HAL_Delay(2500);
//	  PRINTF("ADCValue: %4d | Voltage Value: %4d mV \r\n", ADC_ReadChannels(WM_ADC_CHANNEL), ADC_ReadChannels(WM_ADC_CHANNEL) * 3600 / 4095);
//	  HAL_Delay(2500);
//
//	  writePin(PWM_1, LOW);
//	  writePin(PWM_2, HIGH);
//	  PRINTF("PWM_1 LOW, PWM_2 HIGH ----> ");
//	  HAL_Delay(2500);
//	  PRINTF("ADCValue: %4d | Voltage Value: %4d mV \r\n", ADC_ReadChannels(WM_ADC_CHANNEL), ADC_ReadChannels(WM_ADC_CHANNEL) * 3600 / 4095);
//	  HAL_Delay(2500);
//
//	  writePin(PWM_1, LOW);
//	  writePin(PWM_2, LOW);
//	  PRINTF("PWM_1 LOW, PWM_2 LOW -----> ");
//	  HAL_Delay(2500);
//	  PRINTF("ADCValue: %4d | Voltage Value: %4d mV \r\n", ADC_ReadChannels(WM_ADC_CHANNEL), ADC_ReadChannels(WM_ADC_CHANNEL) * 3600 / 4095);
//	  HAL_Delay(2500);
//
	  PRINTF("=====================================================================================================================\r\n");

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the SYSCLKSource, HCLK, PCLK1 and PCLK2 clocks dividers
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK3|RCC_CLOCKTYPE_HCLK
                              |RCC_CLOCKTYPE_SYSCLK|RCC_CLOCKTYPE_PCLK1
                              |RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.AHBCLK3Divider = RCC_SYSCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.NbrOfConversion = 1;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  hadc.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_1CYCLE_5;
  hadc.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_1CYCLE_5;
  hadc.Init.OversamplingMode = DISABLE;
  hadc.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart2, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart2, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MUX_A_Pin|MUX_B_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, MUX_ON_Pin|PWM_1_Pin|PWM_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : MUX_A_Pin MUX_B_Pin */
  GPIO_InitStruct.Pin = MUX_A_Pin|MUX_B_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MUX_ON_Pin PWM_1_Pin PWM_2_Pin */
  GPIO_InitStruct.Pin = MUX_ON_Pin|PWM_1_Pin|PWM_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : BUT1_Pin BUT2_Pin */
  GPIO_InitStruct.Pin = BUT1_Pin|BUT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : BUT3_Pin */
  GPIO_InitStruct.Pin = BUT3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BUT3_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void PRINTF(const char *format, ...)
{
	static char ll_msg_buf_[500];
    va_list args;
    size_t len;

    /* Format the string */
    __builtin_va_start(args, format);
    len = vsnprintf(ll_msg_buf_, 500, &format[0], args);
    __builtin_va_end(args);
    HAL_UART_Transmit(&huart2, (uint8_t *)&ll_msg_buf_, len, 0xFFFF);
}

uint32_t ADC_ReadChannels(uint32_t channel)
{
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;

}

uint16_t Mean(uint16_t dataset[])
{
	uint16_t i;
	uint16_t dataset_len;

	/* Calculate the length of the data set */
	dataset_len = WM_READ_COUNT;

    uint32_t sample_sum;
    for (i = 0; i < dataset_len; i++)
    	sample_sum += dataset[i];

    return (uint16_t)(sample_sum/dataset_len);

}

uint32_t Mean_WithoutOutlier(uint16_t dataset[])
{
	uint16_t i, j;

	uint16_t dataset_len;

	/* Calculate the length of the data set */
	dataset_len = WM_READ_COUNT;

	/* Sort the data set */
    for (i = 0; i < dataset_len-1; i++)
        for (j = 0; j < dataset_len-i-1; j++)
           if (dataset[j] > dataset[j+1])
           {
        	   dataset[j]	= dataset[j] + dataset[j+1];
        	   dataset[j+1]	= dataset[j] - dataset[j+1];
        	   dataset[j]   = dataset[j] - dataset[j+1];
		   }

    /* Calculate the interquartile range */
    uint8_t	q1_idx = 0, q3_idx = 0;
    float Q1, Q3, IQR;

    q1_idx = (uint8_t)((dataset_len + 1) / 4);
    q3_idx = (uint8_t)((dataset_len + 1) * 3 / 4);

    if ((dataset_len+1)/4 % 2 != 0)
    {
    	Q1 = (dataset[q1_idx - 1] + dataset[q1_idx]) / 2;
    	Q3 = (dataset[q3_idx - 1] + dataset[q3_idx]) / 2;
    }
    else
    {
    	Q1 = dataset[q1_idx - 1];
    	Q3 = dataset[q3_idx - 1];
    }

    IQR = Q3-Q1;

    /* Set the outlier */
    float outlier_low, outlier_high;
    outlier_low  = Q1 - 3/2*IQR;
    outlier_high = Q3 + 3/2*IQR;

//    PRINTF("dataset_len: %d | q1_idx: %d | q3_idx: %d | Q1: %d | Q3: %d | IQR: %d | Outlier low: %d | Outlier high: %d\r\n", dataset_len, q1_idx, q3_idx, (uint16_t)Q1, (uint16_t)Q3, (uint16_t)IQR, (uint16_t)outlier_low, (uint16_t)outlier_high);

//    PRINTF("Outlier: ");
    /* If a sample is not outlier, then sum it */
	uint8_t sample_length = 0;
	uint32_t sample_sum = 0, sample_mean = 0;
    for (i = 0; i < dataset_len; i++)
    {
    	if ((dataset[i] >= outlier_low) && (dataset[i] <= outlier_high))
    	{
    		sample_sum += dataset[i];
    		sample_length++;
    	}
    	else
    	{
//    		PRINTF("%d ", dataset[i]);
    	}
	}
//    PRINTF("\r\n");

    sample_mean = sample_sum/sample_length;

//    PRINTF("Sample mean: %d | Sample sum: %d | Sample length: %d\r\n", sample_mean, sample_sum, sample_length);

    return sample_mean;
}

void WM_InitADC(uint32_t ADC_channel)
{
	HAL_ADCEx_Calibration_Start(&hadc);

	ADC_ChannelConfTypeDef sConfig = {0};

	sConfig.Channel = ADC_channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);
}

void WM_Select(uint8_t channel)
{
	writePin(MUX_A, channel & 0x01);
	writePin(MUX_B, (channel >> 1) & 0x01);
}

double WM_ReadSensor(uint8_t channel)
{

	uint16_t num_of_read = WM_READ_COUNT;

	uint16_t ADC_A1[num_of_read], ADC_A2[num_of_read];
	uint32_t ARead_A1 = 0, ARead_A2 = 0;
	const uint16_t Ref_Resistance = 7870;
	const uint16_t SupplyV = 3600;
	double Calib_Resistance = 1.0;
	double VM1, VM2;
	double WM1_ResistanceA, WM1_ResistanceB, WM1_Resistance;

	/*** Calibration resistance ***/

	uint16_t ADC_Ref;
	double Vref = 0.0;
	double WM1_Ref = 0.0;

	WM_Select(WM_CHANNEL_3);

	HAL_Delay(100);

	writePin(PWM_2, HIGH);
	HAL_Delay(100);

	/* Read ADC */
	HAL_ADC_Start(&hadc);
	HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
	ADC_Ref = HAL_ADC_GetValue(&hadc);

	writePin(PWM_2, LOW);

	Vref = ADC_Ref * SupplyV / (4095 * 1);
	WM1_Ref = Ref_Resistance * Vref / (SupplyV - Vref);

	Calib_Resistance = WM1_Ref / 10000.0;

	PRINTF("ADC_Ref: %d | Vref: %d | WM1_Ref: %d | Calib_constant: %d.%d\r\n", ADC_Ref, (uint16_t)Vref, (uint16_t)WM1_Ref, (uint16_t)(Calib_Resistance), (uint16_t)(Calib_Resistance*1000));

	/*** Select channel ***/
	WM_Select(channel);

	HAL_Delay(100);

	/*** Set the initial state for each pin ***/
	writePin(PWM_1, LOW);
	writePin(PWM_2, LOW);
	HAL_Delay(100);

	uint8_t i = 0;
	for (i = 0; i <= num_of_read - 1; i++)
	{
		/*** Read Watermark with PWM_1 -> PWM_2 current direction ***/

		/* Set PWM_1 pin to high and delay */
		writePin(PWM_1, HIGH);
		DWT_Delay(75);

		/* Read ADC */
		HAL_ADC_Start(&hadc);
		HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
		ADC_A1[i] = HAL_ADC_GetValue(&hadc);

		/* Set PWM_1 pin to low and delay */
		writePin(PWM_1, LOW);
		HAL_Delay(10);

		/* Reverse the polarity. Read Watermark with PWM_2 -> PWM_1 current direction */

		/* Set PWM_1 pin to high and delay */
		writePin(PWM_2, HIGH);
		DWT_Delay(75);

		/* Read ADC */
		HAL_ADC_Start(&hadc);
		HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
		ADC_A2[i] = HAL_ADC_GetValue(&hadc);

		/* Set PWM_1 pin to low and delay */
		writePin(PWM_2, LOW);
		HAL_Delay(10);

	}

//	for (i = 0; i <= num_of_read - 1; i++)
//		PRINTF("%d ", ADC_A1[i]);

//	PRINTF("\r\n");

//	for (i = 0; i <= num_of_read - 1; i++)
//		PRINTF("%d ", ADC_A2[i]);

//	PRINTF("\r\n");

	ARead_A1 = Mean_WithoutOutlier(ADC_A1);
	ARead_A2 = Mean_WithoutOutlier(ADC_A2);

	/* Calculate the voltage */
	VM1 = ARead_A1 * SupplyV / (4095 * 1);
	VM2 = ARead_A2 * SupplyV / (4095 * 1);

	/* Calculate the resistance */
	WM1_ResistanceA = (Ref_Resistance * (SupplyV - VM1) / VM1); //do the voltage divider math, using the Rx variable representing the known resistor
	WM1_ResistanceB = Ref_Resistance * VM2 / (SupplyV - VM2);  // reverse
	WM1_Resistance = ((WM1_ResistanceA + WM1_ResistanceB) / 2);  //average the two directions and apply the calibration factor

	switch ((uint16_t)WM1_Resistance)
	{
		case 0 ... 999    : WM1_Resistance /= 1.20; break;
		case 1000 ... 2999 : WM1_Resistance /= 1.09; break;
		case 3000 ... 4999 : WM1_Resistance /= 1.04; break;
		case 5000 ... 6999 : WM1_Resistance /= 1.018; break;
		case 7000 ... 8999 : WM1_Resistance /= 0.99; break;
		default 		   : WM1_Resistance /= 0.97; break;
	}

	PRINTF("ADC_1 : %4d | ADC_2 : %4d | Volt_1 : %4d mV | Volt_2 : %4d mV | WM1_ResistanceA : %4d ohm | WM1_ResistanceB : %4d ohm | WM1_Resistance : %4d ohm\r\n",
			ARead_A1, ARead_A2,
			(uint16_t)(VM1), (uint16_t)(VM2),
			(uint16_t)(WM1_ResistanceA), (uint16_t)(WM1_ResistanceB), (uint16_t)(WM1_Resistance)
			);

	return WM1_Resistance;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
