/**
  ******************************************************************************
  * @file           : custom_identity.h
  * @brief          : Identity definition for POC 3 Project
  * @author         : Andrean I.
  * @date           : December 2021
  ******************************************************************************
  */

#ifndef __CUSTOM_IDENTITY_H__
#define __CUSTOM_IDENTITY_H__

#define POC3_SolenoidValve_Board_001_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x01 }
#define POC3_SolenoidValve_Board_002_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x02 }
#define POC3_SolenoidValve_Board_003_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x01, 0x00, 0x03 }
#define POC3_Waterflow_Board_001_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x01 }
#define POC3_Waterflow_Board_002_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x02 }
#define POC3_Waterflow_Board_003_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x02, 0x00, 0x03 }
#define POC3_Pressure_Board_001_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x01 }
#define POC3_Pressure_Board_002_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x02 }
#define POC3_Pressure_Board_003_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x04, 0x00, 0x03 }
#define POC3_WatermarkVWC_Board_001_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x01 }
#define POC3_WatermarkVWC_Board_002_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x02 }
#define POC3_WatermarkVWC_Board_003_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x05, 0x00, 0x03 }
#define POC3_Test_Node_DEV_EUI                      { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77 }
#define POC3_Test_Node_SolenoidValve_DEV_EUI	      { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x01, 0x01 }
#define POC3_Test_Node_Waterflow_DEV_EUI	          { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x02, 0x01 }
#define POC3_Test_Node_Pressure_DEV_EUI	            { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x04, 0x01 }
#define POC3_Test_Node_Pressure_002_DEV_EUI         { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x04, 0x02 }
#define POC3_Test_Node_WatermarkVWC_DEV_EUI	        { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x05, 0x01 }
#define POC3_Test_Node_Watermark_Tensio_DEV_EUI	    { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x05, 0x02 }
#define POC3_Improvement_Node_SolenoidValve_DEV_EUI { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x01, 0x02 }
#define POC3_Improvement_Node_WatermarkVWC_DEV_EUI  { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x05, 0x03 }
#define POC3_FaultTest_Node_SolenoidValve_DEV_EUI   { 0xab, 0xcd, 0xef, 0x03, 0x00, 0x99, 0x01, 0x03 }

#define POC3_SolenoidValve_Board_001_APP_KEY         77,f2,68,91,00,63,d6,1d,00,03,c4,e0,4a,bd,b8,a3
#define POC3_SolenoidValve_Board_002_APP_KEY         1d,21,29,9d,2b,92,24,99,1a,8b,62,af,31,36,f7,c5
#define POC3_SolenoidValve_Board_003_APP_KEY         94,bb,58,00,42,64,e1,cf,99,db,79,80,b0,c8,b0,61
#define POC3_Waterflow_Board_001_APP_KEY             3d,57,86,fb,e8,39,5a,88,e2,e3,45,47,f2,10,40,12
#define POC3_Waterflow_Board_002_APP_KEY             46,18,bf,76,a0,62,97,12,a3,6c,3f,58,a8,68,5a,a1
#define POC3_Waterflow_Board_003_APP_KEY             a5,78,a0,96,f6,fd,e4,13,49,53,7f,7b,88,07,30,7d
#define POC3_Pressure_Board_001_APP_KEY              9d,75,61,a7,30,cd,af,84,2b,dc,d7,81,a1,7a,eb,52
#define POC3_Pressure_Board_002_APP_KEY              c2,7c,30,8e,1f,aa,9b,a3,47,4d,73,34,9b,93,f9,71
#define POC3_Pressure_Board_003_APP_KEY              66,62,1b,46,15,d9,7c,e3,15,a8,95,5a,11,ae,97,da
#define POC3_WatermarkVWC_Board_001_APP_KEY          0b,21,c0,25,50,1e,8f,7f,90,21,f1,a4,47,94,9c,4a
#define POC3_WatermarkVWC_Board_002_APP_KEY          17,fa,c5,85,9a,83,37,80,f8,d8,96,81,4c,51,e9,a0
#define POC3_WatermarkVWC_Board_003_APP_KEY          b6,57,d3,45,79,d3,16,d0,4e,1a,9d,17,50,b0,da,ef
#define POC3_Test_Node_APP_KEY                       00,11,22,33,44,55,66,77,88,99,aa,bb,cc,dd,ee,ff
#define POC3_Test_Node_SolenoidValve_APP_KEY         a7,0e,62,54,6a,96,d1,ef,1c,c3,f5,58,25,27,80,86
#define POC3_Test_Node_Waterflow_APP_KEY             36,cd,07,20,7d,9d,b9,a1,ef,cb,59,63,a1,eb,21,e5
#define POC3_Test_Node_Pressure_APP_KEY              1e,bc,22,a0,68,4f,44,b5,a7,04,77,72,95,a2,58,7c
#define POC3_Test_Node_Pressure_002_APP_KEY          78,c9,7a,07,af,42,f2,80,f9,cc,69,a2,ec,5e,52,04
#define POC3_Test_Node_WatermarkVWC_APP_KEY          f7,9a,66,01,8d,43,1a,f7,de,d3,d3,04,69,60,92,94
#define POC3_Test_Node_Watermark_Tensio_APP_KEY      83,dc,63,15,a7,5a,50,92,a6,40,71,0e,86,3d,ef,c5
#define POC3_Improvement_Node_SolenoidValve_APP_KEY  2a,5f,5f,95,37,f5,19,1d,96,84,01,c9,36,93,d7,b5
#define POC3_Improvement_Node_WatermarkVWC_APP_KEY   6A,15,47,D2,34,D2,3C,9C,69,83,C4,6D,56,60,33,DF
#define POC3_FaultTest_Node_SolenoidValve_APP_KEY    2F,E9,26,3F,8B,33,5D,04,9C,62,C9,D5,61,B5,9E,C9

#define POC3_SolenoidValve_Board_001_String          "POC 3 Node - Solenoid Valve 001"
#define POC3_SolenoidValve_Board_002_String          "POC 3 Node - Solenoid Valve 002"
#define POC3_SolenoidValve_Board_003_String          "POC 3 Node - Solenoid Valve 003"
#define POC3_Waterflow_Board_001_String              "POC 3 Node - Waterflow Meter 001"
#define POC3_Waterflow_Board_002_String              "POC 3 Node - Waterflow Meter 002"
#define POC3_Waterflow_Board_003_String              "POC 3 Node - Waterflow Meter 003"
#define POC3_Pressure_Board_001_String               "POC 3 Node - Water Pressure Sensor 001"
#define POC3_Pressure_Board_002_String               "POC 3 Node - Water Pressure Sensor 002"
#define POC3_Pressure_Board_003_String               "POC 3 Node - Water Pressure Sensor 003"
#define POC3_WatermarkVWC_Board_001_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 001"
#define POC3_WatermarkVWC_Board_002_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 002"
#define POC3_WatermarkVWC_Board_003_String           "POC 3 Node - Soil Sensor (Watermark + VWC) 003"
#define POC3_Test_Node_String                        "POC 3 Node - Test Node"
#define POC3_Test_Node_SolenoidValve_String          "POC 3 Node - Solenoid Valve Test Node"
#define POC3_Test_Node_Waterflow_String              "POC 3 Node - Waterflow Meter Test Node"
#define POC3_Test_Node_Pressure_String               "POC 3 Node - Water Pressure Sensor Test Node"
#define POC3_Test_Node_Pressure_002_String           "POC 3 Node - Water Pressure Sensor Test Node 002"
#define POC3_Test_Node_WatermarkVWC_String           "POC 3 Node - Soil Sensor (Watermark + VWC) Test Node"
#define POC3_Test_Node_Watermark_Tensio_String       "POC 3 Node - Watermark + Tensiometer Test Node"
#define POC3_Improvement_Node_SolenoidValve_String   "POC 3 Node - Solenoid Valve Improvement Node"
#define POC3_Improvement_Node_WatermarkVWC_String    "POC 3 Node - Soil Sensor (Watermark) Improvement Node"
#define POC3_FaultTest_Node_SolenoidValve_String     "POC 3 Node - Solenoid Valve Fault Test Node"

#endif /* __CUSTOM_IDENTITY_H__ */