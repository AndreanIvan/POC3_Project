/**
  ******************************************************************************
  * @file           : LIT_Pressure.c
  * @brief          : Source file for 4-20 mA Pressure Sensor Library
  * @author         : Andrean I.
  * @date           : September 2021
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include "LIT_Pressure.h"

#include "sys_app.h"

/* External variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef hadc;

/* Private constants ----------------------------------------------------------*/

/**
 * Defines the log level
 */
#define LOG_LEVEL							2

/**
 * Default number of sample read from ADC
 */
#define NUMBER_OF_SAMPLE					15

#define MAX_VOLT_VALUE                      ReadBatteryLevel

#define TOLERANCE_PERCENTAGE				10

#define MIN_VOLT_MEASURED                   400  * ( 1 - TOLERANCE_PERCENTAGE/100 )
#define MAX_VOLT_MEASURED                   2000 * ( 1 + TOLERANCE_PERCENTAGE/100 )

#define MIN_CURRENT_MEASURED                4000
#define MAX_CURRENT_MEASURED                20000

#define MAX_ADC_VALUE                       4095

#define MIN_ADC_MEASURED					(uint16_t)(MAX_ADC_VALUE * MIN_VOLT_MEASURED / MAX_VOLT_VALUE * 0.9)	// 4095 * 400 / BattLevel
#define MAX_ADC_MEASURED					(uint16_t)(MAX_ADC_VALUE * MAX_VOLT_MEASURED / MAX_VOLT_VALUE * 1.1)	// 4095 * 2000 / BattLevel

#define MIN_PRESSURE_MEASURED               0
#define MAX_PRESSURE_MEASURED               25000

/* Private function prototypes -----------------------------------------------*/
static void Write12VLoadSwitch(uint8_t PinState);
static void Restart12VLoadSwitch(uint16_t delayMs);
static uint32_t ADC_ReadChannels(uint32_t channel);
static uint16_t meanOf(uint16_t arr[]);

/* Private variables ---------------------------------------------------------*/
static uint16_t ReadBatteryLevel;
static uint16_t LastADCValue, LastADCValue1, LastADCValue2;
static uint16_t LastADCVoltage, LastADCVoltage1, LastADCVoltage2;

/* Exported functions ---------------------------------------------------------*/
void Pressure_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PRESSURE_EN_12V_GPIO_Port, PRESSURE_EN_12V_Pin | GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PRESSURE_EN_12V_Pin */
  GPIO_InitStruct.Pin = PRESSURE_EN_12V_Pin | GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PRESSURE_EN_12V_GPIO_Port, &GPIO_InitStruct);
}

uint8_t	Pressure_GetADC(uint16_t *ADCValue)
{
	uint16_t ADC_Sampled[NUMBER_OF_SAMPLE];
    uint16_t ADC_Measured;
    uint16_t Batt_Measured[NUMBER_OF_SAMPLE];

    /* Turn on load switch */
//    Write12VLoadSwitch(GPIO_PIN_SET);

    /* Waiting until step up reach stable state */
//    HAL_Delay(650);

    /* Read ADC */
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	ADC_Sampled[i] = ADC_ReadChannels(PRESSURE_IN1_CHANNEL);
    	HAL_Delay(10);
    	Batt_Measured[i] = SYS_GetBatteryLevel();
    	HAL_Delay(10);
    }

    /* Turn off load switch */
//    Write12VLoadSwitch(GPIO_PIN_RESET);

//    APP_PRINTF("ADC_Sampled:");
//    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
//    {
//    	APP_PRINTF(" %d", ADC_Sampled[i]);
//    }
//    APP_PRINTF("\r\n");

//    APP_PRINTF("Batt_Measured:");
//    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
//    {
//    	APP_PRINTF(" %d", Batt_Measured[i]);
//    }
//    APP_PRINTF("\r\n");

    /* Calculate ADC Value*/
    ADC_Measured = meanOf(ADC_Sampled);
    ReadBatteryLevel = meanOf(Batt_Measured);

    *ADCValue = ADC_Measured;
    LastADCValue = ADC_Measured;

//    APP_PRINTF("ADC Measured: %d | Min: %d | Max: %d\r\n", ADC_Measured, MIN_ADC_MEASURED, MAX_ADC_MEASURED);
    APP_PRINTF("ADC Measured: %4d | ", ADC_Measured);

    /* Return value */
    if (ADC_Measured < 10)
        return SENSOR_NOT_DETECTED;
    else if ((ADC_Measured < MIN_ADC_MEASURED) || (ADC_Measured > MAX_ADC_MEASURED))
    	return SENSOR_ERROR;
    else
        return SENSOR_OK;
}

uint8_t	Pressure_GetADC_2Channel(uint16_t *ADCValue1, uint16_t *ADCValue2)
{
	uint16_t ADC_Sampled1[NUMBER_OF_SAMPLE], ADC_Sampled2[NUMBER_OF_SAMPLE];
    uint16_t ADC_Measured1, ADC_Measured2;
    uint16_t Batt_Measured[NUMBER_OF_SAMPLE];

    /* Turn on load switch */
    Write12VLoadSwitch(GPIO_PIN_SET);

    /* Waiting until step up reach stable state */
    HAL_Delay(800);

    /* Read ADC on channel 1 and channel 2 */
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	ADC_Sampled1[i] = ADC_ReadChannels(PRESSURE_IN1_CHANNEL);
    	HAL_Delay(10);
    	ADC_Sampled2[i] = ADC_ReadChannels(PRESSURE_IN2_CHANNEL);
    	HAL_Delay(10);
    	Batt_Measured[i] = SYS_GetBatteryLevel();
    	HAL_Delay(10);
    }

    /* Turn off load switch */
    Write12VLoadSwitch(GPIO_PIN_RESET);

    APP_PRINTF("ADC_Sampled1:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	APP_PRINTF(" %d", ADC_Sampled1[i]);
    }
    APP_PRINTF("\r\n");

    APP_PRINTF("ADC_Sampled2:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	APP_PRINTF(" %d", ADC_Sampled2[i]);
    }
    APP_PRINTF("\r\n");

    APP_PRINTF("Batt_Measured:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	APP_PRINTF(" %d", Batt_Measured[i]);
    }
    APP_PRINTF("\r\n");

    /* Calculate ADC Value*/
    ADC_Measured1 = meanOf(ADC_Sampled1);
    ADC_Measured2 = meanOf(ADC_Sampled2);

    ReadBatteryLevel = meanOf(Batt_Measured);

    /* Store the value in pointer argument */
    *ADCValue1 = ADC_Measured1;
    *ADCValue2 = ADC_Measured2;

    /* Return value */
    if ((ADC_Measured1 < 10) || (ADC_Measured2 < 10))
    {
    	if (ADC_Measured1 < 10)	return CHANNEL_1_SENSOR_NOT_DETECTED;
    	else if (ADC_Measured2 < 10) return CHANNEL_2_SENSOR_NOT_DETECTED;
    	else return ALL_CHANNEL_SENSOR_NOT_DETECTED;
    }
    else if ((ADC_Measured1 < MIN_ADC_MEASURED) || (ADC_Measured1 > MAX_ADC_MEASURED) || (ADC_Measured2 < MIN_ADC_MEASURED) || (ADC_Measured2 > MAX_ADC_MEASURED))
    {
    	if ((ADC_Measured1 < MIN_ADC_MEASURED) || (ADC_Measured1 > MAX_ADC_MEASURED)) return CHANNEL_1_SENSOR_ERROR;
    	else if ((ADC_Measured2 < MIN_ADC_MEASURED) || (ADC_Measured2 > MAX_ADC_MEASURED)) return CHANNEL_2_SENSOR_ERROR;
    	else return ALL_CHANNEL_SENSOR_ERROR;
    }
    else
        return SENSOR_OK;
}

uint8_t	Pressure_GetVoltage(uint16_t *VoltageValue)
{
	/*** Initialization ***/

	/* Initialize array of voltage reading sampling */
    uint16_t Batt_Sampled[NUMBER_OF_SAMPLE];
	uint16_t ADC_Sampled[NUMBER_OF_SAMPLE];
    uint16_t Voltage_Sampled[NUMBER_OF_SAMPLE];

    /* Initialize variable used */
    uint16_t Voltage_Measured;

	/*** Read ADC Voltage by sampling and calculate each of them ***/

    /* Turn on load switch */
    Write12VLoadSwitch(GPIO_PIN_SET);

    /* Waiting until step up reach stable state */
    HAL_Delay(650);

    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	ADC_Sampled[i] = ADC_ReadChannels(PRESSURE_IN1_CHANNEL);
    	HAL_Delay(10);
    	Batt_Sampled[i] = SYS_GetBatteryLevel();
    	HAL_Delay(10);
    }

    /* Turn off load switch */
    Write12VLoadSwitch(GPIO_PIN_RESET);

    /* Calculate ADC Voltage based on voltage reference */
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	Voltage_Sampled[i] = ADC_Sampled[i] * Batt_Sampled[i] / MAX_ADC_VALUE;
    }

#if (LOG_LEVEL >= 2)

    APP_PRINTF("ADC_Sampled:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++) APP_PRINTF(" %d", ADC_Sampled[i]);
    APP_PRINTF("\r\n");

    APP_PRINTF("Batt_Sampled:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)	APP_PRINTF(" %d", Batt_Sampled[i]);
    APP_PRINTF("\r\n");

#endif

    /* Get mean from filtered samples */
    Voltage_Measured 	= meanOf(Voltage_Sampled);

	/*** Save and output value for debuggin  ***/

    /* Save value in global variable for debugging purposes */
    ReadBatteryLevel 	= meanOf(Batt_Sampled);
    LastADCValue 		= meanOf(ADC_Sampled);
    LastADCVoltage 		= Voltage_Measured;

    /* Print log for debugging purposes */
    APP_PRINTF("Battery Measured : %4d mV | ", ReadBatteryLevel);
    APP_PRINTF("ADC Value : %4d | ", LastADCValue);
    APP_PRINTF("Voltage Measured : %4d mV | ", Voltage_Measured);

	/*** Pass value to argument and return the status ***/

    /* Pass value to argument */
    *VoltageValue = Voltage_Measured;

    /* Return value */
    if (Voltage_Measured < 10)
        return SENSOR_NOT_DETECTED;
    else if ((Voltage_Measured < MIN_VOLT_MEASURED) || (Voltage_Measured > MAX_VOLT_MEASURED))
    	return SENSOR_ERROR;
    else
        return SENSOR_OK;

}

uint8_t	Pressure_GetVoltage_2Channel(uint16_t *VoltageValue1, uint16_t *VoltageValue2)
{
	/*** Initialization ***/

	/* Initialize array of voltage reading sampling */
    uint16_t Batt_Sampled[NUMBER_OF_SAMPLE];
	uint16_t ADC1_Sampled[NUMBER_OF_SAMPLE], ADC2_Sampled[NUMBER_OF_SAMPLE];
    uint16_t Voltage1_Sampled[NUMBER_OF_SAMPLE], Voltage2_Sampled[NUMBER_OF_SAMPLE];

    /* Initialize variable used */
    uint16_t Voltage1_Measured;
    uint16_t Voltage2_Measured;

	/*** Read ADC Voltage by sampling and calculate each of them ***/

    /* Turn on load switch */
    Write12VLoadSwitch(GPIO_PIN_SET);

    /* Waiting until step up reach stable state */
    HAL_Delay(750);

    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	ADC1_Sampled[i] = ADC_ReadChannels(PRESSURE_IN1_CHANNEL);
    	HAL_Delay(10);
    	ADC2_Sampled[i] = ADC_ReadChannels(PRESSURE_IN2_CHANNEL);
    	HAL_Delay(10);
    	Batt_Sampled[i] = SYS_GetBatteryLevel();
    	HAL_Delay(10);

    	if (i == 0)
    	{
    		if ( ( ADC1_Sampled[i] * Batt_Sampled[i] / MAX_ADC_VALUE ) < MIN_VOLT_MEASURED )
    		{
    			APP_PRINTF("Sensor Error. Restarting 12V Load Switch...\r\n");
    			Restart12VLoadSwitch(1000);
    		}
    	}
    }

    /* Turn off load switch */
    Write12VLoadSwitch(GPIO_PIN_RESET);

    /* Calculate ADC Voltage based on voltage reference */
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)
    {
    	Voltage1_Sampled[i] = ADC1_Sampled[i] * Batt_Sampled[i] / MAX_ADC_VALUE;
    	Voltage2_Sampled[i] = ADC2_Sampled[i] * Batt_Sampled[i] / MAX_ADC_VALUE;
    }

#if (LOG_LEVEL >= 2)

    APP_PRINTF("ADC1_Sampled:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++) APP_PRINTF(" %d", ADC1_Sampled[i]);
    APP_PRINTF("\r\n");

    APP_PRINTF("ADC2_Sampled:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++) APP_PRINTF(" %d", ADC2_Sampled[i]);
    APP_PRINTF("\r\n");

    APP_PRINTF("Batt_Sampled:");
    for (int i = 0; i <= NUMBER_OF_SAMPLE; i++)	APP_PRINTF(" %d", Batt_Sampled[i]);
    APP_PRINTF("\r\n");

#endif

    /* Get mean from filtered samples */
    Voltage1_Measured 	= meanOf(Voltage1_Sampled);
    Voltage2_Measured 	= meanOf(Voltage2_Sampled);

	/*** Save and output value for debugging  ***/

    /* Save value in global variable for debugging purposes */
    ReadBatteryLevel 	= meanOf(Batt_Sampled);
    LastADCValue1 		= meanOf(ADC1_Sampled);
    LastADCValue2 		= meanOf(ADC2_Sampled);
    LastADCVoltage1 	= Voltage1_Measured;
    LastADCVoltage2 	= Voltage2_Measured;

    /* Print log for debugging purposes */
    APP_PRINTF("Battery Measured: %4d mV | ", ReadBatteryLevel);
    APP_PRINTF("ADC1 Value : %4d | ADC2 Value: %4d |", LastADCValue1, LastADCValue2);
    APP_PRINTF("Voltage 1 Measured: %4d mV | Voltage 2 Measured: %4d mV | ", Voltage1_Measured, Voltage2_Measured);

	/*** Pass value to argument and return the status ***/

    /* Pass value to argument */
    *VoltageValue1 = Voltage1_Measured;
    *VoltageValue2 = Voltage2_Measured;

    /* Return value */
    if ((Voltage1_Measured < 10) || (Voltage2_Measured < 10))
    {
    	if (Voltage1_Measured < 10)	return CHANNEL_1_SENSOR_NOT_DETECTED;
    	else if (Voltage2_Measured < 10) return CHANNEL_2_SENSOR_NOT_DETECTED;
    	else return ALL_CHANNEL_SENSOR_NOT_DETECTED;
    }
    else if ((Voltage1_Measured < MIN_VOLT_MEASURED) || (Voltage1_Measured > MAX_VOLT_MEASURED) || (Voltage2_Measured < MIN_VOLT_MEASURED) || (Voltage2_Measured > MAX_VOLT_MEASURED))
    {
    	if ((Voltage1_Measured < MIN_VOLT_MEASURED) || (Voltage1_Measured > MAX_VOLT_MEASURED)) return CHANNEL_1_SENSOR_ERROR;
    	else if ((Voltage2_Measured < MIN_VOLT_MEASURED) || (Voltage2_Measured > MAX_VOLT_MEASURED)) return CHANNEL_2_SENSOR_ERROR;
    	else return ALL_CHANNEL_SENSOR_ERROR;
    }
    else
        return SENSOR_OK;

}

uint8_t	Pressure_GetCurrent(uint16_t *CurrentValue)
{
	uint16_t Voltage_Measured;
    uint32_t Current_Measured;
    uint8_t returnStatus;

    returnStatus = Pressure_GetVoltage(&Voltage_Measured);

    Current_Measured = Voltage_Measured * 1000 / 100;

    APP_PRINTF("Current Measured : %4d.%02d mA | ", (uint16_t)(Current_Measured/1000), (uint16_t)(Current_Measured % 1000 / 10));

    *CurrentValue = Current_Measured;

    return returnStatus;
}

uint8_t	Pressure_GetCurrent_2Channel(uint16_t *CurrentValue1, uint16_t *CurrentValue2)
{
	uint16_t Voltage_Measured1, Voltage_Measured2;
    uint32_t Current_Measured1, Current_Measured2;
    uint8_t returnStatus;

    returnStatus = Pressure_GetVoltage_2Channel(&Voltage_Measured1, &Voltage_Measured2);

    Current_Measured1 = Voltage_Measured1 * 1000 / 100;
    Current_Measured2 = Voltage_Measured2 * 1000 / 100;

    APP_PRINTF("Current 1 Measured : %4d.%02d mA | Current 2 Measured : %4d.%02d mA\r\n",
    		(uint16_t)(Current_Measured1/1000), (uint16_t)(Current_Measured1 % 100 / 10),
			(uint16_t)(Current_Measured2/1000), (uint16_t)(Current_Measured2 % 100 / 10));

    *CurrentValue1 = Current_Measured1;
    *CurrentValue2 = Current_Measured2;

    return returnStatus;
}

uint8_t Pressure_GetPressure(uint16_t *PressureValue)
{
	uint16_t Current_Measured;
    int32_t Pressure_Measured;
    uint8_t returnStatus;

//    APP_PRINTF("\nMeasuring pressure on single channel...\r\n");

    returnStatus = Pressure_GetCurrent(&Current_Measured);

    Pressure_Measured = (Current_Measured - MIN_CURRENT_MEASURED) * (MAX_PRESSURE_MEASURED - MIN_PRESSURE_MEASURED) / (MAX_CURRENT_MEASURED - MIN_CURRENT_MEASURED) + MIN_PRESSURE_MEASURED;

    if (Pressure_Measured < 0 ) Pressure_Measured = 0;
    if (Pressure_Measured > 25000) Pressure_Measured = 25000;

//    APP_PRINTF("Pressure Measured: %5d mbar | ", Pressure_Measured);

    uint32_t Moisture_Measured;

    Moisture_Measured = ( Current_Measured - 4000 ) * 100 / 16;
    APP_PRINTF("cbar Measured: %3d.%03d cbar\r\n", (uint16_t)(Moisture_Measured/1000), (uint16_t)(Moisture_Measured%1000) );

    *PressureValue = Pressure_Measured;

    return returnStatus;
}

uint8_t Pressure_GetPressure_2Channel(uint16_t *PressureValue1, uint16_t *PressureValue2)
{
    uint16_t Current_Measured1, Current_Measured2;
    int32_t Pressure_Measured1, Pressure_Measured2;
    uint8_t returnStatus;

    APP_PRINTF("\nMeasuring pressure on dual channel...\r\n");

    returnStatus = Pressure_GetCurrent_2Channel(&Current_Measured1, &Current_Measured2);

    Pressure_Measured1 = (Current_Measured1 - MIN_CURRENT_MEASURED) * (MAX_PRESSURE_MEASURED - MIN_PRESSURE_MEASURED) / (MAX_CURRENT_MEASURED - MIN_CURRENT_MEASURED) + MIN_PRESSURE_MEASURED;
    Pressure_Measured2 = (Current_Measured2 - MIN_CURRENT_MEASURED) * (MAX_PRESSURE_MEASURED - MIN_PRESSURE_MEASURED) / (MAX_CURRENT_MEASURED - MIN_CURRENT_MEASURED) + MIN_PRESSURE_MEASURED;

    APP_PRINTF("Pressure 1 Measured: %d mbar | Pressure 2 Measured: %d mbar\r\n", Pressure_Measured1, Pressure_Measured2);

    if (Pressure_Measured1 < 0 ) Pressure_Measured1 = 0;
    if (Pressure_Measured2 < 0 ) Pressure_Measured2 = 0;
    if (Pressure_Measured1 > 25000) Pressure_Measured1 = 25000;
    if (Pressure_Measured2 > 25000) Pressure_Measured2 = 25000;

    *PressureValue1 = Pressure_Measured1;
    *PressureValue2 = Pressure_Measured2;

    return returnStatus;
}

void Pressure_GetDebugInfo(uint16_t *BattVoltage, uint16_t *ADCValue, uint16_t *ADCVoltage)
{
	*BattVoltage = ReadBatteryLevel;
	*ADCValue = LastADCValue;
	*ADCVoltage = LastADCVoltage;
}

void Pressure_GetDebugInfo_2Channel(uint16_t *BattVoltage, uint16_t *ADCValue1, uint16_t *ADCValue2, uint16_t *ADCVoltage1, uint16_t *ADCVoltage2)
{
	*BattVoltage = ReadBatteryLevel;
	*ADCValue1 = LastADCValue1;
	*ADCValue2 = LastADCValue2;
	*ADCVoltage1 = LastADCVoltage1;
	*ADCVoltage2 = LastADCVoltage2;
}


/* Private functions ---------------------------------------------------------*/
void Write12VLoadSwitch(uint8_t PinState)
{
    HAL_GPIO_WritePin(PRESSURE_EN_12V_GPIO_Port, PRESSURE_EN_12V_Pin, PinState);
//    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_13, PinState);
}

void Restart12VLoadSwitch(uint16_t delayMs)
{
	Write12VLoadSwitch(GPIO_PIN_RESET);
	HAL_Delay(delayMs);
	Write12VLoadSwitch(GPIO_PIN_SET);
	HAL_Delay(750);
}

uint32_t ADC_ReadChannels(uint32_t channel)
{
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;

}

uint16_t meanOf(uint16_t arr[])
{
	/* Calculate the array length */
    size_t arr_len = NUMBER_OF_SAMPLE;
    
//    uint8_t i, j;

//    APP_PRINTF("Unsorted:");
//    for (int i = 0; i < arr_len; i++)
//    	APP_PRINTF(" %d", arr[i]);
//
//    APP_PRINTF("\r\n");

    /* Sort the array using bubble sort algorithm */
    for (int i = 0; i < arr_len-1; i++)
        for (int j = 0; j < arr_len-i-1; j++)
           if (arr[j] > arr[j+1])
           {
           		arr[j] 	 = arr[j] + arr[j+1];
           		arr[j+1] = arr[j] - arr[j+1];
           		arr[j]   = arr[j] - arr[j+1];
		   }
    
//    APP_PRINTF("Sorted:");
//    for (int i = 0; i < arr_len; i++)
//    	APP_PRINTF(" %d", arr[i]);
//
//    APP_PRINTF("\r\n");

    /* Calculate the interquartile range */
    uint8_t	q1_idx = 0, q3_idx = 0;
    float Q1, Q3, IQR;

    q1_idx = (uint8_t)((arr_len + 1) / 4);
    q3_idx = (uint8_t)((arr_len + 1) * 3 / 4);

    if ((arr_len+1)/4 % 2 != 0)
    {
    	Q1 = (arr[q1_idx - 1] + arr[q1_idx]) / 2;
    	Q3 = (arr[q3_idx - 1] + arr[q3_idx]) / 2;
    }
    else
    {
    	Q1 = arr[q1_idx - 1];
    	Q3 = arr[q3_idx - 1];
    }

    IQR = Q3-Q1;

    /* Set the outlier */
    float outlier_low, outlier_high;
    outlier_low  = Q1 - 3/2*IQR;
    outlier_high = Q3 + 3/2*IQR;

//    APP_PRINTF("ArrLen: %d | q1_idx: %d | q3_idx: %d | Q1: %d | Q3: %d | IQR: %d | out_low: %d | out_high: %d | Outlier(s):", arr_len, q1_idx, q3_idx, (uint16_t)Q1, (uint16_t)Q3, (uint16_t)IQR, (uint16_t)outlier_low, (uint16_t)outlier_high);

//    APP_PRINTF("Outliers: ");
    /* If a sample is not outlier, then sum it */
	uint8_t sample_length = 0;
    uint32_t sample_sum = 0, sample_mean = 0;
    for (int i = 0; i < arr_len; i++)
    {
    	if ((arr[i] >= outlier_low) && (arr[i] <= outlier_high))
    	{
    		sample_sum += arr[i];
    		sample_length++;
    	} else {
//    		APP_PRINTF(" %d", arr[i]);
    	}
	}
//    APP_PRINTF("\r\n");

//    /* Ver 2: Sum it anyway */
//    for (i = 0; i < arr_len; i++)
//    {
//    	sample_sum += arr[i];
//    	sample_length++;
//    }
	
	/* Calculate the mean of the sample without the outlier */
	sample_mean = sample_sum/sample_length;

    return sample_mean;
}

/******
 * Next development agenda:
 * - Calculate Q1 and Q3 using formula.
 *
 */
