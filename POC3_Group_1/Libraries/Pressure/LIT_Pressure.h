/**
  ******************************************************************************
  * @file           : LIT_Pressure.h
  * @brief          : Header file for 4-20 mA Pressure Sensor Library
  * @author         : Andrean I.
  * @date           : September 2021
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_PRESSURE_H__
#define __LIT_PRESSURE_H__

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdint.h"
#include "adc_if.h"

/* Exported constants --------------------------------------------------------*/

/**
 * Default Watermark pin configuration
 */

/* Pin to Enable 12V Load Switch Pin of Pressure pin. Pin mode: GPIO Output */

#ifndef PRESSURE_EN_12V_GPIO_Port
#define PRESSURE_EN_12V_GPIO_Port           GPIOA
#endif

#ifndef PRESSURE_EN_12V_Pin
#define PRESSURE_EN_12V_Pin                 GPIO_PIN_8
#endif

/* Pin to read ADC input of Pressure single channel pin. Pin mode: ADC_IN */
#ifndef PRESSURE_IN_GPIO_Port
#define PRESSURE_IN_GPIO_Port               GPIOA
#endif

#ifndef PRESSURE_IN_Pin
#define PRESSURE_IN_Pin                     GPIO_PIN_14
#endif

#ifndef PRESSURE_IN_CHANNEL
#define PRESSURE_IN_CHANNEL                 ADC_CHANNEL_10
#endif

/* Pin to read ADC input of Channel 1 Pressure pin. Pin mode: ADC_IN */
#ifndef PRESSURE_IN1_GPIO_Port
#define PRESSURE_IN1_GPIO_Port               GPIOA
#endif

#ifndef PRESSURE_IN1_Pin
#define PRESSURE_IN1_Pin                     GPIO_PIN_14
#endif

#ifndef PRESSURE_IN1_CHANNEL
#define PRESSURE_IN1_CHANNEL                 ADC_CHANNEL_10
#endif

/* Pin to read ADC input of Channel 1 Pressure 2 pin. Pin mode: ADC_IN */
#ifndef PRESSURE_IN2_GPIO_Port
#define PRESSURE_IN2_GPIO_Port               GPIOA
#endif

#ifndef PRESSURE_IN2_Pin
#define PRESSURE_IN2_Pin                     GPIO_PIN_15
#endif

#ifndef PRESSURE_IN2_CHANNEL
#define PRESSURE_IN2_CHANNEL                 ADC_CHANNEL_11
#endif

/* Exported types ------------------------------------------------------------*/
/*
 * @brief Watermark error state
 */
typedef enum PressureError_e
{
	SENSOR_OK			 				= 0,
	SENSOR_NOT_DETECTED	 				= 1,
	SENSOR_ERROR		 				= 2,
	CHANNEL_1_SENSOR_NOT_DETECTED	 	= 3,
	CHANNEL_2_SENSOR_NOT_DETECTED	 	= 4,
	ALL_CHANNEL_SENSOR_NOT_DETECTED	 	= 5,
	CHANNEL_1_SENSOR_ERROR		 		= 6,
	CHANNEL_2_SENSOR_ERROR		 		= 7,
	ALL_CHANNEL_SENSOR_ERROR		 	= 8
} PressureError_t;

/* Exported functions prototypes ---------------------------------------------*/
/**
 *	@brief	Initialize Pressure sensor interface
 *	@param	None 
 */
void Pressure_Init(void);

/**
 *	@brief	Read the ADC input from the Pressure sensor interface
 *	@param	ADCValue store value of ADC
 *	@retval Error code
 */
uint8_t	Pressure_GetADC(uint16_t *ADCValue);

/**
 *	@brief	Read the ADC input and convert to voltage from the Pressure sensor interface
 *	@param	VoltageValue store value of the converted voltage in mV
 *	@retval Error code
 */
uint8_t	Pressure_GetVoltage(uint16_t *VoltageValue);

/**
 *	@brief	Read the ADC input and convert to output current from the Pressure sensor interface
 *	@param	CurrentValue store value of the converted current in uA
 *	@retval Error code
 */
uint8_t	Pressure_GetCurrent(uint16_t *CurrentValue);

/**
 *	@brief	Read the ADC input and convert to output pressure from the Pressure sensor interface
 *	@param	PressureValue store value of the converted pressure in mbar
 *	@retval Error code
 */
uint8_t Pressure_GetPressure(uint16_t *PressureValue);

/**
 *	@brief	Read the ADC input from the 2-Channel Pressure sensor interface
 *	@param	ADCValue1 store value of ADC on channel 1
 *	@param	ADCValue2 store value of ADC on channel 2
 *	@retval Error code
 */
uint8_t	Pressure_GetADC_2Channel(uint16_t *ADCValue1, uint16_t *ADCValue2);

/**
 *	@brief	Read the ADC input and convert to voltage from the 2-Channel Pressure sensor interface
 *	@param	VoltageValue1 store value of the converted voltage in mV on channel 1
 *	@param	VoltageValue1 store value of the converted voltage in mV on channel 2
 *	@retval Error code
 */
uint8_t	Pressure_GetVoltage_2Channel(uint16_t *VoltageValue1, uint16_t *VoltageValue2);

/**
 *	@brief	Read the ADC input and convert to output current from the 2-Channel Pressure sensor interface
 *	@param	CurrentValue store value of the converted current in uA on channel 1
 *	@param	CurrentValue store value of the converted current in uA on channel 2
 *	@retval Error code
 */
uint8_t	Pressure_GetCurrent_2Channel(uint16_t *CurrentValue1, uint16_t *CurrentValue2);

/**
 *	@brief	Read the ADC input and convert to output pressure from the 2-Channel Pressure sensor interface
 *	@param	PressureValue1 store value of the converted pressure in mbar on channel 1
 *	@param	PressureValue2 store value of the converted pressure in mbar on channel 2
 *	@retval Error code
 */
uint8_t Pressure_GetPressure_2Channel(uint16_t *PressureValue1, uint16_t *PressureValue2);

void Pressure_GetDebugInfo(uint16_t *BattVoltage, uint16_t *ADCValue, uint16_t *ADCVoltage);

void Pressure_GetDebugInfo_2Channel(uint16_t *BattVoltage, uint16_t *ADCValue1, uint16_t *ADCValue2, uint16_t *ADCVoltage1, uint16_t *ADCVoltage2);


#endif /* __LIT_PRESSURE_H__ */
