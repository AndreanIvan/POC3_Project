/*
 * SDI12_Lib.c
 *
 *  Created on    : Mar 24, 2020
 *  Last modified : Jul 28, 2021
 *      Author: Wisang Jati Anggoro, Andrean I.
 */

#include "stdio.h"
#include "string.h"

#include "main.h"
#include "stm32wlxx_hal_uart.h"
#include "sys_app.h"
#include "usart_if.h"

#include "SDI12_Lib.h"

char SDI12_data_buffer[SDI12_BUFFER_SIZE] = {0};
char address;
uint8_t timeToNextMeasure;

/*!
* \brief Read measures of the SDI12 sensor
* \param Sensor searched name
*        Sensor searched length
*        Sensor Serial Number
*        parameter1
*        parameter2
*        parameter3
*        parameter4
* \return 0 if sensor not present or invalid data. 1 Otherwise.
*/
uint8_t SDI12_readMeasures(char *sensorSearchedName, uint8_t sensorSearchedNameLength,
								char *sensorSerialNumber,
								float *parameter1,
								float *parameter2,
								float *parameter3,
								float *parameter4)
{
	char command[6];

	// READING
	char measures[50];
	uint8_t i = 0;

	uint8_t numberFields = 4;

	if(*parameter4 == -1000)
	{
		numberFields = 3;

		if(*parameter3 == -1000)
		{
			numberFields = 2;

			if(*parameter2 == -1000)
			{
				numberFields = 1;
			}
		}
	}
//	//check if correct SDI12 sensor connected
//	if (SDI12_isSensor(sensorSearchedName, sensorSearchedNameLength, sensorSerialNumber) == 0)
//	{
//		memset(sensorSerialNumber, 0x00, sizeof(sensorSerialNumber));
//		return 0;
//	}
//
//	if (SDI12_startSensor() == 0)
//	{
//		return 0;
//	}
//
//	// now wait timeToNextMeasure till data is ready + aditional delay
//	HAL_Delay(timeToNextMeasure*1000);
//
//	// send data command aD0!. Build command with address
//	sprintf(command, "%cD0!", address);
//
//	SDI12_Command(command, 4, 500);

	SDI12_Command("0!", 2, 500);
//	HAL_Delay(100);
	SDI12_Command("0M!", 3, 500);
//	HAL_Delay(100);
	SDI12_Command("0D0!", 4, 500);
//	HAL_Delay(500);

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	//skip address because it is not possible to connect more than one SDI-12 sensor at the same time


	while (i < 30)
	{
		measures[i] = SDI12_data_buffer[i+1];
		if (measures[i] == 0) break;
		i++;
	}

	// PARSING
	i = 0;
	uint8_t counter = 0;

	uint8_t a = 0;
	uint8_t b = 0;
	uint8_t c = 0;
	uint8_t d = 0;

	uint8_t len = strlen(measures);

	char stringParameter1[20];
	char stringParameter2[20];
	char stringParameter3[20];
	char stringParameter4[20];

	//Empty the arrays
	memset(stringParameter1, 0x00, sizeof(stringParameter1));
	memset(stringParameter2, 0x00, sizeof(stringParameter2));
	memset(stringParameter3, 0x00, sizeof(stringParameter3));
	memset(stringParameter4, 0x00, sizeof(stringParameter4));

	//if invalid data, return 0.
	if (len == 0)
	{
		return 0;
	}

	while((counter <= numberFields) && (i <= len))
	{
		if ((measures[i] == '+') || (measures[i] == '-'))
		{
			counter++;
		}
		switch (counter)
		{
			case 1:
				stringParameter1[a] = measures[i];
				a++;
				break;

			case 2:
				stringParameter2[b] = measures[i];
				b++;
				break;

			case 3:
				stringParameter3[c] = measures[i];
				c++;
				break;

			case 4:
				stringParameter4[d] = measures[i];
				d++;
				break;

			default:
				break;
		}
		i++;
	}

	//add eof to strings
	stringParameter1[a] = '\0';
	stringParameter2[b] = '\0';
	stringParameter3[c] = '\0';
	stringParameter4[d] = '\0';


	char * endptr;
	// Convert strings to float values
	*parameter1 = strtof(stringParameter1,&endptr);
	*parameter2 = strtof(stringParameter2,&endptr);
	*parameter3 = strtof(stringParameter3,&endptr);
	*parameter4 = strtof(stringParameter4,&endptr);

#if SDI12_DEBUG
	SDI_printf("d temp e: %d %d %d\r\n",(int)*parameter1,(int)*parameter2,(int)*parameter3);
#endif

	// If every value have been measured, return 1.
	// (counter == numberFields + 1) explication:
	// Some sensors include a '+' simbol at the end of the SDI12 string
	// "+25.582+7.1277-2.2480-8.3837+"
	if ((counter == numberFields) || (counter == numberFields + 1))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/*!
 * \brief detects if SDI12 sensor is present
 * \param Sensor searched name
 *        Sensor searched length
 *        Sensor Serial Number
 * \return return 0 if not, 1 if present.
 */
uint8_t SDI12_isSensor(char *sensorSearchedName, uint8_t sensorSearchedNameLength, char *sensorSerialNumber)
{
	char sensorParsedName[7];
	char sensorVersion[4];
	char command[6];

	// clear variables
	memset(sensorParsedName, 0x00, sizeof(sensorParsedName));
	memset(sensorVersion, 0x00, sizeof(sensorVersion));

	// check if sensor is present. Copy command from flash memory and send it.
	sprintf(command,"?I!");
	HAL_Delay(30);
	int stat2 = SDI12_Command(command, 3, 500);

	//necessary delay
	HAL_Delay(30);

	/* readMeasures answer
	* SENSOR ADDRESS | SDI12 SUPPORT | VENDOR | MODEL | VERSION | SERIAL |
	*/

		address = SDI12_data_buffer[0];

		// SDI12 SUPPORT model
		for(uint8_t i = 0; i < 8; i++)
		{
			SDI12_data_buffer[i+3];
		}

		// sensor model
		for(uint8_t i = 0; i < 6; i++)
		{
			sensorParsedName[i] = SDI12_data_buffer[i+11];
		}

		// sensor version
		for(uint8_t i = 0; i < 3; i++)
		{
			sensorVersion[i] = SDI12_data_buffer[i+17];
		}

		// sensor serial number
		for(uint8_t i = 0; i < 13; i++)
		{
			char byte_buffer = SDI12_data_buffer[i+20];
			//13 is carriage return, end of serial number
			if (byte_buffer == 13)
			{
				break;
			}
			else
			{
				sensorSerialNumber[i] = byte_buffer;

			}
			// add end of string
			sensorSerialNumber[i+1] = 0x00;
		}

	//Compare if the Model field returned by the sensor is equal to sensorSearchedName
	if(strncmp(sensorParsedName, sensorSearchedName, sensorSearchedNameLength) == 0)
	{
		return 1;
	}
	else
	{
		if (sensorParsedName[0] == 0x00)
		{
			#if SDI12DEBUG > 0
				char message[20];
				//"sensor not detected"
				strcpy_P(message, (char*)pgm_read_word(&(table_sdi12[6])));
				PRINTLN_SDI12(message);
			#endif
		}
		else
		{
			#if SDI12DEBUG > 0
				PRINTLN_SDI12(F("Sensor name not matched"));
			#endif
		}

		return 0;
	}
}

/* Send command to SDI-12 UART and receive via UART Interrupt */
int SDI12_Command(uint8_t *data, uint8_t commandLength, int timeout)
{
#if SDI12_LOAD_SWITCH
	/* Activate SDI-12 device */
	HAL_GPIO_WritePin(SDI12_LOAD_SWITCH_GPIOx, SDI12_LOAD_SWITCH_GPIO_PIN, SDI12_LOAD_SWITCH_ACTIVE_STATE);
#endif

	HAL_GPIO_WritePin(SDI_RX_PULLUP_GPIO_Port, SDI_RX_PULLUP_Pin, GPIO_PIN_SET);

	/* Set SDI-12 control pin to allow transmitting from MCU */
	HAL_GPIO_WritePin(SDI_CTRL_GPIO_Port, SDI_CTRL_Pin, GPIO_PIN_RESET);

	/* Change SDI-12 UART pin to GPIO output to send marking */
	HAL_UART_MspDeInit(&huart1);
	SDI12_SetTxToOutput();

	/* Send Marking */
	HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, GPIO_PIN_RESET);
	HAL_Delay(13);

	/* Send break and return GPIO output pin to SDI-12 UART */
	HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, GPIO_PIN_SET);
	HAL_Delay(9);
	HAL_UART_MspInit(&huart1);

	/* Send SDI-12 Command */
	if(HAL_UART_Transmit(&huart1, data, commandLength, 100) != HAL_OK)
	{
		return HAL_ERROR;
	}

	/* Set SDI-12 control pin to allow MCU receiving data */
	HAL_GPIO_TogglePin(SDI_CTRL_GPIO_Port, SDI_CTRL_Pin);

	/* Begin receive response via interrupt */
	startUartSDI12Receive(&huart1);
	HAL_Delay(1000);
	getSDI12RxBuffer(SDI12_data_buffer);

#if SDI12_DEBUG
	/* Print command and response to USB UART */
	APP_PRINTF("Dikirim  : %s\n", data);
	APP_PRINTF("Diterima : %s", SDI12_data_buffer);
	for (int i=0; i<20; i++){
		APP_PRINTF("%d ", SDI12_data_buffer[i]);
	}
	APP_PRINTF("...\n");

#endif

#if SDI12_LOAD_SWITCH
	/* Deactivate SDI-12 device */
	HAL_GPIO_TogglePin(SDI12_LOAD_SWITCH_GPIOx, SDI12_LOAD_SWITCH_GPIO_PIN);
#endif

	return HAL_OK;
}

void SDI12_GPIO_FlowControlInit(void){
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  if (SDI12_FLOW_CTRL_GPIOx == GPIOA)
	  __HAL_RCC_GPIOA_CLK_ENABLE();
  else if (SDI12_FLOW_CTRL_GPIOx == GPIOB)
	  __HAL_RCC_GPIOB_CLK_ENABLE();
  else if (SDI12_FLOW_CTRL_GPIOx == GPIOC)
	  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(SDI12_FLOW_CTRL_GPIOx, SDI12_FLOW_CTRL_GPIO_PIN, !(SDI12_FLOW_CTRL_TRANSMIT));

  /*Configure GPIO pin : PA7 */
  GPIO_InitStruct.Pin = SDI12_FLOW_CTRL_GPIO_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(SDI12_FLOW_CTRL_GPIOx, &GPIO_InitStruct);
}

void SDI12_SetTxToOutput(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_USART1_CLK_DISABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7|GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin : PB6 to Output */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB7 to Analog */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

uint8_t SDI12_startSensor()
{
	char command[6];
	char numberOfMeasures = 0;
	char timeToNextMeasure_string[4];

	memset(timeToNextMeasure_string, 0x00, sizeof(timeToNextMeasure_string));

	sprintf(command, "%cM!", address);
	SDI12_Command(command, 3, 500);

	//necessary delay
	HAL_Delay(30);

	// readMeasures answer
	// Measurement data will be available after specified time
	timeToNextMeasure_string[0] = SDI12_data_buffer[1];
	timeToNextMeasure_string[1] = SDI12_data_buffer[2];
	timeToNextMeasure_string[2] = SDI12_data_buffer[3];

	timeToNextMeasure = atoi(timeToNextMeasure_string);
	// Number of values returned
	numberOfMeasures = SDI12_data_buffer[4];

	return 1;
}

void SDI12_writeChar(uint8_t out)
{
	// parity bit
	out |= (CalEvenParity(out)<<7);

	HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, SET);
	HAL_Delay(SPACING);

	for (uint8_t mask = 0x01; mask; mask<<=1)
	{
		// send payload
		if(out & mask)
		{
			HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, RESET);
		}
		else
		{
			HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, SET);
		}
		HAL_Delay(SPACING);

	}

	// Stop bit
	HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, RESET);
	HAL_Delay(SPACING);
	HAL_GPIO_WritePin(SDI12_TX_GPIOx, SDI12_TX_GPIO_PIN, SET);
}

uint8_t CalEvenParity(uint8_t data)
{
	uint8_t parity=0;

	while(data){
			parity^=(data &1);
			data>>=1;
				}
	return (parity);
}


