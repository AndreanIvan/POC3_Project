/**
  ******************************************************************************
  * @file           : LIT_SDI12.c
  * @brief          : Source file for SDI-12 Library
  * @author         : Andrean I.
  * @date           : August 2021
  ******************************************************************************
  */

#include "LIT_SDI12.h"

//#include "stm32wlxx_hal.h"

#include "sys_app.h"

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

#define BUFFER_SIZE	100

char RxData[BUFFER_SIZE] = {0};
char TxData[BUFFER_SIZE] = {0};

char data_buffer[BUFFER_SIZE] = {0};

/* Private function prototypes -----------------------------------------------*/
static void VWC_USART1_UART_Init(void);
static void SetSDI_Transmit(void);
static void SetSDI_Receive(void);
static void SetSDI_Output(void);

/* Private Variable --------------------------------------------------------- */

/* Exported function ---------------------------------------------------------*/
void VWC_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, SDI_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : SDI_CTRL_Pin SDI_EN_Pin */
  GPIO_InitStruct.Pin = SDI_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//  /*Configure GPIO pin : SDI_RX_PULLUP_Pin */
//  GPIO_InitStruct.Pin = SDI_RX_PULLUP_Pin;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//  HAL_GPIO_Init(SDI_RX_PULLUP_GPIO_Port, &GPIO_InitStruct);

  VWC_USART1_UART_Init();
  HAL_UART_DeInit(&huart1);

}

void SDI_Read(char command[])
{

	sprintf(TxData, command);

	char StrInfo[150];
	sprintf(StrInfo, "Command Sent: %s\r\n", TxData);
	HAL_UART_Transmit(&huart2, (uint8_t *)StrInfo, strlen(StrInfo), HAL_MAX_DELAY);

	/* Set SDI to Output */
	SetSDI_Output();

	/* Send Marking */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
	HAL_Delay(12);

  // /* Send break and return GPIO output pin to SDI-12 UART */
  // HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
  // HAL_Delay(8);
	HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6);

	/* Set PB6 to Transmit */
	SetSDI_Transmit();
	HAL_UART_Transmit(&huart1, (uint8_t *)TxData, strlen(TxData), HAL_MAX_DELAY);

  /* Set PB6 to Receive */
	SetSDI_Receive();
	HAL_UART_Receive(&huart1, (uint8_t *)RxData, 21, 250);

	/* Print data received from SDI */
	if (strcmp(RxData, ""))
	{
	  char StrRec[150];
		sprintf(StrRec, "Response Received: %s", RxData);
		HAL_UART_Transmit(&huart2, (uint8_t *)StrRec, strlen(StrRec), HAL_MAX_DELAY);
	}

	sprintf(data_buffer, RxData);
	HAL_UART_DeInit(&huart1);

	memset(TxData, 0x00, sizeof(TxData));
	memset(RxData, 0x00, sizeof(RxData));
}

/*!
* \brief Read measures of the SDI12 sensor
* \param Sensor searched name
*        Sensor searched length
*        Sensor Serial Number
*        parameter1
*        parameter2
*        parameter3
*        parameter4
* \return 0 if sensor not present or invalid data. 1 Otherwise.
*/
uint8_t SDI12_Measures(	float *parameter1,
						float *parameter2,
						float *parameter3,
						float *parameter4)
{
	// READING
	char measures[50];

	uint8_t numberFields = 4;

	if(*parameter4 == -1000)
	{
		numberFields = 3;

		if(*parameter3 == -1000)
		{
			numberFields = 2;

			if(*parameter2 == -1000)
			{
				numberFields = 1;
			}
		}
	}

	/* Enable SDI */
	HAL_GPIO_WritePin(SDI_EN_GPIO_Port, SDI_EN_Pin, GPIO_PIN_SET);

//	/* Enable RX Pullup */
//	HAL_GPIO_WritePin(SDI_RX_PULLUP_GPIO_Port, SDI_RX_PULLUP_Pin, GPIO_PIN_SET);

	HAL_Delay(650);

	uint8_t i = 0;
//	while ((i < 5) && (strlen(data_buffer) < 5))
//	{
//		//	SDI_Read("0!");
//		//	HAL_Delay(100);
//			SDI_Read("0M!");
//			SDI_Read("0D0!");
//		//	HAL_Delay(500);
//			i++;
//	}

	SDI_Read("0M!");
	SDI_Read("0D0!");

	/* Disable SDI */
	HAL_GPIO_WritePin(SDI_EN_GPIO_Port, SDI_EN_Pin, GPIO_PIN_RESET);

//	/* Disable RX Pullup */
//	HAL_GPIO_WritePin(SDI_RX_PULLUP_GPIO_Port, SDI_RX_PULLUP_Pin, GPIO_PIN_RESET);

	// clear measures array
	memset(measures, 0x00, sizeof(measures));

	// store the reading in measures buffer
	//skip address because it is not possible to connect more than one SDI-12 sensor at the same time

	i = 0;
	while (i < 30)
	{
		measures[i] = data_buffer[i+1];
		if (measures[i] == 0) break;
		i++;
	}

	// PARSING
	i = 0;
	uint8_t counter = 0;

	uint8_t a = 0;
	uint8_t b = 0;
	uint8_t c = 0;
	uint8_t d = 0;

	uint8_t len = strlen(measures);

	char stringParameter1[20];
	char stringParameter2[20];
	char stringParameter3[20];
	char stringParameter4[20];

	//Empty the arrays
	memset(stringParameter1, 0x00, sizeof(stringParameter1));
	memset(stringParameter2, 0x00, sizeof(stringParameter2));
	memset(stringParameter3, 0x00, sizeof(stringParameter3));
	memset(stringParameter4, 0x00, sizeof(stringParameter4));

//	APP_PRINTF("measure len: %d\r\n", len);

	//if invalid data, return 0.
	if (len == 0)
	{
		return 0;
	}

	while((counter <= numberFields) && (i <= len))
	{
		if ((measures[i] == '+') || (measures[i] == '-'))
		{
			counter++;
		}
		switch (counter)
		{
			case 1:
				stringParameter1[a] = measures[i];
				a++;
				break;

			case 2:
				stringParameter2[b] = measures[i];
				b++;
				break;

			case 3:
				stringParameter3[c] = measures[i];
				c++;
				break;

			case 4:
				stringParameter4[d] = measures[i];
				d++;
				break;

			default:
				break;
		}
		i++;
	}

	//add eof to strings
	stringParameter1[a] = '\0';
	stringParameter2[b] = '\0';
	stringParameter3[c] = '\0';
	stringParameter4[d] = '\0';

	char * endptr;
	// Convert strings to float values
	*parameter1 = strtof(stringParameter1,&endptr);
	*parameter2 = strtof(stringParameter2,&endptr);
	*parameter3 = strtof(stringParameter3,&endptr);
	*parameter4 = strtof(stringParameter4,&endptr);

	memset(data_buffer, 0x00, sizeof(data_buffer));

#if SDI12_DEBUG
	APP_PRINF("d temp e: %d %d %d\r\n",(int)*parameter1,(int)*parameter2,(int)*parameter3);
#endif

	// If every value have been measured, return 1.
	// (counter == numberFields + 1) explication:
	// Some sensors include a '+' simbol at the end of the SDI12 string
	// "+25.582+7.1277-2.2480-8.3837+"
	if ((counter == numberFields) || (counter == numberFields + 1))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

/* Private functions ---------------------------------------------------------*/

void VWC_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 1200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_EVEN;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_TXINVERT_INIT|UART_ADVFEATURE_RXINVERT_INIT;
  huart1.AdvancedInit.TxPinLevelInvert = UART_ADVFEATURE_TXINV_ENABLE;
  huart1.AdvancedInit.RxPinLevelInvert = UART_ADVFEATURE_RXINV_ENABLE;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
}

static void SetSDI_Transmit(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 1200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_EVEN;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_TXINVERT_INIT|UART_ADVFEATURE_RXINVERT_INIT;
  huart1.AdvancedInit.TxPinLevelInvert = UART_ADVFEATURE_TXINV_ENABLE;
  huart1.AdvancedInit.RxPinLevelInvert = UART_ADVFEATURE_RXINV_ENABLE;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
}

static void SetSDI_Receive(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 1200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_EVEN;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_TXINVERT_INIT|UART_ADVFEATURE_RXINVERT_INIT|UART_ADVFEATURE_SWAP_INIT;
  huart1.AdvancedInit.TxPinLevelInvert = UART_ADVFEATURE_TXINV_ENABLE;
  huart1.AdvancedInit.RxPinLevelInvert = UART_ADVFEATURE_RXINV_ENABLE;
  huart1.AdvancedInit.Swap = UART_ADVFEATURE_SWAP_ENABLE;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

static void SetSDI_Output(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pins : GPIO_PIN_6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}
