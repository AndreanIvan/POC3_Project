/*
 * SDI12_Lib.h
 *
 *  Created on    : Mar 24, 2020
 *  Last modified : Jul 28, 2021
 *      Author: Wisang Jati Anggoro, Andrean I.
 */

#ifndef INC_SDI12_LIB_H_
#define INC_SDI12_LIB_H_

#include "main.h"
#include <stdlib.h>

/*
 * User Definition
 */
extern UART_HandleTypeDef 				huart1;
extern UART_HandleTypeDef 				huart2;

/* Print data transmitted and received to/from SDI-12 */
#define SDI12_DEBUG						1

/* Define print function of SDI-12 */
#define SDI_printf						APP_PRINTF

/* Define SDI-12 buffer size */
#define SDI12_BUFFER_SIZE 				255

/* UART connected to SDI-12 */
#define SDI12_huart						huart1

/* Tx GPIO Pin of SDI-12 UART */
#define SDI12_TX_GPIOx					GPIOB
#define SDI12_TX_GPIO_PIN				GPIO_PIN_6

/* Flow Control GPIO Pin to control send/receive route from/to MCU */
#define SDI12_FLOW_CTRL_GPIOx			GPIOA
#define SDI12_FLOW_CTRL_GPIO_PIN		GPIO_PIN_4
#define SDI12_FLOW_CTRL_TRANSMIT		GPIO_PIN_RESET

/* If there's load switch to activate SDI-12 module, then set the GPIO Pin */
#define SDI12_LOAD_SWITCH				1
#if SDI12_LOAD_SWITCH
	#define SDI12_LOAD_SWITCH_GPIOx			GPIOA
	#define SDI12_LOAD_SWITCH_GPIO_PIN		GPIO_PIN_8
	#define SDI12_LOAD_SWITCH_ACTIVE_STATE	GPIO_PIN_SET
#endif

/* Init the Flow Control GPIO Pin (if necessary) */
void SDI12_GPIO_FlowControlInit(void);

/*
 * End of User Definition
 */

#define SPACING 						9

uint8_t SDI12_readMeasures(char *sensorSearchedName, uint8_t sensorSearchedNameLength,
								char *sensorSerialNumber,
								float *parameter1,
								float *parameter2,
								float *parameter3,
								float *parameter4);
uint8_t SDI12_isSensor(char *sensorSearchedName,
						uint8_t sensorSearchedNameLength,
						char *sensorSerialNumber);
int SDI12_Command(uint8_t *data, uint8_t commandLength, int timeout);
uint8_t SDI12_startSensor();
void SDI12_SetTxToOutput(void);
void SDI12_writeChar(uint8_t out);
uint8_t CalEvenParity(uint8_t data);

#endif /* INC_SDI12_LIB_H_ */
