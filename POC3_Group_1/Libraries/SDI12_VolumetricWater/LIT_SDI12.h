/**
  ******************************************************************************
  * @file           : LIT_SDI12.h
  * @brief          : Header for SDI-12 Library
  * @author         : Andrean I.
  * @date           : August 2021
  ******************************************************************************
  */

/* Define to prevent recursive inclusion */
#ifndef __LIT_SDI12_H__
#define __LIT_SDI12_H__

/* Include statement */
#include "main.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "sys_app.h"

/**
 * Default SDI12 pin configuration connected to the MCU
 **/

#ifndef SDI_EN_Pin
#define SDI_EN_Pin                  GPIO_PIN_8
#endif

#ifndef SDI_EN_GPIO_Port
#define SDI_EN_GPIO_Port            GPIOA
#endif

#ifndef SDI_RX_PULLUP_Pin
#define SDI_RX_PULLUP_Pin           GPIO_PIN_9
#endif

#ifndef SDI_RX_PULLUP_GPIO_Port
#define SDI_RX_PULLUP_GPIO_Port     GPIOA
#endif


/* Function Declaration */

/**
 *	@brief	Send command to SDI-12 sensor
 *	@param	command to be send
 */
void SDI_Read(char command[]);

uint8_t SDI12_Measures(	float *parameter1,
						float *parameter2,
						float *parameter3,
						float *parameter4);
void VWC_Init(void);

#endif /* __LIT_SDI12_H__ */
