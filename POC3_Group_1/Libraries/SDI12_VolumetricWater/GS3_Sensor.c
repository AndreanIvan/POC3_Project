/*
 * GS3_Sensor.c
 *
 *  Created on    : Mar 24, 2020
 *  Last modified : Jul 28, 2021
 *      Author: Wisang Jati Anggoro, Andrean I.
 */

#include "GS3_Sensor.h"
#include "sys_app.h"

struct sensorGS3Vector sensorGS3;

/*!
	\brief Reads the sensor data
	\param void
	\return 1 if ok, 0 if something fails
*/
uint8_t Decagon_GS3_read(float * dielectricPermittivity,
						  float * electricalConductivity,
						  float * temperature,
						  char * sensorSerialNumber)
{
	// Initialize variables
	sensorGS3.dielectricPermittivity = 0;
	sensorGS3.electricalConductivity = 0;
	sensorGS3.temperature = 0;
	float parameter4_dummy = -1000;

	uint8_t response = 0;
	uint8_t validMeasure = 0;
	uint8_t retries = 0;

	char sensorNameStr[4];
	memset(sensorNameStr, 0x00, sizeof(sensorNameStr));
	strncpy(sensorNameStr, "GS3", 3);

	while ((validMeasure == 0) && (retries < 5))
	{

		response = SDI12_readMeasures(sensorNameStr, strlen(sensorNameStr),
											sensorSerialNumber,
											&sensorGS3.dielectricPermittivity,
											&sensorGS3.temperature,
											&sensorGS3.electricalConductivity,
											&parameter4_dummy);
		strcpy(sensorGS3.sensorSerialNumber,sensorSerialNumber);


		if ((sensorGS3.dielectricPermittivity != 0)
		|| (sensorGS3.temperature != 0)
		|| (sensorGS3.electricalConductivity != 0))
		{
			validMeasure = 1;
		}
		else
		{
			HAL_Delay(1000);
		}
		retries++;
#if GS3_SENSOR_DEBUG
		APP_PRINTF("retry: %d\n",retries);
#endif
	}
#if GS3_SENSOR_DEBUG
	APP_PRINTF("Dielectric Permitivity : %d.%03d\n",
		(long)(sensorGS3.dielectricPermittivity),
		(long)(sensorGS3.dielectricPermittivity * 1000) % 1000);
	APP_PRINTF("Temperature ( Celcius ): %d.%03d\n",
		(long)(sensorGS3.temperature),
		(long)(sensorGS3.temperature * 1000) % 1000);
	APP_PRINTF("Electrical Conductivity: %d.%03d\n",
		(long)(sensorGS3.electricalConductivity),
		(long)(sensorGS3.electricalConductivity * 1000) % 1000);
#endif

	* dielectricPermittivity = sensorGS3.dielectricPermittivity;
	* electricalConductivity = sensorGS3.electricalConductivity;
	* temperature = sensorGS3.temperature;
	sensorSerialNumber = sensorGS3.sensorSerialNumber;

	return response;
}

