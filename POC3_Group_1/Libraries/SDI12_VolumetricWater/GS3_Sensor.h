/*
 * GS3_Sensor.h
 *
 *  Created on    : Mar 24, 2020
 *  Last modified : Jul 28, 2021
 *      Author: Wisang Jati Anggoro, Andrean I.
 */

#ifndef INC_GS3_SENSOR_H_
#define INC_GS3_SENSOR_H_


#include "SDI12_Lib.h"
#include "main.h"
#include <string.h>

/*
 * User Definition
 */

#define GS3_SENSOR_DEBUG 		0
#define GS3_printf				APP_PRINTF

/*
 * End of User Definition
 */

/*!
 * \struct sensorGS3Vector
 * \brief Struct to store data of the GS3 sensor
 */

struct sensorGS3Vector
{

	//! Variable: stores measured dielectric permittivity (dimensionless) in float type
	float dielectricPermittivity;

	//! Variable: stores measured electrical conductivity in Î¼S/cm in float type
	float electricalConductivity;

	//! Variable: stores measured temperature in degrees Celsius in float type
	float temperature;

	//Sensor serial number variable
	char sensorSerialNumber[14];
};

uint8_t Decagon_GS3_read(float * dielectricPermittivity,
						  float * electricalConductivity,
						  float * temperature,
						  char * sensorSerialNumber);


#endif /* INC_GS3_SENSOR_H_ */
