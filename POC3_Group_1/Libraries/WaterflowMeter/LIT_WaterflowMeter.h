/**
  ******************************************************************************
  * @file           : LIT_WaterflowMeter.h
  * @brief          : Header for Waterflow Meter Library
  * @author         : Andrean I.
  * @date           : August 2021
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_PLUVIO_WATERFLOW_H__
#define __LIT_PLUVIO_WATERFLOW_H__

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdint.h"

/* Exported constants --------------------------------------------------------*/

/*!
 * Define Pluvio Waterflow Sensor read duration in ms 
 */
#define WATERFLOW_READ_DURATION        1000

/*
 * Default Pluvio Waterflow pin configuration connected to the MCU
 */

#ifndef WATERFLOW_IN_Pin
#define WATERFLOW_IN_Pin               GPIO_PIN_4
#endif

#ifndef WATERFLOW_IN_GPIO_Port
#define WATERFLOW_IN_GPIO_Port         GPIOA
#endif

#ifndef WATERFLOW_IN_EXTI_IRQn
#define WATERFLOW_IN_EXTI_IRQn         EXTI4_IRQn
#endif

/* Exported functions prototypes ---------------------------------------------*/
/**
 *	@brief	Initialize Waterflow sensor
 *	@param	None
 */
void Waterflow_Init(void);

/**
 *	@brief	Increment the pluvio counter based on the pulse signal
 *	@param	None
 */
void Waterflow_CounterIncrement(void);

/**
 *	@brief	Get the current counter value
 *	@param	CounterValue variable to store the result
 *	@retval	Error code
 */
uint8_t Waterflow_GetCounter(uint32_t *CounterValue);

#endif /* __LIT_PLUVIO_WATERFLOW_H__ */
