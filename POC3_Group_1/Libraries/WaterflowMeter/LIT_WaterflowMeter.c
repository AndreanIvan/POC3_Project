/**
  ******************************************************************************
  * @file           : LIT_WaterflowMeter.c
  * @brief          : Source file for Waterflow Meter Library
  * @author         : Andrean I.
  * @date           : August 2021
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIT_WaterflowMeter.h"

/* Private variables ---------------------------------------------------------*/

/* 
 * Declare counter of pulse interrupt. Can be used only in this file (static)
 */
static uint32_t Waterflow_Counter = 0;

/* Exported functions ---------------------------------------------------------*/
void Waterflow_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*Configure GPIO pin : WATERFLOW_IN_Pin */
	GPIO_InitStruct.Pin = WATERFLOW_IN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(WATERFLOW_IN_GPIO_Port, &GPIO_InitStruct);

    /* Enable and set Button EXTI Interrupt to the lowest priority */
    HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);
}

void Waterflow_CounterIncrement(void)
{
    Waterflow_Counter++;
}

uint8_t Waterflow_GetCounter(uint32_t *CounterValue)
{
    *CounterValue = Waterflow_Counter;
    return HAL_OK;
}
