/**
  ******************************************************************************
  * @file           : LIT_SolenoidValve.h
  * @brief          : Header file for Solenoid Valve
  * @author         : Andrean I.
  * @date           : September 2021
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_SOLENOID_VALVE_H__
#define __LIT_SOLENOID_VALVE_H__

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "adc_if.h"

/* Exported constants --------------------------------------------------------*/

/* --------------------------- Valve HW definition -------------------------------*/
#define VALVE_R_VCAP_DIV_UPPER                      680
#define VALVE_R_VCAP_DIV_LOWER                      100
#define VALVE_VSEL                                  14000		// mV
#define VALVE_VCAP_LOW_THRESHOLD                    15400		// mV
#define VALVE_DISCONNECT_THRESHOLD                  6.5

#define VALVE_CHARGING_TIMEOUT						20000		// charging timeout in ms

#define VALVE_FAILSAFE                              1
#define VALVE_FAILSAFE_TIMER                        600000 // ms

/**
 * Default Watermark pin configuration
 */

/* Enable Load Switch Pin of Watermark interface/shield. Pin mode: GPIO Output */

#ifndef VALVE_VSENSE_Pin
#define VALVE_VSENSE_Pin                    GPIO_PIN_2
#endif

#ifndef VALVE_VSENSE_GPIO_Port
#define VALVE_VSENSE_GPIO_Port              GPIOB
#endif

#ifndef VALVE_VSENSE_ADC_Channel
#define VALVE_VSENSE_ADC_Channel            ADC_CHANNEL_4
#endif

#ifndef VALVE_CTRL_Pin
#define VALVE_CTRL_Pin                      GPIO_PIN_3
#endif

#ifndef VALVE_CTRL_GPIO_Port
#define VALVE_CTRL_GPIO_Port                GPIOB
#endif

#ifndef VALVE_IN1_Pin
#define VALVE_IN1_Pin                       GPIO_PIN_4
#endif

#ifndef VALVE_IN1_GPIO_Port
#define VALVE_IN1_GPIO_Port                 GPIOB
#endif

#ifndef VALVE_IN2_Pin
#define VALVE_IN2_Pin                       GPIO_PIN_5
#endif

#ifndef VALVE_IN2_GPIO_Port
#define VALVE_IN2_GPIO_Port                 GPIOB
#endif

/* Exported types ------------------------------------------------------------*/
typedef enum ValveState_e
{
	VALVE_STATE_CLOSE = 0,
	VALVE_STATE_OPEN  = 1
} ValveState_t;

typedef enum ValveError_e
{
  VALVE_OK              = 0,
  VALVE_ALREADY_OPENED  = 1,
  VALVE_ALREADY_CLOSED  = 2,
  VALVE_TIMEOUT_REACHED = 3
} ValveError_t;

/* Exported functions prototypes ---------------------------------------------*/

/**
 *	@brief	Read the input from Watermark interface
 *	@param	Variable to store value of soil moisture in Pa or cbar/1000
 *	@param	Integer showing which WM selected
 *	@retval Error code
 */

/**
 *  Initialize Valve Pin
 **/
void Valve_Init(void);

/**
 *  Set Valve State
 **/
uint8_t Valve_SetState(uint8_t state);

/**
 *  Get Valve State
 **/
uint8_t Valve_GetState(void);

/**
 *  return Failsafe duration
 **/
uint32_t Valve_GetFailsafeDuration(void);
void ValveSetFailsafeDuration(uint32_t durationMs);


/**
 *  Failsafe Current State
 **/
uint8_t Valve_GetFailsafeCurrentState(void);
void Valve_SetFailsafeCurrentState(uint8_t state);

/**
 *  Failsafe Enabled State
 **/
uint8_t Valve_GetFailsafeEnabledState(void);
void Valve_SetFailsafeEnabledState(uint8_t state);

uint8_t Valve_GetDebugInfo(uint16_t *InitialCapVoltage, uint16_t *FinalCapVoltage, uint16_t *ChargingDuration);
#endif /* __LIT_SOLENOID_VALVE_H__ */
