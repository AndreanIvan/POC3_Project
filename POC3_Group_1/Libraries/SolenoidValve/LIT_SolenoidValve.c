/**
  ******************************************************************************
  * @file           : LIT_SolenoidValve.c
  * @brief          : Source file for Solenoid Valve
  * @author         : Andrean I.
  * @date           : September 2021
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIT_SolenoidValve.h"
#include "adc_if.h"

#include "string.h"
#include "stdio.h"

/* External variables --------------------------------------------------------*/
extern ADC_HandleTypeDef hadc;
extern UART_HandleTypeDef huart2;

/* Private function prototypes -----------------------------------------------*/
static void Valve_Open(void);
static void Valve_Close(void);
//static void MX_ADC_Init(void);
static uint32_t ADC_GetADC(uint32_t channel);
static uint32_t ADC_ReadChannels(uint32_t channel);
static uint16_t meanOf(uint16_t arr[]);

/* Private constants ---------------------------------------------------------*/


#define ADC_NUMBER_OF_SAMPLE					10
#define VALVE_VREF								SYS_GetBatteryLevel()


#define VALVE_PRINT_LOG               			1


#define USE_PRINTF

#if defined(USE_PRINTF)

#include "sys_app.h"
#define	PrintLog	APP_PRINTF

#else
static void PrintLog(const char *format, ...)
{
#if defined(VALVE_PRINT_LOG) && (VALVE_PRINT_LOG != 0)
	static char ll_msg_buf_[127];
    va_list args;
    size_t len;

    /* Format the string */
    __builtin_va_start(args, format);
    len = vsnprintf(ll_msg_buf_, 127, &format[0], args);
    __builtin_va_end(args);
    HAL_UART_Transmit(&huart2, (uint8_t *)&ll_msg_buf_, len, 0xFFFF);
#endif
}
#endif

/* Private variables ---------------------------------------------------------*/
static uint8_t failsafeCurrentState = 0;
static uint8_t failsafeEnabledState = 1;
static uint32_t failsafeDurationMs = VALVE_FAILSAFE_TIMER;

static uint16_t CapVoltage_Initial;
static uint16_t CapVoltage_Final;
static uint16_t CapCharging_Duration;

/* Private typedef -----------------------------------------------------------*/
ValveState_t ValveState = VALVE_STATE_OPEN;

/* Exported functions --------------------------------------------------------*/

void Valve_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, VALVE_CTRL_Pin|VALVE_IN1_Pin|VALVE_IN2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : VALVE_CTRL_Pin VALVE_IN1_Pin VALVE_IN2_Pin */
  GPIO_InitStruct.Pin = VALVE_CTRL_Pin|VALVE_IN1_Pin|VALVE_IN2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* Close Valve */
  Valve_SetState(VALVE_STATE_CLOSE);
}

uint8_t Valve_SetState(uint8_t state)
{
    uint32_t Charging_StartTime, Charging_StopTime, Charging_Duration = 0;
    uint16_t ADC_Value, CapVoltage = 0, BattVoltage = 0;

    uint32_t CurrentProgress, DisplayProgress = 0;

    CapVoltage_Initial = CapVoltage;

    /* If current state equal to set state, give notification and end the function  */
    if (state == ValveState)
    {
      if (state == VALVE_STATE_OPEN)
      {
        PrintLog("Valve already opened!\r\n");
        return VALVE_ALREADY_OPENED;
      }
      else 
      {
        PrintLog("Valve already closed!\r\n");
        return VALVE_ALREADY_CLOSED;
      }
    }
    else
    {
      /* Switch control to charge capacitor */
      HAL_GPIO_WritePin(VALVE_CTRL_GPIO_Port, VALVE_CTRL_Pin, GPIO_PIN_SET);
      
      /* Mark the charging start time */
      Charging_StartTime = HAL_GetTick();

      /* Charge the capacitor until reach threshold voltage or charging timeout */
      while((CapVoltage < VALVE_VSEL) && ((HAL_GetTick() - Charging_StartTime) < VALVE_CHARGING_TIMEOUT))
      {
        ADC_Value = ADC_ReadChannels(VALVE_VSENSE_ADC_Channel);
        BattVoltage = VALVE_VREF;
        CapVoltage = ADC_Value
                       * ( VALVE_R_VCAP_DIV_UPPER + VALVE_R_VCAP_DIV_LOWER )
                       / VALVE_R_VCAP_DIV_LOWER * BattVoltage / 4095;

        /* Save the initial capacitor voltage for debugging purpose */
        if (CapVoltage_Initial == 0)
        	CapVoltage_Initial = CapVoltage;

        /* Charging log */

#define USE_PRINT_LOG
#if defined(USE_PRINT_LOG)
        CurrentProgress = (uint32_t)(20 * CapVoltage / VALVE_VSEL);

        // PrintLog("CapVoltage: %2d.%03d V | i = %d \r\n", (uint16_t)(CapVoltage/1000), (uint16_t)(CapVoltage%1000), CurrentProgress);
        PrintLog("ADC Value: %4d | CapVoltage: %2d.%03d V | BattVoltage: %4d mV | i = %d \r\n", ADC_Value, (uint16_t)(CapVoltage/1000), (uint16_t)(CapVoltage%1000), BattVoltage, CurrentProgress);
#else
        CurrentProgress = (uint32_t)(20 * CapVoltage / VALVE_VSEL);

        if(DisplayProgress == 0)
        {
          PrintLog("Charging the capacitor...\r\n");
          PrintLog("|----- Progress -----|\r\n");
          PrintLog("|=");
          DisplayProgress++;
        }
        if( CurrentProgress > DisplayProgress )
        {
        	PrintLog("=");
        	DisplayProgress++;
        }
        if (CurrentProgress >= 20)
        {
        	PrintLog("|\r\n");
        }
#endif

        HAL_Delay(100);
      }
      
      /* Mark the charging stop time */
      Charging_StopTime = HAL_GetTick();

      /* Count charging duration */
      Charging_Duration = Charging_StopTime - Charging_StartTime;
      PrintLog("CapVoltage: %d.%03d V\r\n", (uint16_t)(CapVoltage/1000), (uint16_t)(CapVoltage%1000));
      PrintLog("Charging Duration : %d ms\r\n", Charging_Duration);

      /* Save the final capacitor voltage and charging duration for debugging purpose */
      CapVoltage_Final = CapVoltage;
      CapCharging_Duration = Charging_Duration;

      /* Send pulse to open or close valve */
      if (state == VALVE_STATE_OPEN)
      {
        Valve_Open();
        ValveState = VALVE_STATE_OPEN;
//        PrintLog("Valve opened.\r\n");

        /* If valve is opened and failsafe is enabled, then close the valve in a certain  */
      }
      else 
      {
        Valve_Close();
        ValveState = VALVE_STATE_CLOSE;
//        PrintLog("Valve closed.\r\n");
      }

      if (Charging_Duration >= VALVE_CHARGING_TIMEOUT)
    	  return VALVE_TIMEOUT_REACHED;
      else
    	  return VALVE_OK;
    }
}
uint8_t Valve_GetState(void)
{
    return ValveState;
}
uint32_t Valve_GetFailsafeDuration(void)
{
    return failsafeDurationMs;
}
void Valve_SetFailsafeDuration(uint32_t durationMs)
{
    failsafeDurationMs = durationMs;
}
uint8_t Valve_GetFailsafeCurrentState(void)
{
    return failsafeCurrentState;
}
void Valve_SetFailsafeCurrentState(uint8_t state)
{
    failsafeCurrentState = state;
}
uint8_t Valve_GetFailsafeEnabledState(void)
{
    return failsafeEnabledState;
}
void Valve_SetFailsafeEnabledState(uint8_t state)
{
    failsafeEnabledState = state;
}

// Current State --> Pas valve lagi kondisi nyala, ngecek failsafe nyala apa nggak, untuk notif uplink
// Failsafe Enabled --> Kalo valvenya nyala bakal dinyalain apa engga, untuk action dari downlink

// Valve menutup --> Jeda datar
// Valve membuka --> Jeda turun

uint8_t Valve_GetDebugInfo(uint16_t *InitialCapVoltage, uint16_t *FinalCapVoltage, uint16_t *ChargingDuration)
{
	*InitialCapVoltage = CapVoltage_Initial;
	*FinalCapVoltage = CapVoltage_Final;
	*ChargingDuration = CapCharging_Duration;

	CapVoltage_Initial = 0;
	CapVoltage_Final = 0;
	CapCharging_Duration = 0;
}


/* Private functions ---------------------------------------------------------*/

static uint32_t ADC_ReadChannels(uint32_t channel)
{
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;
}

//static void MX_ADC_Init(void)
//{
//  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
//  */
//  hadc.Instance = ADC;
//  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
//  hadc.Init.Resolution = ADC_RESOLUTION_12B;
//  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
//  hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
//  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
//  hadc.Init.LowPowerAutoWait = DISABLE;
//  hadc.Init.LowPowerAutoPowerOff = DISABLE;
//  hadc.Init.ContinuousConvMode = DISABLE;
//  hadc.Init.NbrOfConversion = 1;
//  hadc.Init.DiscontinuousConvMode = DISABLE;
//  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
//  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
//  hadc.Init.DMAContinuousRequests = DISABLE;
//  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
//  hadc.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_1CYCLE_5;
//  hadc.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_1CYCLE_5;
//  hadc.Init.OversamplingMode = DISABLE;
//  hadc.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
//  if (HAL_ADC_Init(&hadc) != HAL_OK)
//  {
//    Error_Handler();
//  }
//
//}

void Valve_Open(void)
{
	HAL_GPIO_WritePin(VALVE_CTRL_GPIO_Port, VALVE_CTRL_Pin, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN2_Pin, GPIO_PIN_RESET);
	HAL_Delay(220);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN2_Pin, GPIO_PIN_RESET);
}

void Valve_Close(void)
{
	HAL_GPIO_WritePin(VALVE_CTRL_GPIO_Port, VALVE_CTRL_Pin, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN2_Pin, GPIO_PIN_SET);
	HAL_Delay(220);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(VALVE_IN1_GPIO_Port, VALVE_IN2_Pin, GPIO_PIN_RESET);
}

uint32_t ADC_GetADC(uint32_t channel)
{
    uint16_t ADC_Sampled[ADC_NUMBER_OF_SAMPLE];
    uint16_t ADC_Measured;

    /* Read ADC */
    for (int i = 0; i < ADC_NUMBER_OF_SAMPLE; i++)
    {
    	ADC_Sampled[i] = ADC_ReadChannels(channel);
    	HAL_Delay(1);
    }

    ADC_Measured = meanOf(ADC_Sampled);

    return ADC_Measured;
}

uint16_t meanOf(uint16_t arr[])
{
	/* Calculate the array length */
    size_t arr_len = ADC_NUMBER_OF_SAMPLE;
    
    /* Sort the array using bubble sort algorithm */
    uint8_t i, j;
    for (i = 0; i < arr_len-1; i++)      
        for (j = 0; j < arr_len-i-1; j++) 
           if (arr[j] > arr[j+1])
           {
           		arr[j] 	 = arr[j] + arr[j+1];
           		arr[j+1] = arr[j] - arr[j+1];
           		arr[j]   = arr[j] - arr[j+1];
		   }
    
//    APP_PRINTF("Sorted:");
//    for (i = 0; i < arr_len; i++)
//    	APP_PRINTF(" %d", arr[i]);
//
//    APP_PRINTF("\r\n");

    /* Calculate the interquartile range */
    uint8_t	q1_idx = 0, q3_idx = 0;
    float Q1, Q3, IQR;

    q1_idx = (uint8_t)((arr_len + 1) / 4);
    q3_idx = (uint8_t)((arr_len + 1) * 3 / 4);

    if ((arr_len+1)/4 % 2 != 0)
    {
    	Q1 = (arr[q1_idx - 1] + arr[q1_idx]) / 2;
    	Q3 = (arr[q3_idx - 1] + arr[q3_idx]) / 2;
    }
    else
    {
    	Q1 = arr[q1_idx - 1];
    	Q3 = arr[q3_idx - 1];
    }

    IQR = Q3-Q1;

    /* Set the outlier */
    float outlier_low, outlier_high;
    outlier_low  = Q1 - 3/2*IQR;
    outlier_high = Q3 + 3/2*IQR;

//    APP_PRINTF("ArrLen: %d | q1_idx: %d | q3_idx: %d | Q1: %d | Q3: %d | IQR: %d | out_low: %d | out_high: %d | Outlier(s):", arr_len, q1_idx, q3_idx, (uint16_t)Q1, (uint16_t)Q3, (uint16_t)IQR, (uint16_t)outlier_low, (uint16_t)outlier_high);

    /* If a sample is not outlier, then sum it */
	uint8_t sample_length = 0;
    uint32_t sample_sum = 0, sample_mean = 0;
    for (i = 0; i < arr_len; i++)
    {
    	if ((arr[i] >= outlier_low) && (arr[i] <= outlier_high))
    	{
    		sample_sum += arr[i];
    		sample_length++;
    	} else {
//    		APP_PRINTF(" %d", arr[i]);
    	}
	}
//    APP_PRINTF("\r\n");

//    /* Ver 2: Sum it anyway */
//    for (i = 0; i < arr_len; i++)
//    {
//    	sample_sum += arr[i];
//    	sample_length++;
//    }
	
	/* Calculate the mean of the sample without the outlier */
	sample_mean = sample_sum/sample_length;

    return sample_mean;
}
