/**
  ******************************************************************************
  * @file           : LIT_Watermark.c
  * @brief          : Source file for Watermark Soil Moisture Sensor Library
  * @author         : Andrean I.
  * @date           : November 2021
  ******************************************************************************
  */

/* Includes -------------------------------------------------------------------*/
#include "LIT_Watermark.h"

/* External variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef hadc;

//TIM_HandleTypeDef htim2;

/* Private constants ----------------------------------------------------------*/
#define	WATERMARK_READ_COUNT		50

#define HIGH						GPIO_PIN_SET
#define LOW							GPIO_PIN_RESET

#define V_SUPPLY					3600
#define	R_REF						7870
#define	R_DIV_1						7870
#define	R_DIV_2						10000

#define R_CALLIB					10000

/* Private function prototypes -----------------------------------------------*/
static void writePin(uint8_t Pin, uint8_t PinState);
static void WM_Select(uint8_t channel);
static void WM_ADCInit(uint32_t ADC_channel);
static uint32_t ADC_ReadChannels(uint32_t channel);
static uint32_t Mean_WithoutOutlier(uint16_t dataset[]);
//static uint16_t WM_GetBatteryLevel(void);
static void WM_GPIO_Init(void);
static void WM_GPIO_DeInit(void);
//static void WM_TIM2_Init(void);
//void delay_us(uint16_t us);

/* Private variables ---------------------------------------------------------*/
uint16_t ReadVRef = 0;
uint16_t Read_Callib_Res = 0;

/*
 * @brief Watermark error state
 */
//typedef enum WatermarkError_e
//{
//	SENSOR_OK			 	= 0,
//	SENSOR_SHORTED	 		= 1,
//	INTERFACE_NOT_RESPONDED	= 2
//} WatermarkError_t;

/* Exported functions ---------------------------------------------------------*/
void Watermark_Init(void)
{
  WM_GPIO_Init();
}

uint8_t Watermark_GetResistance(uint16_t *resistance, uint8_t channel)
{
	WM_GPIO_Init();

//	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_RESET);

	uint16_t read_count = WATERMARK_READ_COUNT;

	uint16_t ADC_ReadA[read_count], ADC_ReadB[read_count];
	uint32_t ADC_ValueA = 0, ADC_ValueB = 0;
	float ADC_VoltA, ADC_VoltB;
	float Res_ParA, Res_ParB;
	float Res_VoltA, Res_VoltB;
	float Res_WM_A, Res_WM_B, Res_WM;
	float R_VSEN = (float)(R_DIV_1 + R_DIV_2) / R_DIV_2;

	uint8_t i = 0;

	writePin(WM_EN, GPIO_PIN_RESET);
	writePin(WM_REF, GPIO_PIN_RESET);

	HAL_Delay(100);

	uint16_t SupplyV = SYS_GetBatteryLevel();

	writePin(WM_EN, GPIO_PIN_SET);
	writePin(WM_REF, GPIO_PIN_SET);

	HAL_Delay(100);

	ReadVRef = SYS_GetBatteryLevel();

	/***** Callibration channel resistance calculation *****/

	uint16_t ADC_ReadCalA[50], ADC_ReadCalB[50];
	uint32_t ADC_ValCalA, ADC_ValCalB;
	float ADC_VoltCalA, ADC_VoltCalB;
	float Res_ParCalA, Res_ParCalB;
	float Res_VoltCalA, Res_VoltCalB;
	float Res_CalA, Res_CalB, Res_Cal;
	float Callib_Const;

	/*** Select channel ***/
	WM_Select(WM_CHANNEL_3);
	HAL_Delay(100);

	/*** Set the initial state for each pin ***/
	writePin(WM_PWM1, LOW);
	writePin(WM_PWM2, LOW);
	HAL_Delay(100);

	for (i = 0; i < 50; i++)
	{
		/*** Read Watermark with WM_PWM1 -> WM_PWM2 current direction ***/

		/* Set WM_PWM1 pin to high and delay */
		writePin(WM_PWM1, HIGH);

		/* Read ADC */
		ADC_ReadCalA[i] = ADC_ReadChannels(WATERMARK_IN_CHANNEL);

		/* Set WM_PWM1 pin to low and delay */
		writePin(WM_PWM1, LOW);

		/* Reverse the polarity. Read Watermark with WM_PWM2 -> WM_PWM1 current direction */

		/* Set WM_PWM1 pin to high and delay */
		writePin(WM_PWM2, HIGH);

		/* Read ADC */
		ADC_ReadCalB[i] = ADC_ReadChannels(WATERMARK_IN_CHANNEL);

		/* Set WM_PWM1 pin to low and delay */
		writePin(WM_PWM2, LOW);
		HAL_Delay(10);


	}

	writePin(WM_PWM1, LOW);
	writePin(WM_PWM2, LOW);

	/* Calculate the mean of the ADC Value */
	ADC_ValCalA = Mean_WithoutOutlier(ADC_ReadCalA);
	ADC_ValCalB = Mean_WithoutOutlier(ADC_ReadCalB);

	/* Calculate the voltage */
	ADC_VoltCalA = ADC_ValCalA * ReadVRef / (4095 * 1.0);
	ADC_VoltCalB = ADC_ValCalB * ReadVRef / (4095 * 1.0);

	/* Calculate the paralel resistance value */
	Res_ParCalA = ( (float) R_REF * ( R_DIV_1 + R_DIV_2 )) / ( R_REF + ( R_DIV_1 + R_DIV_2 ));
	Res_ParCalB = ( (float) R_REF / ( SupplyV - R_VSEN * ADC_VoltCalB )) * R_VSEN * ADC_VoltCalB;

	/* Calculate the resistance */
	Res_CalA = Res_ParCalA / ( R_VSEN * ADC_VoltCalA ) * ( SupplyV - R_VSEN * ADC_VoltCalA );
	Res_CalB = ( Res_ParCalB * ( R_DIV_1 + R_DIV_2 ) ) / ( ( R_DIV_1 + R_DIV_2 ) - Res_ParCalB );
	Res_Cal = (Res_CalA + Res_CalB) / 2;

	/* Calculate the callibration constant */
	Callib_Const = Res_Cal / R_CALLIB;

	Read_Callib_Res = Res_Cal;

	APP_PRINTF("SupplyV: %d mV | Res_Cal : %5d ohm | Callib_Const: %d.%03d\r\n",
			(uint32_t)(SupplyV),
			(uint32_t)(Res_Cal),
			(uint32_t)Callib_Const, (uint32_t)(Callib_Const * 1000) % 1000
			);

	/***** Selected channel resistance calculation *****/

	/*** Select channel ***/
	WM_Select(channel);
	HAL_Delay(100);

	/*** Set the initial state for each pin ***/
	writePin(WM_PWM1, LOW);
	writePin(WM_PWM2, LOW);
	HAL_Delay(100);

	for (i = 0; i <= read_count - 1; i++)
	{
		/*** Read Watermark with WM_PWM1 -> WM_PWM2 current direction ***/

		/* Set WM_PWM1 pin to high and delay */
		writePin(WM_PWM1, HIGH);
//		DWT_Delay(75);

		/* Read ADC */
		ADC_ReadA[i] = ADC_ReadChannels(WATERMARK_IN_CHANNEL);

		/* Set WM_PWM1 pin to low and delay */
		writePin(WM_PWM1, LOW);
		HAL_Delay(10);

		/* Reverse the polarity. Read Watermark with WM_PWM2 -> WM_PWM1 current direction */

		/* Set WM_PWM1 pin to high and delay */
		writePin(WM_PWM2, HIGH);
//		DWT_Delay(75);

		/* Read ADC */
		ADC_ReadB[i] = ADC_ReadChannels(WATERMARK_IN_CHANNEL);

		/* Set WM_PWM1 pin to low and delay */
		writePin(WM_PWM2, LOW);
		HAL_Delay(10);

	}

//	for (i = 0; i <= read_count - 1; i++)
//		APP_PRINTF("%d ", ADC_ReadA[i]);
//
//	APP_PRINTF("\r\n");
//
//	for (i = 0; i <= read_count - 1; i++)
//		APP_PRINTF("%d ", ADC_ReadB[i]);
//
//	APP_PRINTF("\r\n");

	/* Calculate the mean of the ADC Value */
	ADC_ValueA = Mean_WithoutOutlier(ADC_ReadA);
	ADC_ValueB = Mean_WithoutOutlier(ADC_ReadB);

	/* Calculate the voltage */
	ADC_VoltA = ADC_ValueA * ReadVRef / (4095 * 1.0);
	ADC_VoltB = ADC_ValueB * ReadVRef / (4095 * 1.0);

	/* Calculate the paralel resistance value */
	Res_ParA = ( (float) R_REF * ( R_DIV_1 + R_DIV_2 )) / ( R_REF + ( R_DIV_1 + R_DIV_2 ));
	Res_ParB = ( (float) R_REF / ( SupplyV - R_VSEN * ADC_VoltB )) * R_VSEN * ADC_VoltB;

	/* Calculate the resistance */
	Res_WM_A = Res_ParA / ( R_VSEN * ADC_VoltA ) * ( SupplyV - R_VSEN * ADC_VoltA );
	Res_WM_B = ( Res_ParB * ( R_DIV_1 + R_DIV_2 ) ) / ( ( R_DIV_1 + R_DIV_2 ) - Res_ParB );
	Res_WM = (Res_WM_A + Res_WM_B) / 2;

	writePin(WM_EN, GPIO_PIN_RESET);
	writePin(WM_REF, GPIO_PIN_RESET);

	WM_GPIO_DeInit();

	Res_WM /= Callib_Const;

	if (Res_WM > 30000) Res_WM = 30000;

	*resistance = (uint16_t)Res_WM;

//	APP_PRINTF("R_VSEN: %d | VRef: %4d | ADC_1: %4d | ADC_2: %4d | Volt_1: %4d mV | Volt_2: %4d mV | Res_ParA: %4d ohm | Res_ParB: %4d ohm | Res_WM_A : %5d ohm | Res_WM_B : %5d ohm | Res_WM : %5d ohm\r\n",
//			(uint32_t)(R_VSEN * 1000000),
//			(uint32_t)(ReadVRef * 1000),
//			ADC_ValueA * 1000, ADC_ValueB * 1000,
//			(uint32_t)(ADC_VoltA * 1000), (uint32_t)(ADC_VoltB * 1000),
//			(uint32_t)(Res_ParA * 1000), (uint32_t)(Res_ParB * 1000),
//			(uint32_t)(Res_WM_A * 1000), (uint32_t)(Res_WM_B * 1000), (uint32_t)(Res_WM * 1000)
//			);

	APP_PRINTF("R_VSEN: %d | VRef: %4d | ADC_1: %4d | ADC_2: %4d | Volt_1: %4d mV | Volt_2: %4d mV | Res_ParA: %4d ohm | Res_ParB: %4d ohm | Res_WM_A : %5d ohm | Res_WM_B : %5d ohm | Res_WM : %5d ohm\r\n",
			(uint32_t)R_VSEN,
			ReadVRef,
			ADC_ValueA, ADC_ValueB,
			(uint32_t)(ADC_VoltA), (uint32_t)(ADC_VoltB),
			(uint32_t)(Res_ParA), (uint32_t)(Res_ParB),
			(uint32_t)(Res_WM_A), (uint32_t)(Res_WM_B), (uint32_t)(Res_WM)
			);

	if (Res_WM < 100)
		return SENSOR_SHORTED;
	else
		return SENSOR_OK;

	APP_PRINTF("10\r\n");
}

void Watermark_GetDebugInfo(uint16_t *VRef, uint16_t *Callib_Resistance)
{
	*VRef = ReadVRef;
	*Callib_Resistance = Read_Callib_Res;
}

/* Private functions ---------------------------------------------------------*/
void writePin(uint8_t Pin, uint8_t PinState)
{
	switch (Pin)
	{
		case WM_MUX_A : HAL_GPIO_WritePin(WATERMARK_MUXA_GPIO_Port, WATERMARK_MUXA_Pin, PinState); break;
		case WM_MUX_B : HAL_GPIO_WritePin(WATERMARK_MUXB_GPIO_Port, WATERMARK_MUXB_Pin, PinState); break;
		case WM_PWM1  : HAL_GPIO_WritePin(WATERMARK_PWM1_GPIO_Port, WATERMARK_PWM1_Pin, PinState); break;
		case WM_PWM2  : HAL_GPIO_WritePin(WATERMARK_PWM2_GPIO_Port, WATERMARK_PWM2_Pin, PinState); break;
		case WM_EN	  : HAL_GPIO_WritePin(WATERMARK_EN_GPIO_Port, WATERMARK_EN_Pin, PinState); break;
		case WM_REF	  : HAL_GPIO_WritePin(WATERMARK_REF_GPIO_Port, WATERMARK_REF_Pin, PinState); break;
		default	: break;
	}
}

static void WM_Select(uint8_t channel)
{
	switch(channel)
	{
		case WM_CHANNEL_1 :
		{
			writePin(WM_MUX_A, LOW);
			writePin(WM_MUX_B, LOW);
		} break;
		case WM_CHANNEL_2 :
		{
			writePin(WM_MUX_A, HIGH);
			writePin(WM_MUX_B, LOW);
		} break;
		case WM_CHANNEL_3 :
		{
			writePin(WM_MUX_A, LOW);
			writePin(WM_MUX_B, HIGH);
		} break;
		case WM_CHANNEL_4 :
		{
			writePin(WM_MUX_A, HIGH);
			writePin(WM_MUX_B, HIGH);
		} break;
		default:
			break;
	}
}

static void WM_ADCInit(uint32_t ADC_channel)
{
	HAL_ADCEx_Calibration_Start(&hadc);

	ADC_ChannelConfTypeDef sConfig = {0};

	sConfig.Channel = ADC_channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);
}

uint32_t ADC_ReadChannels(uint32_t channel)
{
  uint32_t ADCxConvertedValues = 0;
  ADC_ChannelConfTypeDef sConfig = {0};

  MX_ADC_Init();

  /* Start Calibration */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /* Configure Regular Channel */
  sConfig.Channel = channel;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  if (HAL_ADC_Start(&hadc) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }
  /** Wait for end of conversion */
  HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);

  /** Wait for end of conversion */
  HAL_ADC_Stop(&hadc) ;   /* it calls also ADC_Disable() */

  ADCxConvertedValues = HAL_ADC_GetValue(&hadc);

  HAL_ADC_DeInit(&hadc);

  return ADCxConvertedValues;

}


uint32_t Mean_WithoutOutlier(uint16_t dataset[])
{
	uint16_t i, j;

	uint16_t dataset_len;

	/* Calculate the length of the data set */
	dataset_len = WATERMARK_READ_COUNT;

	/* Sort the data set */
    for (i = 0; i < dataset_len-1; i++)
        for (j = 0; j < dataset_len-i-1; j++)
           if (dataset[j] > dataset[j+1])
           {
        	   dataset[j]	= dataset[j] + dataset[j+1];
        	   dataset[j+1]	= dataset[j] - dataset[j+1];
        	   dataset[j]   = dataset[j] - dataset[j+1];
		   }

    /* Calculate the interquartile range */
    uint8_t	q1_idx = 0, q3_idx = 0;
    float Q1, Q3, IQR;

    q1_idx = (uint8_t)((dataset_len + 1) / 4);
    q3_idx = (uint8_t)((dataset_len + 1) * 3 / 4);

    if ((dataset_len+1)/4 % 2 != 0)
    {
    	Q1 = (dataset[q1_idx - 1] + dataset[q1_idx]) / 2;
    	Q3 = (dataset[q3_idx - 1] + dataset[q3_idx]) / 2;
    }
    else
    {
    	Q1 = dataset[q1_idx - 1];
    	Q3 = dataset[q3_idx - 1];
    }

    IQR = Q3-Q1;

    /* Set the outlier */
    float outlier_low, outlier_high;
    outlier_low  = Q1 - 3/2*IQR;
    outlier_high = Q3 + 3/2*IQR;

//    PRINTF("dataset_len: %d | q1_idx: %d | q3_idx: %d | Q1: %d | Q3: %d | IQR: %d | Outlier low: %d | Outlier high: %d\r\n", dataset_len, q1_idx, q3_idx, (uint16_t)Q1, (uint16_t)Q3, (uint16_t)IQR, (uint16_t)outlier_low, (uint16_t)outlier_high);

//    PRINTF("Outlier: ");
    /* If a sample is not outlier, then sum it */
	uint8_t sample_length = 0;
	uint32_t sample_sum = 0, sample_mean = 0;
    for (i = 0; i < dataset_len; i++)
    {
    	if ((dataset[i] >= outlier_low) && (dataset[i] <= outlier_high))
    	{
    		sample_sum += dataset[i];
    		sample_length++;
    	}
    	else
    	{
//    		PRINTF("%d ", dataset[i]);
    	}
	}
//    PRINTF("\r\n");

    sample_mean = sample_sum/sample_length;

//    PRINTF("Sample mean: %d | Sample sum: %d | Sample length: %d\r\n", sample_mean, sample_sum, sample_length);

    return sample_mean;
}

//uint16_t WM_GetBatteryLevel(void)
//{
//
//  APP_PRINTF("Init Variable\r\n");
//
//  uint16_t batteryLevelmV = 0;
//  uint32_t measuredLevel = 0;
//
//  APP_PRINTF("Init VREFINT ADC Channel\r\n");
//
//  WM_ADCInit(ADC_CHANNEL_VREFINT);
//
//  APP_PRINTF("Start reading\r\n");
//
//  HAL_ADC_Start(&hadc);
//
//  APP_PRINTF("Start PoolForConversion\r\n");
//
//  HAL_ADC_PollForConversion(&hadc, 1000);
//
//  APP_PRINTF("Stop ADC\r\n");
//  HAL_ADC_Stop(&hadc);
//
//  APP_PRINTF("ADC Get Value\r\n");
//  measuredLevel = HAL_ADC_GetValue(&hadc);
//
//  APP_PRINTF("Convert measured level to battery level mV\r\n");
//
//  if (measuredLevel == 0)
//  {
//	  batteryLevelmV = 0;
//  }
//  else
//  {
//	  if ((uint32_t)*VREFINT_CAL_ADDR != (uint32_t)0xFFFFU)
//	  {
//		  /* Device with Reference voltage calibrated in production:
//			use device optimized parameters */
//		  batteryLevelmV = __LL_ADC_CALC_VREFANALOG_VOLTAGE(measuredLevel,
//				  	  	  	  ADC_RESOLUTION_12B);
//	  }
//	  else
//	  {
//		  /* Device with Reference voltage not calibrated in production:
//			use generic parameters */
//		  batteryLevelmV = (VREFINT_CAL_VREF * 1510) / measuredLevel;
//	  }
//  }
//
//  return batteryLevelmV;
//  /* USER CODE BEGIN SYS_GetBatteryLevel_2 */
//
//  /* USER CODE END SYS_GetBatteryLevel_2 */
//}

void WM_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, WATERMARK_MUXA_Pin|WATERMARK_MUXB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, WATERMARK_EN_Pin|WATERMARK_PWM1_Pin|WATERMARK_PWM2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : WATERMARK_MUXA_Pin WATERMARK_MUXB_Pin */
  GPIO_InitStruct.Pin = WATERMARK_MUXA_Pin|WATERMARK_MUXB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : WATERMARK_EN_Pin WATERMARK_PWM1_Pin WATERMARK_PWM2_Pin */
  GPIO_InitStruct.Pin = WATERMARK_EN_Pin|WATERMARK_PWM1_Pin|WATERMARK_PWM2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void WM_GPIO_DeInit(void)
{
  /*Configure GPIO pins : WATERMARK_MUXA_Pin WATERMARK_MUXB_Pin */
  HAL_GPIO_DeInit(GPIOB, WATERMARK_MUXA_Pin);
  HAL_GPIO_DeInit(GPIOB, WATERMARK_MUXB_Pin);

  /*Configure GPIO pins : WATERMARK_EN_Pin WATERMARK_PWM1_Pin WATERMARK_PWM2_Pin */
  HAL_GPIO_DeInit(GPIOA, WATERMARK_EN_Pin);
  HAL_GPIO_DeInit(GPIOA, WATERMARK_PWM1_Pin);
  HAL_GPIO_DeInit(GPIOA, WATERMARK_PWM2_Pin);
}
