/**
  ******************************************************************************
  * @file           : LIT_Watermark.h
  * @brief          : Header file for Watermark Soil Moisture Sensor Library
  * @author         : Andrean I.
  * @date           : November 2021
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LIT_WATERMARK_H__
#define __LIT_WATERMARK_H__

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "adc_if.h"

#include "sys_app.h"

/* Exported constants --------------------------------------------------------*/
/*!
 * Define Watermark debug (0:OFF, 1:ON)
 */
#define WATERMARK_DEBUG						          1

//#define WM_CHANNEL_1		0
//#define WM_CHANNEL_2		1
//#define WM_CHANNEL_3		2
//#define WM_CHANNEL_4		3
//
//#define	WM_MUX_A			4
//#define WM_MUX_B			5
//#define WM_PWM1				6
//#define WM_PWM2				7

/**
 * Default Watermark pin configuration
 */

#ifndef WATERMARK_EN_GPIO_Port
#define WATERMARK_EN_GPIO_Port              GPIOA
#endif

#ifndef WATERMARK_EN_Pin
#define WATERMARK_EN_Pin                    GPIO_PIN_1
#endif

#ifndef WATERMARK_REF_GPIO_Port
#define WATERMARK_REF_GPIO_Port             GPIOA
#endif

#ifndef WATERMARK_REF_Pin
#define WATERMARK_REF_Pin                   GPIO_PIN_13
#endif

#ifndef WATERMARK_MUXA_GPIO_Port
#define WATERMARK_MUXA_GPIO_Port            GPIOB
#endif

#ifndef WATERMARK_MUXA_Pin
#define WATERMARK_MUXA_Pin                  GPIO_PIN_4
#endif

#ifndef WATERMARK_MUXB_GPIO_Port
#define WATERMARK_MUXB_GPIO_Port            GPIOB
#endif

#ifndef WATERMARK_MUXB_Pin
#define WATERMARK_MUXB_Pin                  GPIO_PIN_5
#endif

#ifndef WATERMARK_PWM1_GPIO_Port
#define WATERMARK_PWM1_GPIO_Port            GPIOA
#endif

#ifndef WATERMARK_PWM1_Pin
#define WATERMARK_PWM1_Pin                  GPIO_PIN_6
#endif

#ifndef WATERMARK_PWM2_GPIO_Port
#define WATERMARK_PWM2_GPIO_Port            GPIOA
#endif

#ifndef WATERMARK_PWM2_Pin
#define WATERMARK_PWM2_Pin                  GPIO_PIN_7
#endif

#ifndef WATERMARK_IN_GPIO_Port
#define WATERMARK_IN_GPIO_Port              GPIOA
#endif

#ifndef WATERMARK_IN_Pin
#define WATERMARK_IN_Pin                    GPIO_PIN_14
#endif

#ifndef WATERMARK_IN_CHANNEL
#define WATERMARK_IN_CHANNEL                ADC_CHANNEL_10
#endif

/* Exported types ------------------------------------------------------------*/
typedef enum WatermarkPin_e
{
	WM_MUX_A,
	WM_MUX_B,
	WM_PWM1,
	WM_PWM2,
	WM_EN,
	WM_REF
} WatermarkPin_t;

/*
 * @brief Watermark channel option
 */
typedef enum WatermarkChannel_e
{
	WM_CHANNEL_1 = 0,
	WM_CHANNEL_2 = 1,
	WM_CHANNEL_3 = 2,
	WM_CHANNEL_4 = 3,
	WM_Cal = WM_CHANNEL_4
} WatermarkChannel_t;

/*
 * @brief Watermark error state
 */
typedef enum WatermarkError_e
{
	SENSOR_OK			 	= 0,
	SENSOR_SHORTED	 		= 1,
	INTERFACE_NOT_RESPONDED	= 2
} WatermarkError_t;

/* Exported functions prototypes ---------------------------------------------*/

void Watermark_Init(void);

/**
 *	@brief	Read the input from Watermark interface
 *	@param	resistance to store value of sensor's resistance
 *	@param	channel selected channel
 *	@retval Error code
 */
uint8_t Watermark_GetResistance(uint16_t *resistance, uint8_t channel);

void Watermark_GetDebugInfo(uint16_t *VRef, uint16_t *Callib_Resistance);

#endif /* __LIT_WATERMARK_H__ */
